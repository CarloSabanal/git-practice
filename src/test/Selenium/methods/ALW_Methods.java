package methods;

import javax.swing.JOptionPane;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import pageObjects.LoginObjects;

public class ALW_Methods {

	GenericMethods gm = new GenericMethods();
	
	public void login(WebDriver driver, String userName, String password) throws InterruptedException{
		
		//6976.1 to 4
		gm.waitForObject(driver, LoginObjects.txtUsername);
		if(gm.elementExist(driver, LoginObjects.txtUsername) && gm.elementExist(driver, LoginObjects.txtPassword)){
            gm.setLogMsg("Pass", "Username and Password exists!");
            
            driver.findElement(LoginObjects.txtUsername).clear();
            driver.findElement(LoginObjects.txtUsername).sendKeys(userName);//hina.tabassum saket.parekh
            driver.findElement(LoginObjects.txtPassword).clear();
            driver.findElement(LoginObjects.txtPassword).sendKeys(password);
            driver.findElement(LoginObjects.btnSignin).click();
            Thread.sleep(5000);
            
            /*
            WebElement elm =driver.findElement(LoginObjects.btnSecurityCode);
            if(elm.isDisplayed())
            {
            JOptionPane.showMessageDialog(null,"Please enter the Security scemantic code and than press ok");
            }
            */
			} else {
			            gm.logScreenshot("Fail", "Username and Password fields do NOT exists!", driver);
			}
		
	}
	
	
	public void firstTimeUser(WebDriver driver) throws InterruptedException{
			
		Thread.sleep(15000);
		
		if(gm.elementExist(driver, LoginObjects.firstPopUp) && driver.getCurrentUrl().equals("https://liquidworkforcev2-test.accenture.com/Landing/LandingPage")){
			gm.logScreenshot("Pass", "Pop up for T&C and DP appeared!", driver);
			if(gm.elementExist(driver, LoginObjects.dataPrivacyStatement) && gm.elementExist(driver, LoginObjects.termsOfUse)){
				gm.setLogMsg("Pass", "Links preseeeeent!!!!");
			} else {
				gm.setLogMsg("Fail", "Links NOOOOOT preseeeeent!!!!");
			}
			/**driver.findElement(LoginObjects.agree).click();commenting this out to avoid tagging saket's id as 2nd time user*/
			
		} else {
			gm.logScreenshot("Fail", "There was no pop up!", driver);
		}

	}
	
	public void oldUser(WebDriver driver) throws InterruptedException{
		
		Thread.sleep(15000);
				
		if(!gm.elementExist(driver, LoginObjects.firstPopUp) && !driver.getCurrentUrl().equals("https://liquidworkforcev2-test.accenture.com/Landing/LandingPage")){
			gm.logScreenshot("Pass", "There was no pop up!", driver);
		} else {
			gm.logScreenshot("Fail", "Pop up for T&C and DP appeared!", driver);
		}

	}
	
	public void invalidCredentials (WebDriver driver) throws InterruptedException{
		
			Thread.sleep(10000);
			if(gm.elementExist(driver, LoginObjects.accessDenied)){
				gm.logScreenshot("Pass", "User is denied", driver);
			} else {
				gm.logScreenshot("Fail", "User was able to log in! NOOOOOOO! Security breach!!!!!!!!", driver);
			}
		
	}
	
	public void setEnvironment(WebDriver driver, String url) throws InterruptedException
	{
		System.setProperty(EnvironmentVariables.driverType, EnvironmentVariables.driverPath);
		driver = new ChromeDriver();//InternetExplorerDriver();ChromeDriver
		gm.setLogMsg("Info", "Successfully launched the browser");
		driver.manage().window().maximize();
		driver.navigate().to(url);
		Thread.sleep(10000);
		
		
		
	}
}
