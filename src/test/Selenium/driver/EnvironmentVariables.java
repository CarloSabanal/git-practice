package driver;

public class EnvironmentVariables {

	// Update the Work Book Name 
	public final static String workbookName = "Selenium_DataSheet.xlsx";
	public final static String userFile = "addmembertemplate.csv";
	public final static String driverPath = "C:\\Users\\jan.carlo.l.sabanal\\OneDrive - Accenture\\WebDriver\\chromedriver.exe";//or chromedriver //IEDriverServer
	public final static String driverType = "webdriver.chrome.driver";//or chrome //ie
	
	public final static String dataPoolPath = System.getProperty("user.dir")+"\\src\\test\\Selenium\\resources\\"+workbookName;
	public final static String userFilePath = System.getProperty("user.dir")+"\\src\\test\\Selenium\\resources\\"+userFile;
	
	
	//Constant Variables
		public final static String USERNAME = "";
		public final static String PASSWORD = "gC6Kc7Y0e534f9J1oM2H";
		public final static String URL = "https://liquidworkforcev2-test.accenture.com/";
		public final static String TaskURL = "https://liquidworkforcev2-test.accenture.com/tasks/";
		public final static String TaskID = "263";//342 - marketplace member, 338 - not member
		
}
