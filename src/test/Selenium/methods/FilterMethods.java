package methods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import pageObjects.DashboardObjects;
import pageObjects.FilterObjects;
import pageObjects.HeaderObjects;
import pageObjects.PostTaskObjects;

import java.util.List;

import org.openqa.selenium.By;

/**
 * 
 * @author jan.carlo.l.sabanal
 * @since 11/21/2016
 * Description - this is for validating the post section of the dashboard  
 * 
 * */

public class FilterMethods {
	
	
	GenericMethods gm = new GenericMethods();
//=========================================================================================================================================
	/**User Story 11741: As a Task Poster, I am able to provide Language(s) to filter my targeted crowd in Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @since 2017/01/13
	 * 
	 * */
	public void applyLanguageFilter(WebDriver driver, String language, String languageAny, String index) throws InterruptedException
	{
			
			if(languageAny.equalsIgnoreCase("all")){
				driver.findElement(FilterObjects.radioAll).click();
			}
			
			String[] languages = language.split(",");
			driver.findElement(FilterObjects.languageField).click();//click the field first
			Thread.sleep(3000);
			
			for (int j = 0; j < languages.length; j++) {//every time clicking on the choices in the drop down menu,cursor automatically moves to the next input field
									
	            List<WebElement> elm = driver.findElements(FilterObjects.languageFieldInput);
	            System.out.println("The len is : "+ elm.size());
	            elm.get(0).sendKeys(languages[j]);
	            Thread.sleep(1000);
	            driver.findElement(FilterObjects.languageComboBox(languages[j])).click();
			}
			
			/**11741.1.1.1.6 to test the delete button*/
			while(gm.elementExist(driver, FilterObjects.languageDeleteSelected)){
				driver.findElement(FilterObjects.languageDeleteSelected).click();
			}
			
			/**then repeat selecting language filter*/
			for (int j = 0; j < languages.length; j++) {
				
	            List<WebElement> elm = driver.findElements(FilterObjects.languageFieldInput);
	            System.out.println("The len is : "+ elm.size());
	            elm.get(0).sendKeys(languages[j]);
	            Thread.sleep(1000);
	            driver.findElement(FilterObjects.languageComboBox(languages[j])).click();
			}
			driver.findElement(FilterObjects.filterLanguageSelected).click();		
			driver.findElement(FilterObjects.applyAndNext(index)).click();		
			Thread.sleep(2000);
			gm.checkThings(driver, FilterObjects.filterLanguageUnselected, "Langauge Link not highlighted");
			gm.checkThings(driver, FilterObjects.languageCheck, "Language Checkmark displayed");
	}	
	
//=========================================================================================================================================
	/**User Story 11058:Task Poster is able to provide my required Career Level to filter my targeted crowd in Post a task screen for the new UI.
	 *  - superseding - User Story 9534:Able to provide my required Career Level to filter my targeted crowd in Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 01/16/2017
	 * 
	*/
	public void applyCareerLevelFilter(WebDriver driver, String level, String index) throws InterruptedException{
		String[] careerLevel = level.split(",");
			
				driver.findElement(FilterObjects.careerLevelCheckBox("Any")).click();
				
				for(int i=0;i<careerLevel.length;i++){
					if(careerLevel[i].equals("13")||careerLevel[i].equals("12")||careerLevel[i].equals("11")||careerLevel[i].equals("10")){
						driver.findElement(FilterObjects.careerLevelCheckBox(careerLevel[i] + "0")).click();
					} else {
						if(careerLevel[i].equalsIgnoreCase("ldr")){
							driver.findElement(FilterObjects.careerLevelCheckBox("LDR")).click();
						} else {
							driver.findElement(FilterObjects.careerLevelCheckBox("0" + careerLevel[i] + "0")).click();
						}
					}
					Thread.sleep(1000);
					gm.setLogMsg("Pass", careerLevel[i]);
				}
				
				gm.logScreenshot("info", "checkboxes", driver);
				
				driver.findElement(FilterObjects.applyAndNext(index)).click();		
				Thread.sleep(2000);
				gm.checkThings(driver, FilterObjects.filterCareerLevelUnselected, "Career Level Link not highlighted");
				gm.checkThings(driver, FilterObjects.careerLevelCheck, "Career Level Checkmark displayed");
				Thread.sleep(3000);
			
	}
	
	
//=========================================================================================================================================
	/**User Story 11060:Task Poster is able to provide my required Organization Unit to filter my targeted crowd in Post a task screen
	 *  
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 03/14/2017  - Pi Day! :)
	 * 
	*/
	public void applyOrgUnitFilter(WebDriver driver, String orgUnit) throws InterruptedException{

		
		
		String[]orgUnitGroup = orgUnit.split("-");
		System.out.println(orgUnitGroup.length + " split .");
		driver.findElement(FilterObjects.orgUnitAnyCheckBox).click();
		
		for(int k = 0; k<orgUnitGroup.length; k++){/**this is to enable numerous org units to be selected. each specific org unit is separated by *. to navigate to the org unit, hierarchy is separated by comma.*/
			String[]orgUnitSeries = orgUnitGroup[k].split(",");
			System.out.println(orgUnitSeries.length + " split ,");
			
			
			int j;
			for(j = 0;j<orgUnitSeries.length-1;j++){
				driver.findElement(FilterObjects.orgUnitExpand(orgUnitSeries[j])).click();
				Thread.sleep(5000);
			}
			driver.findElement(FilterObjects.orgUnitCheckBox(orgUnitSeries[j])).click();
		}
			
	
	}
	
	
//=========================================================================================================================================
	/**User Story 11057:Task Poster is able to provide my required Resource Location to filter my targeted crowd in Post a task screen
	 *  
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 03/14/2017  - Pi Day! :)
	 * 
	*/
	public void applyResourceLocationFilter(WebDriver driver, String resourceLoc) throws InterruptedException{

		
		
		String[]resourceLocGroup = resourceLoc.split("-");
		System.out.println(resourceLocGroup.length + " split .");
		driver.findElement(FilterObjects.resourceLocAnyCheckBox).click();
		
		for(int k = 0; k<resourceLocGroup.length; k++){/**this is to enable numerous org units to be selected. each specific org unit is separated by *. to navigate to the org unit, hierarchy is separated by comma.*/
			String[]resourceLocSeries = resourceLocGroup[k].split(",");
			System.out.println(resourceLocSeries.length + " split ,");
			
			
			int j;
			for(j = 0;j<resourceLocSeries.length-1;j++){
				driver.findElement(FilterObjects.resourceLocExpand(resourceLocSeries[j])).click();
				Thread.sleep(5000);
			}
			driver.findElement(FilterObjects.resourceLocCheckBox(resourceLocSeries[j])).click();
		}
			
	
	}

//=========================================================================================================================================
	/**User Story 11059:Task Poster is able to provide my required Talent Segment to filter my targeted crowd in Post a task screen.
	 *  
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 03/14/2017  - Pi Day! :)
	 * 
	*/
	public void applyTalentSegmentFilter(WebDriver driver, String talentSegment) throws InterruptedException{

		
		
		String[]talentSegmentGroup = talentSegment.split("-");
		System.out.println(talentSegmentGroup.length + " split .");
		driver.findElement(FilterObjects.talentSegmentAnyCheckBox).click();
		
		for(int k = 0; k<talentSegmentGroup.length; k++){/**this is to enable numerous org units to be selected. each specific org unit is separated by *. to navigate to the org unit, hierarchy is separated by comma.*/
			String[]talentSegmentSeries = talentSegmentGroup[k].split(",");
			System.out.println(talentSegmentSeries.length + " split ,");
			
			
			int j;
			for(j = 0;j<talentSegmentSeries.length-1;j++){
				driver.findElement(FilterObjects.talentSegmentExpand(talentSegmentSeries[j])).click();
				Thread.sleep(5000);
			}
			driver.findElement(FilterObjects.talentSegmentCheckBox(talentSegmentSeries[j])).click();
		}
			
	
	}
	
	
//=========================================================================================================================================
	/**User Story 11594: Task Poster is able to provide my required Specialty to filter my targeted crowd in Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @since 2017/03/14
	 * 
	 * */
	public void applySpecialtyFilter(WebDriver driver, String specialty, String index) throws InterruptedException
	{
			
						
			String[] specialties = specialty.split(",");
			driver.findElement(FilterObjects.specialtyField).click();//click the field first
			Thread.sleep(3000);
			
			for (int j = 0; j < specialties.length; j++) {//every time clicking on the choices in the drop down menu,cursor automatically moves to the next input field
									
	            List<WebElement> elm = driver.findElements(FilterObjects.specialtyFieldInput);
	            System.out.println("The len is : "+ elm.size());
	            elm.get(0).sendKeys(specialties[j]);
	            Thread.sleep(1000);
	            driver.findElement(FilterObjects.specialtyComboBox(specialties[j])).click();
			}
			
			/**11741.1.1.1.6 to test the delete button*/
			while(gm.elementExist(driver, FilterObjects.specialtyDeleteSelected)){
				driver.findElement(FilterObjects.specialtyDeleteSelected).click();
			}
			
			/**then repeat selecting specialties*/
			for (int j = 0; j < specialties.length; j++) {//every time clicking on the choices in the drop down menu,cursor automatically moves to the next input field
				
	            List<WebElement> elm = driver.findElements(FilterObjects.specialtyFieldInput);
	            System.out.println("The len is : "+ elm.size());
	            elm.get(0).sendKeys(specialties[j]);
	            Thread.sleep(1000);
	            driver.findElement(FilterObjects.specialtyComboBox(specialties[j])).click();
			}
			
			driver.findElement(FilterObjects.filterSpecialtySelected).click();		
			driver.findElement(FilterObjects.applyAndNext(index)).click();		
			Thread.sleep(2000);
			gm.checkThings(driver, FilterObjects.filterSpecialtyUnselected, "specialty Link not highlighted");
			gm.checkThings(driver, FilterObjects.specialtyCheck, "specialty Checkmark displayed");
	}
}
