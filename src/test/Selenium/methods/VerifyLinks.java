package methods;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import driver.GenericMethods;
import pageObjects.FooterObjects;
import pageObjects.Terms_Policies_PageObjects;

public class VerifyLinks {

	
	GenericMethods gm = new GenericMethods();
	
	
	
	public void checkLinks(WebDriver driver, String links, String j, boolean click, String flag,String value1, String value2, String value3, String value4) throws InterruptedException{		
		
	
		VerifyLinks vl = new VerifyLinks();
		By masterLinks;
			if(flag.contains("head")){
				masterLinks = By.xpath("//nav//descendant::*[text()='" + links.toUpperCase() + "']");
			} else {
				masterLinks = By.xpath("//*[@class='col-md-" + j + "']/descendant::a[text()='" + links + "']");
			}
			System.out.println(masterLinks);
			
			if(gm.elementExist(driver,masterLinks)){
				gm.setLogMsg("Pass", links + " exists!");
			} else {
				gm.setLogMsg("Fail", links + " does not exist!");
			}
						
			if(click){//if we will click the link - this is for 6991.5 to 6991.10, 12 to 14, 17
								
				Thread.sleep(2000);
				
				/**6991.17 and 6991.14 commented out yet since selenium is not functioning well*/
				if(links.equals("Feedback")||links.equals("Circles")){
					vl.checkNewTab(driver, masterLinks, links);
				} else {
					
					driver.findElement(masterLinks).click();//will actually click the link
					
					
							/**6988.9 this is for both header and footer. header functionality but when you click on footer links, this will also come to play.
							 * 
							 * "Validate that the label turns white and underlined in blue to indicate that it is currently the selected navigation
								Main Navigation:
								               HOME
								               MARKETPLACES
								               TASKS
								               APPLICATIONS
								               POSTS"

							 * 
							 * */
							if(!links.equals("User Guide")||!links.equals("FAQ")||!links.equals("Post a Task")){//because userguide and faq does not have link in the header
								System.out.println(links + "**************************************************************************");
								By headerSelectedLink = By.xpath("//*[@class='activeLinkHeader activeLink']//descendant::*[text()='" + links.toUpperCase() + "']");
								By headerSelectedDiv = By.xpath("//*[@class='activeLinkHeader activeLink']");
								
								if(gm.elementExist(driver, headerSelectedLink)){
									
									String bottomColor = driver.findElement(headerSelectedDiv).getCssValue("border-bottom-color");
									String backColor = driver.findElement(headerSelectedDiv).getCssValue("color");
									
									System.out.println(headerSelectedLink + " " + bottomColor + " " + backColor);
									
									if(bottomColor.equals("rgba(135, 206, 235, 1)")&& backColor.equals("rgba(255, 255, 255, 1)")){
										gm.logScreenshot("Pass", "Colorful!!!",driver);
									} else {
										gm.logScreenshot("Fail", "Dull!!! Link is not colorful!!!!",driver);
									}
									
								} else {
									gm.logScreenshot("info", "Selected link is not to be highlighted!!!",driver);
								}
								
								
							}
					
										
					/**6991.5*/
					if(links.equals("Home")){//because home has different url
						links = "Dashboard";
					} else { if(links.equals("Post a Task"))
						links = "PostTask";
					}
					
					/**6991.6 to 13*/
					vl.commonVerification(driver, links.replace(" ", ""), flag, value1, value2, value3, value4);
							
				}
			}//if click
			  
	}
	
//=========================================================================================================================================	
	
	public void commonVerification(WebDriver driver, String url, String flag, String value1, String value2, String value3, String value4) throws InterruptedException{
		
		FooterMethods fm = new FooterMethods();
		VerifyLinks vl = new VerifyLinks();
		HeaderMethods hm = new HeaderMethods();
		
		if(driver.getCurrentUrl().equals("https://liquidworkforcev2-test.accenture.com/" + url)){
			gm.logScreenshot("Pass", url + " - Redirected to https://liquidworkforcev2-test.accenture.com/" + url, driver);
			
			if(flag.contains("head")){
				/**6988.10 to 14*/
				hm.verifyHeaderContents(driver, value1, value2, value3, value4);
			} else {
				/**6991.6 to 13*/
				fm.verifyFooterContents(driver, value1, value2, value3, value4);
				
				
				
				//6991.20
				fm.footerWordings(driver);
				vl.checkTermsAndPolicies(driver);
			}
				
		} else {
			gm.logScreenshot("Fail", "Incorrect page is loaded!!!", driver);
		}
		
	}
	
//=========================================================================================================================================	
	
	/**hey this is not working get some help from others*/
	public void checkNewTab(WebDriver driver, By footerLinks, String links) throws InterruptedException{    
		
		String parentHandle = driver.getWindowHandle(); // get the current window handle
		driver.findElement(footerLinks).click(); // click some link that opens a new window
		Thread.sleep(10000);
		for (String winHandle : driver.getWindowHandles()) {
			System.out.println(parentHandle + " "  + winHandle + driver.getWindowHandles());
		    if(!parentHandle.equals(winHandle)){
		    	driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		    }//else do nothing and iterate to the next page
			
		}

		//code to do something on new window
		if(links.equals("Feedback")){
			if(driver.getCurrentUrl().equals("https://bpo-portal.accenture.com/")){
				gm.logScreenshot("Pass", "opens new tab for: " + driver.getCurrentUrl(), driver );
			} else {
				gm.logScreenshot("Info", "Maybe wrong URL!", driver );
			}
		} else {
			if(driver.getCurrentUrl().equals("https://go.accenture.com/liquidworkforce")){
				gm.logScreenshot("Pass", "opens new tab for: " + driver.getCurrentUrl(), driver );
			} else {
				gm.logScreenshot("Info", "Maybe wrong URL!", driver );
			}
		}
		
		
		driver.close(); // close newly opened window when done with it
		driver.switchTo().window(parentHandle);
     }

//=========================================================================================================================================	
	
	public void checkTermsAndPolicies(WebDriver driver) throws InterruptedException{
		
		
		VerifyLinks vl = new VerifyLinks();
		
		/**6991.21 Verify that when "Privacy Policy" is a clickable link that will redirect page to the Privacy Policy page with available BACK button at the bottom of the page.*/
		if(gm.elementExist(driver, FooterObjects.dataPrivacyStatement)){
			String previousPage = driver.getCurrentUrl();
			driver.findElement(FooterObjects.dataPrivacyStatement).click();
			Thread.sleep(2000);
			
			String url = "DataPrivacy";
			vl.commonVerification2(driver, url, previousPage, Terms_Policies_PageObjects.DataPrivacy);
			
		}
		
		/**6991.24 Verify that when "Terms & Conditions" is a clickable link that will redirect page to the Terms & Conditions page with available BACK button at the bottom of the page.*/
		if(gm.elementExist(driver, FooterObjects.termsOfUse)){
			String previousPage = driver.getCurrentUrl();
			driver.findElement(FooterObjects.termsOfUse).click();
			Thread.sleep(2000);
			
			String url = "TermsOfUse";
			vl.commonVerification2(driver, url, previousPage, Terms_Policies_PageObjects.TermsOfUse);
			
		}
	}
	
//=========================================================================================================================================	
	
	public void commonVerification2(WebDriver driver, String url, String previousPage, By whichBy) throws InterruptedException{
		
		FooterMethods fm = new FooterMethods();
		HeaderMethods hm = new HeaderMethods();
		
		if(driver.getCurrentUrl().equals("https://liquidworkforcev2-test.accenture.com/Policy/" + url) && gm.elementExist(driver, Terms_Policies_PageObjects.importantDocumentDiv) && gm.elementExist(driver, whichBy)){
			
			gm.logScreenshot("Pass", url + " appeared!", driver);
			
			/**6991.25 Verify that the Terms & Conditions landing page have header and footer when accessed through link in the footer.*/
			fm.verifyFooter(driver);
			hm.verifyHeader(driver);
			
			/**6991.26 Verify that when clicking the BACK button in the Terms & Conditions, the page will redirected back to the previous page last accessed.*/
			driver.findElement(Terms_Policies_PageObjects.backButton).click();
			Thread.sleep(2000);
			
			if(driver.getCurrentUrl().equals(previousPage)){
				gm.setLogMsg("Pass", "Back button working!!");
			} else {
				gm.setLogMsg("Pass", "Back button NOT working!!");
			}
			
		} else {
			gm.logScreenshot("Fail", url + " did not appear!", driver);
		}
	}
	
	
}
