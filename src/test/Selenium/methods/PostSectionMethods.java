package methods;

import org.openqa.selenium.WebDriver;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import pageObjects.DashboardObjects;
import pageObjects.HeaderObjects;
import org.openqa.selenium.By;

/**
 * 
 * @author jan.carlo.l.sabanal
 * @since 11/21/2016
 * Description - this is for validating the post section of the dashboard  
 * 
 * */

public class PostSectionMethods {
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();

//=========================================================================================================================================	
	
	/**7789.5  Verify whether In Progress and Completed tabs are visible to user or not.
	 * 7790.3 to 4 is covered
	 * 
	 * 
	*/
	public void verifyPostsSectionLinks(WebDriver driver) throws InterruptedException	
	{			
		if(gm.elementExist(driver, DashboardObjects.inProgressPosts(" active")) && gm.elementExist(driver, DashboardObjects.completePosts(""))){
			System.out.println("==============================yehey==============================PostsSectionLinks");
			gm.logScreenshot("Pass", "Links exist!", driver);
		} else {
			gm.logScreenshot("Fail", "Links do not exist!!!", driver);
			driver.findElement(DashboardObjects.expandPost).click();
			//driver.findElement(DashboardObjects.exposePost).click();
		}
	}
	
//=========================================================================================================================================
	
	public void clickLink(WebDriver driver) throws InterruptedException	
	{			
		checkColor(driver, DashboardObjects.inProgressPosts(" active"), "In Progress", "Dashboard");
		verifyTextFieldsExistence(driver, true);
					
		driver.findElement(DashboardObjects.completePosts("")).click();
		Thread.sleep(2000);
		checkColor(driver, DashboardObjects.completePosts(" active"), "Complete", "Dashboard/CompletedPosts");
		verifyTextFieldsExistence(driver, false);
		
		driver.findElement(DashboardObjects.inProgressPosts("")).click();//this will check AGAIN if In Progress link becomes clickable AGAIN
		Thread.sleep(2000);		
		checkColor(driver, DashboardObjects.inProgressPosts(" active"), "In Progress", "Dashboard");
	}
	
//=========================================================================================================================================
	
	public void checkColor(WebDriver driver, By selectedLink, String link, String url){
		
		String bottomColor, backColor;		
		
		if(gm.elementExist(driver, selectedLink)){//&& driver.getCurrentUrl().equals(EnvironmentVariables.URL + url)){
			System.out.println("==============================yehey==============================" + link + " link is selected");
			gm.logScreenshot("Pass", link + " link is selected!", driver);
			
			bottomColor = driver.findElement(selectedLink).getCssValue("border-bottom-color");
			backColor = driver.findElement(selectedLink).getCssValue("color");
			System.out.println(bottomColor + " " + backColor);
			
			if(bottomColor.equals("rgba(66, 180, 238, 1)")&& backColor.equals("rgba(66, 180, 238, 1)")){
				gm.logScreenshot("Pass", "Colorful!!!",driver);
			} else {
				gm.logScreenshot("Fail", "Dull!!! Link is not colorful!!!!",driver);
			}
				
		} else {
			gm.logScreenshot("Fail", link + " link is NOT selected!!!", driver);
		}
	}
	
//=========================================================================================================================================

	public void verifyTextFieldsExistence(WebDriver driver, boolean flag){
		
		for(int i = 1;i<=8;i++){/**for normal fields without duplicate class names*/
			iterateThroughTextFields(driver, DashboardObjects.postCardTaskTitle(i));	
			iterateThroughTextFields(driver, DashboardObjects.postCardEffort(i));
			iterateThroughTextFields(driver, DashboardObjects.postCardStartDate(i));
			iterateThroughTextFields(driver, DashboardObjects.postCardEndDate(i));			
			iterateThroughTextFields(driver, DashboardObjects.postCardDescription(i));
			
		}
		
		for(int i = 1;i<=16;i++){/**for fields that have duplicate class names*/
			if ( (i & 1) == 0 ) { //if even
				iterateThroughTextFields(driver, DashboardObjects.postCardContact(i));
				iterateThroughTextFields(driver, DashboardObjects.postCardActivityInfo2(i));
			} else {//if odd
				iterateThroughTextFields(driver, DashboardObjects.postCardMarketplaceName(i));
				
				if(flag)/**if checking cards under In Progress tab, do line below*/
				iterateThroughTextFields(driver, DashboardObjects.postCardActivityInfo1(i));
			}
			
		}
	}

//=========================================================================================================================================	
	public void iterateThroughTextFields(WebDriver driver, By selectedLink){
		if(gm.elementExist(driver, selectedLink)){
			gm.setLogMsg("Pass", selectedLink + " text field is present!");
		} else {
			gm.logScreenshot("Fail", selectedLink + " text field is NOT present!!",driver);
		}
	}
	//=========================================================================================================================================
	
	/**
	 * @author jan.carlo.l.sabanal
	 * Description: User Story 8671:Complete Post Detail Page
	 * This method will click each post card under Posts section, and check if it is navigated to the correct page.
	 * */
	
	public void completedPostsDetails(WebDriver driver, String taskId) throws InterruptedException{
		
		String [] url = taskId.split(",");
		
		
		for(int i = 1;i<= url.length +1;i++){
			Thread.sleep(2000);
			driver.findElement(HeaderObjects.home).click();
			
			if(!gm.elementExist(driver, DashboardObjects.inProgressPosts(" active")) && !gm.elementExist(driver, DashboardObjects.completePosts("")))
				driver.findElement(DashboardObjects.expandPost).click();
			
			driver.findElement(DashboardObjects.completePosts("")).click();
			driver.findElement(DashboardObjects.postCardTaskTitle(i)).click();
			Thread.sleep(3000);
			
			if(driver.getCurrentUrl().equals(EnvironmentVariables.URL + "Posts/" + url[i-1])){
				gm.logScreenshot("Pass", "Navigated successfully to post #" + url[i-1], driver);
			} else {
				gm.logScreenshot("Fail", "Incorrect page loaded!!",driver);
			}
			driver.findElement(HeaderObjects.home).click();
		}
			
	}
	
	
}
