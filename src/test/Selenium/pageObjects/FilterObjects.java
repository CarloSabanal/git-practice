package pageObjects;

import org.openqa.selenium.By;

public class FilterObjects {
	
	/**Filter button on post task page*/
	public static By filterButtonDisabled = By.xpath("//div[@id='divtargetcrowdcount' and contains(@style,'hidden')]/ancestor::button[@id='btntargetCrowd']");
	public static By filterButtonEnabled = By.xpath("//div[@id='divtargetcrowdcount' and contains(@style,'visible')]/ancestor::button[@id='btntargetCrowd']");
	public static By filterButtonStar = By.xpath("//button[@id='btntargetCrowd' and @disabled]//label[text()='★']");
	public static By filterButtonCrowdCount = By.xpath("//button[@id='btntargetCrowd']//div[@id='divtargetcrowdcount' and contains( @style,'visibility: visible;')]/label[@id='lbltargetcrowdcount']");
	public static By filterButtonErrorMessage = By.xpath("//div[@class='TargetReachErrorMessage' and @style='display: block;']//p[text()='Please select Target Crowd Filter.']");
	
	
	/**filter page*/
	public static By header = By.xpath("//div[@id='divtargetcrowdfilter']//label[@class='lable-nm lbltargetScreen lbltargetpeopletext']");
	public static By headerCount = By.xpath("//div[@id='divtargetcrowdfilter']//label[@class='lable-nm lbltargetScreen lbltargetpeopletext']/b[2]");
	public static By filterPanelHeader = By.xpath("//div[@id='divleftmenu']//label[@class='lable-nm lblrefineby' and text()='REFINE BY . . .']");
	public static By filterPanelStar = By.xpath("//div[@id='divleftmenu']//label[@class='lable-nm lblrefineby lblrequiredstar' and text()='★']");
	public static By filterPanelRequired = By.xpath("//div[@id='divleftmenu']//label[@class='lable-nm lblrefineby' and text()='REQUIRED']");
	public static By applyAndClose(String index) {return By.xpath("//div[@id='tableftmenu-pane-"+index+"']//button[@id='btnapplyandclose']");}
	public static By applyAndNext(String index){return By.xpath("//div[@id='tableftmenu-pane-"+index+"']//button[@id='btnapplyandnext']");}
	public static By done = By.xpath("//button[@id='btndone']");
	
	/**language*/
	public static By languageHeader = By.xpath("//*[@id='tableftmenu-pane-0' and @aria-hidden='false']/div/p[text()='Language Proficiency Requirements']");
	public static By filterLanguageSelected = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Language']/ancestor::a[@aria-selected='true']");
	public static By filterLanguageUnselected = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Language']/ancestor::a[@aria-selected='false']");
	public static By languageIcon = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Language']/ancestor::a/img[contains(@src,'icn_language.png')]");
	public static By languageStar = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Language']/ancestor::a/label[text()='★']");
	public static By languageCheck = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Language']/ancestor::a/span[@style='display: inline;']/i[@class='fa fa-check']");
	public static By languagePageText = By.xpath("//div[@id='tableftmenu-pane-0']//div//span[text()='Candidate should be proficient in ']");
	public static By radioAll = By.xpath("//input[@id='all']/ancestor::span/label");
	public static By languageField = By.xpath("//p[text()='Language Proficiency Requirements']/ancestor::div//div[@class='Select-placeholder' and text()='Search to add language']");
	public static By languageFieldInput= By.xpath("//p[text()='Language Proficiency Requirements']/ancestor::div[@class='col-md-9']//span[@class='Select-multi-value-wrapper']//input");
	public static By languageComboBox(String lang){return By.xpath("//p[text()='Language Proficiency Requirements']/ancestor::div[@class='col-md-9']//div[@role='listbox']//div[@role='option' and text()='"+lang+"']");}
	public static By languageDeleteSelected= By.xpath("//p[text()='Language Proficiency Requirements']/ancestor::div[@class='col-md-9']//span[@class='Select-multi-value-wrapper']/div[@class='Select-value']/span[@class='Select-value-icon'][1]");
	public static By languageFilterErrorMessage = By.xpath("//div[@class='LocationvalidationErrorMessage' and contains( @style,'display: none;')]/p[text()='This is a required filter.']");
	
	/**Resource Loc*/
	public static By resourceLocHeader = By.xpath("//*[@id='tableftmenu-pane-1' and @aria-hidden='false']/div/p[text()='Resource Location Requirements']");
	public static By resourceLocPageText = By.xpath("//div[@id='tableftmenu-pane-1']/div/div/span[text()='Only people in the selected locations will be targeted. Select at least one or “Any.” ']");
	public static By filterResourceLocSelected = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Resource Location']/ancestor::a[@aria-selected='true']");
	public static By filterResourceLocUnselected = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Resource Location']/ancestor::a[@aria-selected='false']");
	public static By resourceLocIcon = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Resource Location']/ancestor::a/img[contains(@src,'icn_res_location.png')]");
	public static By resourceLocStar = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Resource Location']/ancestor::a/label[text()='★']");
	public static By resourceLocCheck = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Resource Location']/ancestor::a/span[@style='display: inline;']/i[@class='chk_filterico fa fa-check']");
	public static By resourceLocAnyCheckBox = By.xpath("//*[@id='tableftmenu-pane-1']//a[@title='Any']/preceding-sibling::span[@class='rc-tree-checkbox rc-tree-checkbox-checked']");
	public static By resourceLocCheckBox(String loc){return By.xpath("//*[@id='tableftmenu-pane-1']//a[contains(@title,'"+loc+"')]/preceding-sibling::span[@class='rc-tree-checkbox']");}//contain is used because data comes from MRDR, space are not trimmed
	public static By resourceLocExpand(String loc){return By.xpath("//*[@id='tableftmenu-pane-1']//a[contains(@title,'"+loc+"')]/preceding-sibling::span[@class='rc-tree-switcher rc-tree-noline_close']");}//contain is used because data comes from MRDR, space are not trimmed
	public static By ResourceLocationvalidationErrorMessage = By.xpath("//div[@class='ResourceLocationvalidationErrorMessage' and contains( @style,'display: none;')]/p[text()='This is a required filter.']");
	
	/**Career Level*/
	public static By careerLevelHeader = By.xpath("//*[@id='tableftmenu-pane-2' and @aria-hidden='false']/div/p[text()='Career Level Requirements']");
	public static By careerLevelPageText = By.xpath("//div[@id='tableftmenu-pane-2']/div/p[text()='Only people matching the selected levels will be targeted. Select at least one or \"Any\".']");
	public static By filterCareerLevelSelected = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Career Level']/ancestor::a[@aria-selected='true']");
	public static By filterCareerLevelUnselected = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Career Level']/ancestor::a[@aria-selected='false']");
	public static By careerLevelIcon = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Career Level']/ancestor::a/img[contains(@src,'icn_career_level.png')]");
	public static By careerLevelStar = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Career Level']/ancestor::a/label[text()='★']");
	public static By careerLevelCheck = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Career Level']/ancestor::a/span[@style='display: inline;']/i[@class='fa fa-check']");
	public static By careerLevelCheckBox(String level){return By.xpath("//input[@id='"+ level +"']/ancestor::li/label/span");}
	public static By careerLevelCheckBoxDisabled(String level){return By.xpath("//input[@id='"+ level +"' and @disabled]");}
	public static By careerLevelFilterErrorMessage = By.xpath("//div[@class='CareerLevelvalidationErrorMessage' and contains( @style,'display: none;')]/p[text()='This is a required filter.']");

	/**Talent Segment*/
	public static By talentSegmentHeader = By.xpath("//*[@id='tableftmenu-pane-3' and @aria-hidden='false']/div/p[text()='Talent Segment Requirements']");
	public static By talentSegmentPageText = By.xpath("//div[@id='tableftmenu-pane-3']/div/div/span[text()='Only people in the selected talent segment will be targeted. Select at least one or “Any.” ']");
	public static By filterTalentSegmentSelected = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Talent Segment']/ancestor::a[@aria-selected='true']");
	public static By filterTalentSegmentUnselected = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Talent Segment']/ancestor::a[@aria-selected='false']");
	public static By talentSegmentIcon = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Talent Segment']/ancestor::a/img[contains(@src,'icn_talent_segment.png')]");
	public static By talentSegmentStar = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Talent Segment']/ancestor::a/label[text()='★']");
	public static By talentSegmentCheck = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Talent Segment']/ancestor::a/span[@style='display: inline;']/i[@class='chk_filterico fa fa-check']");
	public static By talentSegmentAnyCheckBox = By.xpath("//*[@id='tableftmenu-pane-3']//a[@title='Any']/preceding-sibling::span[@class='rc-tree-checkbox rc-tree-checkbox-checked']");
	public static By talentSegmentCheckBox(String talent){return By.xpath("//*[@id='tableftmenu-pane-3']//a[contains(@title,'"+talent+"')]/preceding-sibling::span[@class='rc-tree-checkbox']");}//contain is used because data comes from MRDR, space are not trimmed
	public static By talentSegmentExpand(String talent){return By.xpath("//*[@id='tableftmenu-pane-3']//a[contains(@title,'"+talent+"')]/preceding-sibling::span[@class='rc-tree-switcher rc-tree-noline_close']");}//contain is used because data comes from MRDR, space are not trimmed
	public static By TalentSegmentvalidationErrorMessage = By.xpath("//div[@class='TalentSegmentvalidationErrorMessage' and contains( @style,'display: none;')]/p[text()='This is a required filter.']");
	
	/**Org unit*/
	public static By orgUnitHeader = By.xpath("//*[@id='tableftmenu-pane-4' and @aria-hidden='false']/div/p[text()='Organization Unit Requirements']");
	public static By orgUnitPageText = By.xpath("//*[@id='tableftmenu-pane-4']//div[@class='filter-subtitle']/span[text()='Only people in the selected organization units will be targeted. Select at least one or “Any.” ']");
	public static By filterOrgUnitSelected = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Organization Unit']/ancestor::a[@aria-selected='true']");
	public static By filterOrgUnitUnselected = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Organization Unit']/ancestor::a[@aria-selected='false']");
	public static By orgUnitIcon = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Organization Unit']/ancestor::a/img[contains(@src,'icn_org_unit.png')]");
	public static By orgUnitStar = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Organization Unit']/ancestor::a/label[text()='★']");
	public static By orgUnitCheck = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Organization Unit']/ancestor::a/span[@style='display: inline;']/i[@class='fa fa-check']");
	public static By orgUnitCheckBox(String unit){return By.xpath("//*[@id='tableftmenu-pane-4']//a[@title='"+unit+"']/preceding-sibling::span[@class='rc-tree-checkbox']");}
	public static By orgUnitAnyCheckBox = By.xpath("//*[@id='tableftmenu-pane-4']//a[@title='Any']/preceding-sibling::span[@class='rc-tree-checkbox rc-tree-checkbox-checked']");
	public static By orgUnitExpand(String unit){return By.xpath("//*[@id='tableftmenu-pane-4']//a[@title='"+unit+"']/preceding-sibling::span[@class='rc-tree-switcher rc-tree-noline_close']");}
	public static By orgLevelFilterErrorMessage = By.xpath("//div[@class='OrgUnitvalidationErrorMessage' and contains( @style,'display: none;')]/p[text()='This is a required filter.']");
	
	/**Specialty*/
	public static By specialtyHeader = By.xpath("//*[@id='tableftmenu-pane-5' and @aria-hidden='false']/div/p[text()='Specialty Requirements']");
	public static By specialtyPageText = By.xpath("//div[@id='tableftmenu-pane-5']//div/span[text()='Only people with the selected specialties will be targeted. Select at least one. ']");
	public static By filterSpecialtySelected = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Specialty']/ancestor::a[@aria-selected='true']");
	public static By filterSpecialtyUnselected = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Specialty']/ancestor::a[@aria-selected='false']");
	public static By specialtyIcon = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Specialty']/ancestor::a/img[contains(@src,'icn_specialty.png')]");
	public static By specialtyStar = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Specialty']/ancestor::a/label[text()='★']");
	public static By specialtyCheck = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Specialty']/ancestor::a/span[@style='display: inline;']/i[@class='fa fa-check']");
	public static By specialtyField = By.xpath("//p[text()='Specialty Requirements']/ancestor::div//div[@class='Select-placeholder' and text()='Search to add specialty']");
	public static By specialtyFieldInput= By.xpath("//p[text()='Specialty Requirements']/ancestor::div[@id='tableftmenu-pane-5']//span[@class='Select-multi-value-wrapper']//input");
	public static By specialtyComboBox(String spec){return By.xpath("//p[text()='Specialty Requirements']/ancestor::div[@id='tableftmenu-pane-5']//div[@role='listbox']//div[@role='option' and text()='"+spec+"']");}
	public static By specialtyDeleteSelected= By.xpath("//p[text()='Specialty Requirements']/ancestor::div[@id='tableftmenu-pane-5']//span[@class='Select-multi-value-wrapper']/div[@class='Select-value']/span[@class='Select-value-icon'][1]");
	public static By SpecialtyvalidationErrorMessage = By.xpath("//div[@class='SpecialtyvalidationErrorMessage' and contains( @style,'display: none;')]/p[text()='This is a required filter.']");
	
	
	
	public static By filterMarketAttributesUnselected = By.xpath("//div[@id='divleftmenu']//ul//label[text()='Marketplace Attributes']/ancestor::a[@aria-selected='false']");
	
	
	
	
	
}

