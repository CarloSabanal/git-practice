/**
 * 
 */
package scripts;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import methods.ALW_Methods;
import methods.DashboardMethod;
import methods.SearchPageMethods;
import pageObjects.DashboardObjects;
import pageObjects.PostTaskObjects;
import pageObjects.SearchPageObjects;
/**
 * @author somnath.bhunia
 * @since 2/20/2017
 */
public class SearchPage
{
	WebDriver driver = null;
	GenericMethods gm = new GenericMethods();
	DashboardMethod dm = new DashboardMethod();
	ALW_Methods am = new ALW_Methods();
	SearchPageMethods sm = new SearchPageMethods();


	@BeforeClass()
	public void loadURL() throws InterruptedException	
	{
		System.setProperty(EnvironmentVariables.driverType, EnvironmentVariables.driverPath);
		driver = new ChromeDriver();
		//driver = new InternetExplorerDriver();
		driver.manage().window().maximize();
		driver.navigate().to(EnvironmentVariables.URL);
		gm.setLogMsg("Info", "Successfully launched Chrome");

	}

	//************************************************************************************************************************************************************************************************************************

	@Test(priority = 0, description = "Landing inside the Search Results page")
	public void searchPageByButton()throws InterruptedException
	{
		Thread.sleep(10000);
		am.login(driver, EnvironmentVariables.USERNAME, EnvironmentVariables.PASSWORD);
		gm.setLogMsg("Info", "Successful Login");
		Thread.sleep(10000);
		sm.typeAndSearch(driver, "Test");
		Thread.sleep(5000);
		if(driver.getCurrentUrl().contains("SearchResults"))
		{
			gm.setLogMsg("Pass", "Successfully navigated to Search Results page");
			gm.logScreenshot("Pass", "Able to navigate to Search Results", driver);
		}
		else
		{

			gm.setLogMsg("Fail", "Navigation to the Search Results page was unsuccessful");
			gm.logScreenshot("Fail", "Unable to navigate to Search Results", driver);
		}

	}

	//****************************************************************************************************************************************************************

	@Test(priority = 1, description = "Screening search results using filters", dataProvider = "TestData")
	@Parameters("sheetname")
	public void searchFilters(String flag, String scenario, String comment1, String comment2, String userName, String password, String marketplaceFilter,
			String categoryFilter, String taskStartFilter, String effortFilter, String urgent, String New, String approval, String WBS,
			String WBSChargeability, String Offer, String WorkLocation)throws InterruptedException
	{
		driver.findElement(SearchPageObjects.searchFilterButton).click();
		Thread.sleep(2000);

		if(flag.equalsIgnoreCase("y"))
		{
			int i=1;
			System.out.println("I am here at "+i);
			i++;
			while(!(taskStartFilter.equalsIgnoreCase("null")))
			{
				System.out.println("I have passed ");
				if(gm.elementExist(driver, SearchPageObjects.searchStartDate))
				{
					System.out.println("I am here at the innermost layer "+i);
					gm.setLogMsg("Info", "Task start Date dropdown is visible");
					sm.selectFromDropdown(driver, SearchPageObjects.searchStartDate, taskStartFilter);
					System.out.println("I am here at the bottom"+i);
					break;
				}
				else
				{
					System.out.println("I am here after failing ");
					gm.logScreenshot("Fail", "Task start date dropdown is not visible", driver);
				}
			}

			driver.findElement(SearchPageObjects.applyButton).click();



				/*driver.findElement(By.xpath("//*[@id='divfilterleft']/div[1]/div[1]/span/div/ul[2]/li[1]/a/label")).click(); //click 'Select all' for the first time
		sm.selectFromDropdown(driver, SearchPageObjects.searchMarketplace, " CrowdUp");
		sm.selectFromDropdown(driver, SearchPageObjects.searchMarketplace, " Procurement Network");
		sm.selectFromDropdown(driver, SearchPageObjects.searchMarketplace, " Wrong option");*/

				/*sm.selectFromDropdown(driver, SearchPageObjects.searchStartDate, "This week");
		sm.selectFromDropdown(driver, SearchPageObjects.searchEffort, "Less than 1 day");*/

				//sm.selectCheckBox(driver, SearchPageObjects., "Pass");
		}
	}

	//****************************************************************************************************************************************************************************************************************************************************

	@DataProvider(name = "TestData")
	public Object[][] getValueFromExcel(ITestContext context)
	{
		String sheetName = "SearchResults";// context.getCurrentXmlTest().getParameter("sheetName").toString().trim();
		Object[][] data = gm.getExcelData(EnvironmentVariables.dataPoolPath, sheetName);		
		return data;
	}

	//*****************************************************************************************************************************************************************************************************************************************************

	@AfterClass
	public void closeTheAction() throws InterruptedException
	{
		Thread.sleep(3000);
		JOptionPane.showMessageDialog(null,"Execution has completed. To close the browser, press ok");
		//driver.close();
		gm.setLogMsg("info", "End of Post a task Test");		
	}

}
