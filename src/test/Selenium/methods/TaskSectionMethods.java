package methods;

import org.openqa.selenium.WebDriver;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import pageObjects.DashboardObjects;
import pageObjects.HeaderObjects;
import org.openqa.selenium.By;

/**
 * 
 * @author jan.carlo.l.sabanal
 * @since 12/02/2016
 * Description - this is for validating the TASK section of the dashboard  
 * Close
 * User Story 8805:Display contents of Task Section on Dashboard
 * */

public class TaskSectionMethods {
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();

//=========================================================================================================================================	
	
	
	public void verifyTasksSectionLinks(WebDriver driver) throws InterruptedException	
	{			
		if(gm.elementExist(driver, DashboardObjects.inProgressTasks(""))&& gm.elementExist(driver, DashboardObjects.completeTasks(""))&&gm.elementExist(driver, DashboardObjects.appliedTasks(""))&& gm.elementExist(driver, DashboardObjects.suggestedTasks(" active"))){
			System.out.println("==============================yehey==============================TasksSectionLinks");
			gm.logScreenshot("Pass", "Links exist!", driver);
		} else {
			gm.logScreenshot("Fail", "Links do not exist!!!", driver);
			driver.findElement(DashboardObjects.expandTask).click();
		}
	}
	
//=========================================================================================================================================
	
	public void clickLink(WebDriver driver, String taskId) throws InterruptedException	
	{			
		
		String[] url = taskId.split(",");
		
		checkColor(driver, DashboardObjects.suggestedTasks(" active"), "Suggested");
		verifyTextFieldsExistence(driver);
		for(int i=0;i<url.length;i++){
			tasksDetails(driver,url[i]);
		}
		/** user has no tasks in other pages
		
		driver.findElement(DashboardObjects.inProgressTasks("")).click();
		Thread.sleep(2000);
		checkColor(driver, DashboardObjects.inProgressTasks(" active"), "In Progress");
		verifyTextFieldsExistence(driver);
		tasksDetails(driver,url[1]);
		
		driver.findElement(DashboardObjects.appliedTasks("")).click();
		Thread.sleep(2000);		
		checkColor(driver, DashboardObjects.appliedTasks(" active"), "Applied");
		verifyTextFieldsExistence(driver);
		tasksDetails(driver,url[2]);
		
		driver.findElement(DashboardObjects.completeTasks("")).click();
		Thread.sleep(2000);		
		checkColor(driver, DashboardObjects.completeTasks(" active"), "Completed");
		verifyTextFieldsExistence(driver);
		tasksDetails(driver,url[3]);
		*/
	}
	
//=========================================================================================================================================
	
	public void checkColor(WebDriver driver, By selectedLink, String link){
		
		String bottomColor, backColor;		
		
		if(gm.elementExist(driver, selectedLink)){//&& driver.getCurrentUrl().equals(EnvironmentVariables.URL + url)){
			System.out.println("==============================yehey==============================" + link + " link is selected");
			gm.logScreenshot("Pass", link + " link is selected!", driver);
			
			bottomColor = driver.findElement(selectedLink).getCssValue("border-bottom-color");
			backColor = driver.findElement(selectedLink).getCssValue("color");
			System.out.println(bottomColor + " " + backColor);
			
			if(bottomColor.equals("rgba(66, 180, 238, 1)")&& backColor.equals("rgba(66, 180, 238, 1)")){
				gm.logScreenshot("Pass", "Colorful!!!",driver);
			} else {
				gm.logScreenshot("Fail", "Dull!!! Link is not colorful!!!!",driver);
			}
				
		} else {
			gm.logScreenshot("Fail", link + " link is NOT selected!!!", driver);
		}
	}
	
//=========================================================================================================================================

	public void verifyTextFieldsExistence(WebDriver driver){
		
		for(int i = 1;i<=8;i++){/**for normal fields without duplicate class names*/
			iterateThroughTextFields(driver, DashboardObjects.taskCardTaskTitle(i));	
			iterateThroughTextFields(driver, DashboardObjects.taskCardEffort(i));
			iterateThroughTextFields(driver, DashboardObjects.taskCardStartDate(i));
			iterateThroughTextFields(driver, DashboardObjects.taskCardEndDate(i));			
			iterateThroughTextFields(driver, DashboardObjects.taskCardDescription(i));
			iterateThroughTextFields(driver, DashboardObjects.taskCardActivityInfo(i));
		}
		
		for(int i = 1;i<=16;i++){/**for fields that have duplicate class names*/
			if ( (i & 1) == 0 ) { //if even
				iterateThroughTextFields(driver, DashboardObjects.taskCardContact(i));
			} else {//if odd
				iterateThroughTextFields(driver, DashboardObjects.taskCardMarketplaceName(i));
				
				
			}
			
		}
	}

//=========================================================================================================================================	
	public void iterateThroughTextFields(WebDriver driver, By selectedLink){
		if(gm.elementExist(driver, selectedLink)){
			gm.setLogMsg("Pass", selectedLink + " text field is present!");
		} else {
			gm.logScreenshot("Fail", selectedLink + " text field is NOT present!!",driver);
		}
	}
	//=========================================================================================================================================
	
	/**
	 * @author jan.carlo.l.sabanal
	 * Description: User Story 8671:Complete Post Detail Page
	 * This method will click each post card under Posts section, and check if it is navigated to the correct page.
	 * */
	
	public void tasksDetails(WebDriver driver, String url) throws InterruptedException{
				
			Thread.sleep(2000);
			
			
			if(!gm.elementExist(driver, DashboardObjects.inProgressTasks(""))&& !gm.elementExist(driver, DashboardObjects.completeTasks(""))&& !gm.elementExist(driver, DashboardObjects.appliedTasks(""))&& !gm.elementExist(driver, DashboardObjects.suggestedTasks(" active")))
				driver.findElement(DashboardObjects.expandTask).click();
			
		
			driver.findElement(DashboardObjects.taskCardTaskTitle(1)).click();
			Thread.sleep(3000);
			
			if(driver.getCurrentUrl().equals(EnvironmentVariables.URL + "tasks/" + url)){
				gm.logScreenshot("Pass", "Navigated successfully to task #" + url, driver);
			} else {
				gm.logScreenshot("Fail", "Incorrect page loaded!!",driver);
			}
			
			driver.findElement(HeaderObjects.home).click();
		
			
	}
	
	
}
