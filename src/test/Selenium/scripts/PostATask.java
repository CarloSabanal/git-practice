package scripts;

import javax.swing.JOptionPane;

/**5 minutes execution time*/

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.openqa.selenium.JavascriptExecutor;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import methods.ALW_Methods;
import methods.ApplicantMethods;
import methods.FooterMethods;
import methods.JobPosterActionsInTaskDetailsPageMethods;
import methods.PostSectionMethods;
import methods.PostTaskMethods;
import methods.ValidateTaskDetailsMethods;
import pageObjects.FilterObjects;
import pageObjects.PostTaskObjects;
import pageObjects.TaskDetailsObjects;


public class PostATask {
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();
	PostTaskMethods ptm = new PostTaskMethods();
	ALW_Methods am = new ALW_Methods();
	ValidateTaskDetailsMethods tdm = new ValidateTaskDetailsMethods();
	ApplicantMethods afj = new ApplicantMethods();
	JobPosterActionsInTaskDetailsPageMethods jpa = new JobPosterActionsInTaskDetailsPageMethods();
	
	@BeforeClass()
	public void openBrowser() throws InterruptedException	
	{	System.setProperty(EnvironmentVariables.driverType, EnvironmentVariables.driverPath);
		driver = new ChromeDriver();//InternetExplorerDriver();ChromeDriver
		gm.setLogMsg("Info", "Successfully launched the browser");
		driver.manage().window().maximize();
		driver.navigate().to(EnvironmentVariables.URL);//task ID, can be variable passed by postatask script, or another column in datapool where task id is dynamically saved.
		gm.setLogMsg("info", "Starting Post a task Test");	
	}

	/**
	 * Sprint 4
	 * Description: ALW ReArch Regression - Post Task Section
	 * @author jan.carlo.l.sabanal
	 * @since 2016/12/19
	 * 
	 */
	
	@Test(priority = 0, description ="ALW",dataProvider="TestData")
	@Parameters("sheetName")
	public void testPostsTask(String flag, String validity, String comment1, String comment2, String userName, String passWord, 
			String workLocationType, String workLocation, String title, String addDetails, String charge, 
			String multiOffer, String resources, String description, String eid, String market, String category, String startHours, 
			String startMinutes, String startAMPM, String mode, String endHours, String endMinutes, String endAMPM, String timeZone, 
			String effortUnit, String effort, String startDateDay, String startDateMonth, String startDateYear, String endDateDay, 
			String endDateMonth, String endDateYear, String expireDateHour, String expireDateMinute, String expireDateAMPM, 
			String expireDateDay, String expireDateMonth, String expireDateYear, String isFilterRequired, String requiredFilters, 
			String language, String languageAny, String resourceLoc, String careerLevel, String talentSegment, String orgUnit, 
			String specialty, String skills, String marketAttributes,String restrictApplicants,String validateTaskDetails, String comment, 
			String applicant, String applicationUseCase, String applyNotes, String withdrawApp, 
			String withdrawTask, String withdrawReason, String deleteTask) throws InterruptedException	
	{
		
		if(flag.equalsIgnoreCase("y")){
			if(!passWord.equalsIgnoreCase("null")){//first iteration is for login only.
				am.login(driver, userName, passWord);
			} else {
				
				/**User Story 9513:Post a new task page from ALW tool
				 * @author jan.carlo.l.sabanal
				 * @since 2016/12/19
				 * 
				 * */
				gm.waitForObject(driver, PostTaskObjects.postATask);
				driver.findElement(PostTaskObjects.postATask).click();
				Thread.sleep(2000);
				
				if(driver.getCurrentUrl().equals(EnvironmentVariables.URL + "PostTask")){
					gm.logScreenshot("Pass", "Navigated to post task section!!", driver);
					driver.findElement(PostTaskObjects.saveButton).click();//this is to check if - Bug 12344:BUG: Validation message is displayed even the description field has a value. - occurs
					
					ptm.validateRequiredFieldsIndicator(driver);/**User Story 13109*/
					ptm.addTitle(driver,title);
					ptm.marketplace(driver,market, category, isFilterRequired);
					ptm.effort(driver,effort.replace(".0", ""),effortUnit);
					ptm.startDate(driver, startHours.replace(".0", ""), startMinutes.replace(".0", ""), startAMPM, mode, endHours.replace(".0", ""), endMinutes.replace(".0", ""), endAMPM, startDateDay.replace(".0", ""), startDateMonth, startDateYear.replace(".0", ""), endDateDay.replace(".0", ""), endDateMonth, endDateYear.replace(".0", ""), expireDateHour.replace(".0", ""), expireDateMinute.replace(".0", ""),	expireDateAMPM,	expireDateDay.replace(".0", ""), expireDateMonth,expireDateYear.replace(".0", ""));
					ptm.startWorkHours(driver,comment2, startHours.replace(".0", ""), startMinutes.replace(".0", ""), startAMPM, mode, endHours.replace(".0", ""), endMinutes.replace(".0", ""), endAMPM, timeZone);
					ptm.postOnBehalf(driver,eid);
					ptm.description(driver, description);
					ptm.additionalDetails(driver,addDetails, charge, market);
					ptm.multiOffer(driver,multiOffer,resources);
					ptm.workLocation(driver, workLocationType, workLocation, validity);
					
					/**insert here scroll up*/
					JavascriptExecutor jse = (JavascriptExecutor) driver;
				    jse.executeScript("window.scrollBy(0,-500)", "");//("arguments[0].scrollIntoView();", FilterObjects.filterButtonErrorMessage);
				    driver.findElement(PostTaskObjects.jobTitleField).click();
				    
					if(isFilterRequired.equalsIgnoreCase("yes")&&!category.equals("null")){//when marketplace require having filters. Skip this method if not required
						driver.findElement(PostTaskObjects.saveButton).click();//User Story 11591.1.2.2d
						ptm.applyFilters(driver, requiredFilters, language, languageAny, resourceLoc, careerLevel, talentSegment, orgUnit, specialty, skills, marketAttributes);
					}
					
				} else {
					gm.logScreenshot("Fail", "wrong page!!", driver);
				}
				
				gm.logScreenshot("info", "screen shot before save or go to home!!", driver);
				
				
				
				
/********************************For valid scenarios, validate the review post modal. If review modal does not display, this will log a failure and will proceed to the next iteration. Validation of Task Detail Page and Task Application is under here.*/
				if(validity.contains("valid")){
					
					System.out.println("******************************************valid******************************************");
					String targetCrowdBeforeReview = driver.findElement(FilterObjects.filterButtonCrowdCount).getText();/**gets info from filter button before opening the review modal User Story 10343:Task Poster is able to see information of the Targeted Crowd in the Post a task screen.*/
					Thread.sleep(1000);
					driver.findElement(PostTaskObjects.saveButton).click();
					Thread.sleep(1000);
					driver.findElement(PostTaskObjects.reviewCloseButton).click();
					Thread.sleep(1000);
					driver.findElement(PostTaskObjects.saveButton).click();
					
					Thread.sleep(3000);
					if(gm.elementExist(driver, PostTaskObjects.reviewModal)){//methods inside this will be dependent on the presence of review modal. in case the review modal does not appear, it will log failure and then will proceed to the next iteration.
						
						ptm.reviewPost(driver, title, requiredFilters, language, languageAny, resourceLoc, careerLevel, talentSegment, orgUnit, specialty, skills, marketAttributes, restrictApplicants,targetCrowdBeforeReview);
						String taskUrl = null;
					
/*********************************************US11082 Review My Posted Task*******************************************************************************/
						if(validateTaskDetails.equals("yes")){
							driver.findElement(PostTaskObjects.viewMyPost).click();
							Thread.sleep(5000);
							
							taskUrl = driver.getCurrentUrl();
							System.out.println(taskUrl+"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
							gm.checkThings(driver, TaskDetailsObjects.applyButton, "Expect FAILED - Poster should not see Apply buton to his own posted task");/**US12860 CONDITION 6*/
							tdm.methodsInvoker(driver, userName, workLocationType, workLocation, title, addDetails, 
									charge, multiOffer, resources, description, eid, market, category, startHours, startMinutes, startAMPM, mode, endHours, 
									endMinutes, endAMPM, timeZone, effortUnit, effort, startDateDay, startDateMonth, startDateYear, endDateDay, endDateMonth, 
									endDateYear, expireDateHour, expireDateMinute, expireDateAMPM, expireDateDay, expireDateMonth, expireDateYear, 
									isFilterRequired, requiredFilters, language, languageAny, resourceLoc, careerLevel, talentSegment, orgUnit, specialty, 
									skills, marketAttributes,restrictApplicants, comment, "yes");/**yes because he is the job poster US13326*/
													
						}else{
							driver.findElement(PostTaskObjects.home2).click();
						}
					
					
/*********************************************US11094 WITHDRAW TASK US12860 CONDITION 5*******************************************************************/
						Thread.sleep(3000);
						if(withdrawTask.equals("yes")){
							jpa.withdrawTask(driver, title, withdrawReason);
						}

/*********************************************US11093 DELETE TASK*****************************************************************************************/
						if(deleteTask.equals("yes")){
							jpa.deleteTask(driver, title,taskUrl);
						}
					
/*********************************************US11099 Apply For Job***************************************************************************************/
						if(!applicant.equals("null")){
							
							driver.close();
							driver = new ChromeDriver();//InternetExplorerDriver();ChromeDriver
							gm.setLogMsg("Info", "Successfully launched the browser");
							driver.manage().window().maximize();
							driver.navigate().to(taskUrl);
							am.login(driver, applicant, EnvironmentVariables.PASSWORD);
							Thread.sleep(5000);
							
							
							
							if(!deleteTask.equals("yes")){
								/**validate the task details using an applicant*/
								tdm.methodsInvoker(driver, userName, workLocationType, workLocation, title, addDetails, 
										charge, multiOffer, resources, description, eid, market, category, startHours, startMinutes, startAMPM, mode, endHours, 
										endMinutes, endAMPM, timeZone, effortUnit, effort, startDateDay, startDateMonth, startDateYear, endDateDay, endDateMonth, 
										endDateYear, expireDateHour, expireDateMinute, expireDateAMPM, expireDateDay, expireDateMonth, expireDateYear, 
										isFilterRequired, requiredFilters, language, languageAny, resourceLoc, careerLevel, talentSegment, orgUnit, specialty, 
										skills, marketAttributes,restrictApplicants, comment, "no");/**no because he is the applicant US13326*/
								
								afj.applyForJob(driver, applyNotes, addDetails, applicationUseCase);
								
								if(withdrawApp.equals("yes"))
									afj.withdrawApp(driver, withdrawReason, title);/**US11100*/
							} else {
								if(driver.getCurrentUrl().equals(EnvironmentVariables.URL + "DashboardMain"))
									gm.setLogMsg("Pass", "Task is successfully deleted and cannot be accessed - " + taskUrl);
								else
									gm.setLogMsg("Fail", "Wrong URL - " + driver.getCurrentUrl());
							}
							
							
							driver.close();
							driver = new ChromeDriver();//InternetExplorerDriver();ChromeDriver
							gm.setLogMsg("Info", "Successfully launched the browser");
							driver.manage().window().maximize();
							driver.navigate().to(taskUrl);
							am.login(driver, userName, EnvironmentVariables.PASSWORD);
							Thread.sleep(5000);
							
							
							
							if(!deleteTask.equals("yes")){
								String taskStatus = driver.findElement(TaskDetailsObjects.taskStatus).getText();
								/**insert here job status validation, sprint 8*/
							} else {
								if(driver.getCurrentUrl().equals(EnvironmentVariables.URL + "DashboardMain"))
									gm.setLogMsg("Pass", "Task is successfully deleted and cannot be accessed - " + taskUrl);
								else
									gm.setLogMsg("Fail", "Wrong URL - " + driver.getCurrentUrl());
							}
							
							
						}
					}//if review modal is not present
					 else {
						gm.logScreenshot("Fail", "No review modal, cannot post the task!!! There could be an error message!!!", driver);
						driver.navigate().to(EnvironmentVariables.URL);
					}
					
/*********************************************if unsuccessful posting of task is expected*****************************************************************/
				} else {
					/**insert method here that will check start over post*/
					
					System.out.println("******************************************invalid******************************************");
					driver.findElement(PostTaskObjects.saveButton).click();
					Thread.sleep(2000);					
					if(gm.elementExist(driver,PostTaskObjects.reviewModal)){
						gm.logScreenshot("Fail", "Post must not be posted!!!", driver);
						driver.findElement(PostTaskObjects.reviewCloseButton).click();
						driver.findElement(PostTaskObjects.home).click();
						Thread.sleep(5000);
					} else {
						JOptionPane.showMessageDialog(null,"Please check errors manually, then press ok");
						gm.logScreenshot("Pass", "error expected!!!", driver);
						//driver.navigate().to(EnvironmentVariables.URL);
						driver.findElement(PostTaskObjects.home).click();
					}
					Thread.sleep(5000);
				}
			}
			
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	@DataProvider(name = "TestData")
	public Object[][] getValueFromExcel(ITestContext context)
	{
		String sheetName = "postTask";//PostValidTasks,postTask
		Object[][] data = gm.getExcelData(EnvironmentVariables.dataPoolPath, sheetName);		
		return data;
	}
	

	@AfterClass
	public void closeTheAction() throws InterruptedException
	{
		Thread.sleep(5000);
		JOptionPane.showMessageDialog(null,"Execution is done. To close browser, press ok");
		//driver.close();
		gm.setLogMsg("info", "End of Post a task Test");		
	}
}