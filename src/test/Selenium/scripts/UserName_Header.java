/**
 * 
 */
package scripts;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import org.testng.ITestContext;
import org.testng.annotations.Test;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import methods.ALW_Methods;
import methods.HeaderMethods;
import methods.VerifyLinksMethods;
import pageObjects.HeaderObjects;
import pageObjects.UserNameObjects;


/**This script is for User Story - 13714 and 13715 :- User is able to see options on clicking his/her name in the header and
 * is able to navigate to different pages upon clicking respective links.
 * @author somnath.bhunia
 * @since 2/16/2017
 */
public class UserName_Header 
{
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();
	HeaderMethods hm = new HeaderMethods();
	ALW_Methods am = new ALW_Methods();
	VerifyLinksMethods vlm = new VerifyLinksMethods(); 
	
	@BeforeTest()
	public void loadURL()
	{
		System.setProperty(EnvironmentVariables.driverType, EnvironmentVariables.driverPath);
		driver = new ChromeDriver();//InternetExplorerDriver();ChromeDriver
		gm.setLogMsg("Info", "Successfully launched the browser");
		driver.manage().window().maximize();
		driver.navigate().to(EnvironmentVariables.URL);
		gm.setLogMsg("info", "Successfully launched the application");
	}
	
	@Test(priority=1, description="UserName contains expected navigable links")
	public void testUserNameLink()throws InterruptedException
	{
		am.login(driver, EnvironmentVariables.USERNAME, EnvironmentVariables.PASSWORD);
		hm.verifyUserName(driver, EnvironmentVariables.USERNAME);
		driver.findElement(UserNameObjects.userNameButton).click();
		Thread.sleep(2000);
		if(gm.elementExist(driver, UserNameObjects.userImage))
		{
			gm.setLogMsg("Pass", "The user's image name is displayed");
		}
		else
		{
			gm.logScreenshot("Fail", "The user's image is not present", driver);
		}
		
		if(gm.elementExist(driver, UserNameObjects.userNameText))
		{
			gm.setLogMsg("Pass", "The user name is displayed");
		}
		else
		{
			gm.logScreenshot("Fail", "The user name is not present", driver);
		}
		
		if(gm.elementExist(driver, UserNameObjects.userProfileButton))
		{
			gm.setLogMsg("Pass", "The 'View Profile' button is displayed");
		}
		else
		{
			gm.logScreenshot("Fail", "The 'View Profile' button is not present", driver);
		}
		
		if(gm.elementExist(driver, UserNameObjects.userFeedback))
		{
			gm.setLogMsg("Pass", "The user feedback link is displayed");
		}
		else
		{
			gm.logScreenshot("Fail", "The user feedback link is not present", driver);
		}
		
		if(gm.elementExist(driver, UserNameObjects.userSignOut))
		{
			gm.setLogMsg("Pass", "The 'Sign Out' link is displayed");
		}
		else
		{
			gm.logScreenshot("Fail", "The 'Sign Out' link is not present", driver);
		}
		
		hm.verifyUserName(driver, driver.findElement(UserNameObjects.userNameText).getText());
		vlm.checkNewTab(driver, UserNameObjects.userFeedback, "FEEDBACK");
	}

}
