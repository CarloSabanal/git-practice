package methods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import pageObjects.DashboardObjects;
import pageObjects.FilterObjects;
import pageObjects.HeaderObjects;
import pageObjects.PostTaskObjects;
import pageObjects.TaskDetailsObjects;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;

/**User Story 11082: User can access the Task Details read mode screen and see the details section.
 * @author jan.carlo.l.sabanal
 * @since 1/27/2017
 * 
 * 
*/

public class ValidateTaskDetailsMethods {
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();
	PostTaskMethods ptm = new PostTaskMethods();
	
//=========================================================================================================================================	

	
	public  void methodsInvoker(WebDriver driver, String userName, 
			String workLocationType, String workLocation, String title, String addDetails, String charge, 
			String multiOffer, String resources, String description, String eid, String market, String category, String startHours, 
			String startMinutes, String startAMPM, String mode, String endHours, String endMinutes, String endAMPM, String timeZone, 
			String effortUnit, String effort, String startDateDay, String startDateMonth, String startDateYear, String endDateDay, 
			String endDateMonth, String endDateYear, String expireDateHour, String expireDateMinute, String expireDateAMPM, 
			String expireDateDay, String expireDateMonth, String expireDateYear, String isFilterRequired, String requiredFilters, 
			String language, String languageAny, String resourceLoc, String careerLevel, String talentSegment, String orgUnit, String specialty, String skills, 
			String marketAttributes,String restrictApplicants, String comment, String poster) throws InterruptedException	
	{
		
		gm.setLogMsg("Info", "Starting validation");
		
		if(poster.equals("yes"))/**validate buttons only if job poster - US13326*/
			validateButtons(driver);
		
		insertValidateComment(driver,comment);
		validateTaskHeader(driver, title, userName);
		validateAdditionalDetails1(driver, addDetails, charge);
		validateAdditionalDetails2(driver, multiOffer, resources, workLocationType, workLocation );
		validateSubHeader(driver, market, category, effort, effortUnit);
		validateDates(driver, startHours.replace(".0", ""), startMinutes.replace(".0", ""), startAMPM, endHours.replace(".0", ""), endMinutes.replace(".0", ""), endAMPM, startDateDay.replace(".0", ""), startDateMonth, startDateYear.replace(".0", ""), endDateDay.replace(".0", ""), endDateMonth, endDateYear.replace(".0", ""), expireDateHour.replace(".0", ""), expireDateMinute.replace(".0", ""),	expireDateAMPM,	expireDateDay.replace(".0", ""), expireDateMonth,expireDateYear.replace(".0", ""));
		//validateDescription(driver, description); manual checking for now
		validateWorkTime(driver,startHours.replace(".0", ""), startMinutes.replace(".0", ""), startAMPM, endHours.replace(".0", ""), endMinutes.replace(".0", ""), endAMPM, timeZone);
		validatePostingOnBehalf(driver, eid);
		
		
		if(isFilterRequired.equalsIgnoreCase("yes")){
			validateAppliedFilters(driver, requiredFilters, language, languageAny, resourceLoc, careerLevel, talentSegment, orgUnit, specialty, skills, marketAttributes);
		} else  {
			gm.checkHiddenThings(driver, TaskDetailsObjects.languageFilters, "There should be no Language filters diplayed.");
			gm.checkHiddenThings(driver, TaskDetailsObjects.careerLevelFilters, "There should be no Career Level filters diplayed.");
			gm.checkHiddenThings(driver, TaskDetailsObjects.orgUnitFilters, "There should be no Organization Unit filters diplayed.");
			gm.checkHiddenThings(driver, TaskDetailsObjects.talentSegmentFilters, "There should be no Talent Segment filters diplayed.");
			gm.checkHiddenThings(driver, TaskDetailsObjects.resourceLocFilters, "There should be no Resource Location filters diplayed.");
			gm.checkHiddenThings(driver, TaskDetailsObjects.specialtyFilters, "There should be no Specialty filters diplayed.");
		}
		
	}
	
//=========================================================================================================================================	
	/**1.1.1.1 to 1.1.1.3*/
	public void validateTaskHeader(WebDriver driver, String title, String userName) throws InterruptedException	
	{
		String taskDetailTitle = null;
		
		if(gm.elementExist(driver, TaskDetailsObjects.newTag))
			taskDetailTitle = driver.findElement(TaskDetailsObjects.taskTitle).getText().replaceAll("New", "");
		else
			taskDetailTitle = driver.findElement(TaskDetailsObjects.taskTitle).getText();
		
		if(taskDetailTitle.contains(title)){//equals method is not working for this one, I dunno why
			gm.setLogMsg("Pass", "Title is correct!");
		}
		else{
			gm.logScreenshot("Fail", "Title is incorrect! - " + taskDetailTitle  , driver);
			System.out.println(taskDetailTitle);
			System.out.println(title);
		}
			
			
		String poster = driver.findElement(TaskDetailsObjects.poster).getText().replaceAll("Posted by - ", "");
		
		
		if(poster.contains(userName))
			gm.setLogMsg("Pass", "Poster info is correct!");
		else
			gm.logScreenshot("Fail", "Poster info is incorrect! " + poster + " - " + userName, driver);
		
		
		String postedDate = driver.findElement(TaskDetailsObjects.postedDate).getText();
	    Date today = Calendar.getInstance().getTime();
	    SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
	    String systemTime = formatter.format(today);
	    
	    if(postedDate.contains(systemTime.toUpperCase())){
	    	if(gm.elementExist(driver, TaskDetailsObjects.newTag))
	    		gm.setLogMsg("Pass", "New tag is visible");
	    	else
	    		gm.logScreenshot("Fail", "New tag is not visible" + postedDate + " " + systemTime,driver);
	    } else {
	    	if(gm.elementExist(driver, TaskDetailsObjects.newTag))
	    		gm.setLogMsg("Fail", "More than 24 hours passed - new tag is visible" + postedDate + " " + systemTime);
	    	else
	    		gm.logScreenshot("Pass", "New tag is not visible since more than 24 hours passed",driver);
	    }
	    	
	}
	
//=========================================================================================================================================
	/**1.1.2.4 to 1.1.2.7*/
	public void validateAdditionalDetails1(WebDriver driver, String addDetails, String charge) throws InterruptedException	
	{			
		if(!addDetails.equals("null")){
			String[] selectedDetails = addDetails.split(",");
			for(int i = 0;i<selectedDetails.length;i++){
				
				
				if(selectedDetails[i].equals("Supervisor Approval Required")){
					if(gm.elementExist(driver, TaskDetailsObjects.supervisorInfo)){
						gm.checkThings(driver, TaskDetailsObjects.supervisorInfoYes, "Supervisor Approval Required must be YES");
					} else {
						gm.logScreenshot("Fail", "Supervisor Info not present!", driver);
					}
				}
				
				if(selectedDetails[i].equals("Urgent Task")){
					gm.checkThings(driver, TaskDetailsObjects.urgentTag, "Urgent Tag MUST be visible");
				}
				
				if(selectedDetails[i].equals("WBS Provided")){
					if(gm.elementExist(driver, TaskDetailsObjects.wbsDiv)){
						gm.checkThings(driver, TaskDetailsObjects.wbsYes, "WBS YES - expect Passed");
						if(charge.equalsIgnoreCase("Yes"))
							gm.checkThings(driver, TaskDetailsObjects.wbsChargeYes, "Charegeable must be YES because datapool says YES");
						else
							gm.checkThings(driver, TaskDetailsObjects.wbsChargeNo, "Charegeable must be NO because datapool says Null");
					} else {
						gm.logScreenshot("Fail", "WBS Info not present!", driver);
					}
				}
			}
			gm.checkThings(driver, TaskDetailsObjects.supervisorInfo, "Supervisor Approval Required should be present");
			gm.checkThings(driver, TaskDetailsObjects.urgentTagHidden, "Check if urgent tag is ready and in place");
			gm.checkThings(driver, TaskDetailsObjects.wbsDiv, "WBS info should be present");
			
		} else {
			gm.checkThings(driver, TaskDetailsObjects.supervisorInfo, "Supervisor Approval Required should be present and contains NO");
			gm.checkThings(driver, TaskDetailsObjects.urgentTag, "Urgent tag should be hidden - EXPECT FAILED");
			gm.checkThings(driver, TaskDetailsObjects.wbsNo, "WBS should display NO");
			
		}	
	}
	

	
//=========================================================================================================================================
	/**1.1.2.8 to 1.1.2.9, 1.1.1.4*/
	public void validateAdditionalDetails2(WebDriver driver, String multioffer, String resources, String workLocType, String workLoc) throws InterruptedException	
	{
		if(multioffer.equals("null")){
			gm.checkThings(driver, TaskDetailsObjects.multiNo, "Multi Offer should display NO");
			gm.checkThings(driver, TaskDetailsObjects.multiResource, "Resource part should not be present - EXPECT FAIL");
		} else {
			gm.checkThings(driver, TaskDetailsObjects.multiYes, "Multi Offer should display Yes");
			gm.checkThings(driver, TaskDetailsObjects.multiResource, "Resource part should display resource number");
			String multiResource = driver.findElement(TaskDetailsObjects.multiResource).getText();
			if(resources.equals(multiResource))
				gm.setLogMsg("Pass", "Resource count is correct!");
			else
				gm.logScreenshot("Fail", "Resource count is INcorrect! " + multiResource + " " + resources, driver);	
		}
		
		
		gm.checkThings(driver, TaskDetailsObjects.workLocationDiv, "Work Location Info must be present");
		String locationDetails = driver.findElement(TaskDetailsObjects.workLocationDetails).getText();
		String locationDetailsSubHeader = driver.findElement(TaskDetailsObjects.subHeaderLocation).getText();
		
		
		if(workLocType.equals("Client Site")){
					
			if(locationDetails.contains("Client Site - " + workLoc)&&locationDetailsSubHeader.equals("Client Site")){//in the datapool, facility/client location should be exact
				gm.setLogMsg("Pass", "Location is correct! " + locationDetails);
			}else{
				gm.logScreenshot("Fail", "Location Type is Incorrect!", driver);	
			}
		}
		
		
		if(workLocType.equals("Accenture Location")){
			
			
			if(locationDetails.contains("On Site - ")&& locationDetails.contains(workLoc) &&locationDetailsSubHeader.equals("On Site")){//in the datapool, facility/client location should be exact
				gm.setLogMsg("Pass", "Location is correct!");
			}else{
				gm.logScreenshot("Fail", "Location Type is Incorrect! " + locationDetails, driver);	
			}
		}
		
		
		if(workLocType.contains("Virtual")){
			gm.checkThings(driver, TaskDetailsObjects.workLocationVirtual, "Work Location Info must be Virtual");
			if(locationDetailsSubHeader.equals("Virtual")){//in the datapool, facility/client location should be exact
				gm.setLogMsg("Pass", "Location type in the header is correct!");
			}else{
				gm.logScreenshot("Fail", "Location type in the header is Incorrect! " + locationDetailsSubHeader, driver);	
			}
		}
			
	}

//=========================================================================================================================================	
	/**1.1.1.5 to 1.1.1.7*/
	public void validateSubHeader(WebDriver driver, String market, String category, String effort, String effortUnit) throws InterruptedException{
		
		String subHeaderMarket = driver.findElement(TaskDetailsObjects.marketplace).getText();
		String subHeaderCategory = driver.findElement(TaskDetailsObjects.category).getText();
		String subHeaderEffort = driver.findElement(TaskDetailsObjects.effort).getText();
		
		
		
		if(subHeaderMarket.equals(market)){
			gm.setLogMsg("Pass", "Marketplace displayed in the subheader is correct!");
		}else{
			gm.logScreenshot("Fail", "Marketplace displayed in the subheader is INCORRECT! " + subHeaderMarket, driver);	
		}
		
		if(subHeaderCategory.equals(category)){
			gm.setLogMsg("Pass", "Category displayed in the subheader is correct!");
		}else{
			gm.logScreenshot("Fail", "Category displayed in the subheader is INCORRECT! " + subHeaderCategory, driver);	
		}
		
		
		if(effort.equals("null")){
			effort = "";
		} else {
			effort = effort + " ";
		}
		
		if(subHeaderEffort.equals(effort + effortUnit.replace("s", "") + " Effort")){
			gm.setLogMsg("Pass", "EFFORT displayed in the subheader is correct!");
		}else{
			gm.logScreenshot("Fail", "EFFORT displayed in the subheader is INCORRECT! " + subHeaderEffort + " - " + effort + effortUnit.replace("s", "") + " Effort", driver);	
		}
		
		
	}
	
	
//=========================================================================================================================================
	
	/**1.1.1.8 to 1.1.1.10*/
	
	public void validateDates(WebDriver driver, String startHours, String startMinutes, String startAMPM, String endHours, String endMinutes, 
			String endAMPM, String startDateDay, String startDateMonth, String startDateYear, String endDateDay, String endDateMonth, String endDateYear, 
			String expireDateHour, String expireDateMinute, String expireDateAMPM, String expireDateDay, String expireDateMonth, String expireDateYear) throws InterruptedException{
		
		
		
		String startDate = ptm.converter(startDateDay) + " " + startDateMonth.substring(0, Math.min(startDateMonth.length(), 3)).toUpperCase() + " " + startDateYear + " " +ptm.converter(startHours) + ":" + ptm.converter(startMinutes) + " " + startAMPM;
		String endDate = ptm.converter(endDateDay) + " " + endDateMonth.substring(0, Math.min(endDateMonth.length(), 3)).toUpperCase() + " " + endDateYear + " " +ptm.converter(endHours) + ":" + ptm.converter(endMinutes) + " " + endAMPM;
		String expireDate = ptm.converter(expireDateDay) + " " + expireDateMonth.substring(0, Math.min(expireDateMonth.length(), 3)).toUpperCase() + " " + expireDateYear + " " +ptm.converter(expireDateHour) + ":" + ptm.converter(expireDateMinute) + " " + expireDateAMPM;
		
		String taskDetailsStartDate = driver.findElement(TaskDetailsObjects.startDate).getText().replaceAll("START: ", "");
		String taskDetailsEndDate = driver.findElement(TaskDetailsObjects.endDate).getText().replaceAll("END: ", "");
		String taskDetailsExpireDate = driver.findElement(TaskDetailsObjects.expireDate).getText().replaceAll("EXP: ", "");
		
		if(taskDetailsStartDate.equals(startDate)){
			gm.setLogMsg("Pass", "startDate displayed is correct!");
		}else{
			gm.logScreenshot("Fail", "startDate displayed is INCORRECT! " + taskDetailsStartDate + " - " + startDate, driver);	
		}
		
		
		if(taskDetailsEndDate.equals(endDate)){
			gm.setLogMsg("Pass", "endDate displayed is correct!");
		}else{
			gm.logScreenshot("Fail", "endDate displayed is INCORRECT! " + taskDetailsEndDate+ " - " + endDate, driver);	
		}
		
		
		if(taskDetailsExpireDate.equals(expireDate)){
			gm.setLogMsg("Pass", "expireDate displayed is correct!");
		}else{
			gm.logScreenshot("Fail", "expireDate displayed is INCORRECT! " + taskDetailsExpireDate+ " - " + expireDate, driver);	
		}
		
	}
	
//=========================================================================================================================================
	
	public void validateDescription(WebDriver driver, String description) throws InterruptedException
	{
		
		
	}
	
//=========================================================================================================================================
	/**1.1.2.1 to 1.1.2.3*/
	
	public void validateWorkTime(WebDriver driver, String startHours, String startMinutes, String startAMPM, String endHours, String endMinutes, String endAMPM, String timeZone ) throws InterruptedException
	{
		if(startHours.equals("null")){
			
			gm.checkThings(driver, TaskDetailsObjects.workTimeDiv, "Work Time Div should not be visible - EXPECT FAIL");
			
		} else {
			
			if(gm.elementExist(driver, TaskDetailsObjects.workTimeDiv)){
				String startTime = ptm.converter(startHours) + ":" + ptm.converter(startMinutes) + " " + startAMPM;
				String endTime = ptm.converter(endHours) + ":" + ptm.converter(endMinutes) + " " + endAMPM;
				
				String taskDetailsStartTime = driver.findElement(TaskDetailsObjects.startTime).getText();
				String taskDetailsEndTime = driver.findElement(TaskDetailsObjects.endTime).getText();
				String taskDetailsTimeZone = driver.findElement(TaskDetailsObjects.timeZone).getText();
				
				if(taskDetailsStartTime.equals(startTime)){
					gm.setLogMsg("Pass", "startTime displayed is correct!");
				}else{
					gm.logScreenshot("Fail", "startTime displayed is INCORRECT! " + taskDetailsStartTime, driver);	
				}
				
				if(taskDetailsEndTime.equals(endTime)){
					gm.setLogMsg("Pass", "endTime displayed is correct!");
				}else{
					gm.logScreenshot("Fail", "endTime displayed is INCORRECT! " + taskDetailsEndTime, driver);	
				}
				
				if(taskDetailsTimeZone.equals(timeZone)){
					gm.setLogMsg("Pass", "timeZone displayed is correct!");
				}else{
					gm.logScreenshot("Fail", "timeZone displayed is INCORRECT! " + taskDetailsTimeZone, driver);	
				}
			} else {
				gm.logScreenshot("Fail", "Work Times are not displayed!!! ", driver);	
			}
			
		}
		
	}	
	
	
//=========================================================================================================================================
	/**1.1.2.10*/
	public void validatePostingOnBehalf(WebDriver driver, String eid) throws InterruptedException{
		
		if(eid.equals("null")){
			gm.checkThings(driver, TaskDetailsObjects.behalfDiv, "Posting on Behalf Of Div should not be visible - EXPECT FAIL");
		} else {
			if(gm.elementExist(driver, TaskDetailsObjects.behalfDiv)){
				if(eid.equals(driver.findElement(TaskDetailsObjects.postOnBehalfOf).getText())){
					gm.setLogMsg("Pass", "Task Delegate displayed is correct!");
				}else{
					gm.logScreenshot("Fail", "Task Delegate displayed is INCORRECT! " + driver.findElement(TaskDetailsObjects.postOnBehalfOf).getText(), driver);	
				}
			} else {
				gm.logScreenshot("Fail", "Task Delegate are not displayed!!! ", driver);	
			}
		}
	}	

//=========================================================================================================================================
	/**1.1.2.10*/
	public void validateAppliedFilters(WebDriver driver, String requiredFilters, String language, String languageAny, String resourceLoc, 
			String careerLevel, String talentSegment, String orgUnit, String specialty, String skills, String marketAttributes) throws InterruptedException{
		
		String[] filter = requiredFilters.split(",");
		for (int i = 0; i < filter.length; i++) {
			
			switch(filter[i]){
			case "Language":{

				gm.checkThings(driver, TaskDetailsObjects.languageFilters, "Language Filter Div Title");
				
				String[] languages = language.split(",");
				
				for (int j = 0;j < languages.length; j++){
					gm.checkThings(driver, TaskDetailsObjects.languageFilterSelected(languages[j]), "Passed if " + languages[j] + " is displayed.");
				}
		
			}
			break;
			
			case "Resource Location":{

				gm.checkThings(driver, TaskDetailsObjects.resourceLocFilters, "Resource Location Filter Div Title");
				
				String[] resourceLocs = resourceLoc.split("-");
				
				for (int k = 0;k < resourceLocs.length;k++){//iterate over the different locations
					String[] resourceLocExact = resourceLocs[k].split(",");
					String location = resourceLocExact[resourceLocExact.length-1];//extract the exact location and exclude the parent locations
					gm.checkThings(driver, TaskDetailsObjects.resourceLocFilterSelected(location), "Passed if " + location +" filter is displayed.");
				}
				
			}
			break;
			
			case "Career Level":{

				gm.checkThings(driver, TaskDetailsObjects.careerLevelFilters, "Career Level Filter Div Title");
				
				String[] careerLevels = careerLevel.split(",");
				
				for (int j = 0;j < careerLevels.length; j++){
					
					if(careerLevels[j].equals("LDR")){
						gm.checkThings(driver, TaskDetailsObjects.careerLevelFilterLDR, "Passed if Accenture Leadership is displayed.");
					} else {
						gm.checkThings(driver, TaskDetailsObjects.careerLevelFilterSelected(careerLevels[j]), "Passed if " + careerLevels[j] + " is displayed.");
					}
					
				}
			
			}
			break;
			
			case "Organization Unit":{

				gm.checkThings(driver, TaskDetailsObjects.orgUnitFilters, "Org Unit Filter Div Title");
				
				String[] orgUnits = orgUnit.split("-");
				
				for (int k = 0;k < orgUnits.length;k++){//iterate over the different org units
					String[] orgUnitExact = orgUnits[k].split(",");
					String org = orgUnitExact[orgUnitExact.length-1];//extract the exact location and exclude the parent locations
					gm.checkThings(driver, TaskDetailsObjects.orgUnitFilterSelected(org), "Passed if " + org +" filter is displayed.");
				}
				
			}
			break;
			
			case "Talent Segment":{

				gm.checkThings(driver, TaskDetailsObjects.talentSegmentFilters, "Org Unit Filter Div Title");
				
				String[] talentSegments = talentSegment.split("-");
				
				for (int k = 0;k < talentSegments.length;k++){//iterate over the different org units
					String[] talentSegmentExact = talentSegments[k].split(",");
					String org = talentSegmentExact[talentSegmentExact.length-1];//extract the exact location and exclude the parent locations
					gm.checkThings(driver, TaskDetailsObjects.talentSegmentFilterSelected(org), "Passed if " + org +" filter is displayed.");
				}
				
			}
			break;
			
			
			case "Specialty":{

				gm.checkThings(driver, TaskDetailsObjects.specialtyFilters, "Specialty Filter Div Title");
				
				String[] specialties = specialty.split(",");
				
				for (int j = 0;j < specialties.length; j++){
					gm.checkThings(driver, TaskDetailsObjects.specialtyFilterSelected(specialties[j]), "Passed if " + specialties[j] + " is displayed.");
				}
		
			}
			break;
			
			default:
			}
					
		}
		
		
	}
	
//=========================================================================================================================================	
	
	public void insertValidateComment(WebDriver driver, String comment) throws InterruptedException{
		
		if(!comment.equals("null")){
			gm.checkThings(driver, TaskDetailsObjects.commentButtonHidden, "Buttons should not be visible");
			if(gm.elementExist(driver, TaskDetailsObjects.commentSection)&&gm.elementExist(driver, TaskDetailsObjects.commentField)){
				
				List<WebElement> elm = driver.findElements(TaskDetailsObjects.commentPosted);
	            System.out.println("The len is : "+ elm.size());
	            
				
				driver.findElement(TaskDetailsObjects.commentField).click();
				driver.findElement(TaskDetailsObjects.commentField).sendKeys(comment);
				gm.checkThings(driver, TaskDetailsObjects.commentCancel, "Cancel Button should be visible already");
				gm.checkThings(driver, TaskDetailsObjects.commentSubmit, "Comment Buttons should be visible already");
				driver.findElement(TaskDetailsObjects.commentSubmit).click();
				Thread.sleep(3000);
				String newlyPostedComment = driver.findElement(TaskDetailsObjects.commentNewPost(elm.size()+1)).getText();
				
				if(newlyPostedComment.equals(comment)){
					gm.setLogMsg("Pass", "Comment is displayed correctly!");
				} else {
					gm.logScreenshot("Fail", "Comment is either not posted or incorrect!", driver);
				}
				
				
			} else {
				gm.logScreenshot("Fail", "Comment section and field do not exist!!!", driver);
			}
		}
		
	}
	
	
	
//=========================================================================================================================================	

	/**User Story 13326: User can access the Task Details read mode screen and see the details section.
	 * @author jan.carlo.l.sabanal
	 * @since 02/02/2017
	 * 
	 * 
	*/
	
	public void validateButtons(WebDriver driver) throws InterruptedException{
		
		/**copy*/
		gm.checkThings(driver, TaskDetailsObjects.copyButton, "Copy button should always exist");
		
		String status = driver.findElement(TaskDetailsObjects.taskStatus).getText();
		
		/**withdraw edit delete*/
		if(status.equals("OPEN - POSTED")||status.equals("OPEN - APPLIED")||status.equals("OPEN - OFFERED")||status.equals("ACCEPTED")){
			gm.checkThings(driver, TaskDetailsObjects.withdrawButton, "Withdraw button should be displayed");
			gm.checkThings(driver, TaskDetailsObjects.deleteButton, "Delete button should be displayed");
			gm.checkThings(driver, TaskDetailsObjects.editButton, "Edit button should be displayed");
		} else {
			gm.checkThings(driver, TaskDetailsObjects.withdrawButton, "EXPECT FAILED - Withdraw button should NOT be displayed");
			gm.checkThings(driver, TaskDetailsObjects.editButton, "EXPECT FAILED - Edit button should NOT be displayed");
			gm.checkThings(driver, TaskDetailsObjects.deleteButton, "EXPECT FAILED - Delete button should NOT be displayed");
			
		}
		
	}
	
	
//=========================================================================================================================================	

	/**User Story 14055: Task Poster able to see the Participants section in the Task Details screen of my posted task.
	 * User Story 14056: Task Poster able to see access the Manage Participants screen from Participants section of the task details page.
	 * 
	 * @author jan.carlo.l.sabanal
	 * @since 02/15/2017
	 * 
	 * 
	*/
	
	public void inspectManageParticipantsMethods(WebDriver driver, String data1) throws InterruptedException{
		String staffCount = driver.findElement(TaskDetailsObjects.staffCount).getText();
		Thread.sleep(3000);
		driver.findElement(TaskDetailsObjects.manageParticipants).click();
		gm.checkThings(driver, TaskDetailsObjects.closeParticipantsModal, "Close modal button");
		String modalStaffCount = driver.findElement(TaskDetailsObjects.modalStaffCount).getText();
		
		if(modalStaffCount.equals(staffCount + " for this Task"))
			gm.setLogMsg("Pass", "Staff count in manage participants modal is correct");
		else
			gm.logScreenshot("Fail", "Staff count in manage participants modal is incorrect", driver);
		
		if(gm.elementExist(driver, TaskDetailsObjects.tabsActive("APPLIED")))
			gm.setLogMsg("Pass", "APPLIED tab is opened by default");
		else
			gm.logScreenshot("Fail", "APPLIED tab is NOT opened by default", driver);
			
		String tabColor, tabUnderline;
		
		String[] countPerTab = data1.split(",");
		
		String[] tabs = {"APPLIED","OFFERED","ACCEPTED","DECLINED","WITHDRAWN"};
		
			for(int i = 0;i<tabs.length;i++){
				
				/**check if tabs are getting highlighted when clicked. APPLIED tab is selected by default.*/
				tabColor = driver.findElement(TaskDetailsObjects.tabsActive(tabs[i])).getCssValue("color");
				tabUnderline = driver.findElement(TaskDetailsObjects.tabsActive(tabs[i])).getCssValue("border-bottom-color");
				
				if(tabColor.equals("rgba(66, 180, 238, 1)")&&tabUnderline.equals("rgba(66, 180, 238, 1)"))
					gm.setLogMsg("Pass", tabs[i] + " tab is highlighted when selected" );
				else
					gm.logScreenshot("Fail", tabs[i] + " tab is NOT highlighted when selected - " + tabColor + " and " + tabUnderline, driver);
				
				/**check the staff count on the tabs*/
				if(!countPerTab[i].equals("0")){
					try{
						String tabStaffCount = driver.findElement(TaskDetailsObjects.tabsStatusCtr(tabs[i])).getText();
						if(tabStaffCount.equals(countPerTab[i]))
							gm.setLogMsg("Pass", "Correct crowd count displayed");
						else
							gm.logScreenshot("Fail", tabs[i] + "Incorrect crowd count displayed - " + tabStaffCount + "!=" +  countPerTab[i], driver);
					} catch(org.openqa.selenium.NoSuchElementException e){
						gm.logScreenshot("Info", "No staff count over the " + tabs[i] + " tab", driver);
					}
				} else {
					/**Acceptance criteria for APPLIED tab only but applicable to all tabs:
					 * 1.1.3)  If no content is available, the blue dot will not be displayed and with the tab content will display the static text: 
	                   'There are no applicants for this task at this time. '*/
					gm.checkThings(driver, TaskDetailsObjects.textWhenEmpty(i+1), "No resources under "+tabs[i]+" TAB so text must be displayed");
				}
								
				/**clicks the next tab, i.e. from APPLIED[0], will click OFFERED[1]*/
				if(i<4){//so that the last time it will click the tab is when i=3, i+1 = 4 = WITHDRAWN. if i=4, i+1=5, will result to arrayOutOfBounds
					driver.findElement(TaskDetailsObjects.tabsInactive(tabs[i+1])).click();
				}
				Thread.sleep(2000);
			}
		
		
	}
	
//=========================================================================================================================================	

	/**
	 * User Story 14452: As a task poster, I am able to access the 'APPLIED' tab of Manage Participants screen.
	 * User Story 11104: Task Poster is able to see the Offer option from the Manage Application Screen.
	 * User Story 11105: Task Poster is able to offer the task through the Offer option from the Manage Application Screen.
	 * User Story 11102: Task Poster is able to see the Decline Application option from the Manage Application Screen.
	 * User Story 11103:Task Poster is able to decline the application through the Decline Application option from the Manage Application Screen.
	 * User Story 14053:Task Poster able to see the application status change in my Manage Application screen after the user declined my offer.
	 * 
	 * @author jan.carlo.l.sabanal
	 * @since 02/15/2017
	 * 
	 * 
	*/
	
	public void applyTabActions(WebDriver driver, String data1, String actionSet, String moreDetails, String applyDetails, String applicationComment) throws InterruptedException{
		
		String[]name = data1.split(",");
		String[]actionsPerUser = actionSet.split(",");
		
		
		if(!data1.equals("null")){
			gm.checkThings(driver, TaskDetailsObjects.applyChangesDisabled, "Apply Changes button should be disabled");
			for(int i=0;i<name.length;i++){
				Thread.sleep(2000);
				gm.checkThings(driver, TaskDetailsObjects.rosterName(name[i]), "Applicant's name should be displayed - " + name[i]);
				gm.checkThings(driver, TaskDetailsObjects.rosterPosition(name[i]), "Applicant's position should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterLocation(name[i]), "Applicant's location should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterDigital(name[i]), "Applicant's digital reputation should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterCareer(name[i]), "Applicant's career level should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterTalent(name[i]), "Applicant's talent segment should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterOffer(name[i]), "Poster should be able to offer this applicant - " + name[i]);
				gm.checkThings(driver, TaskDetailsObjects.rosterDecline(name[i]), "Poster should be able to decline this applicant - " + name[i]);
				gm.checkThings(driver, TaskDetailsObjects.rosterLike(name[i]), "Poster should be able to decline this applicant - " + name[i]);
				gm.checkThings(driver, TaskDetailsObjects.rosterMoreOrLess(name[i]), "Poster should be able to decline this applicant - " + name[i]);
				
				String[]action = actionsPerUser[i].split("-");
						
				for(int j=0;j<action.length;j++){
					switch(action[j]){
						case "Offer":
							driver.findElement(TaskDetailsObjects.rosterOffer(name[i])).click();
							Thread.sleep(1000);
							break;
							
						case "Decline":
							driver.findElement(TaskDetailsObjects.rosterDecline(name[i])).click();
							Thread.sleep(1000);
							break;
							
						case "Flag":
							driver.findElement(TaskDetailsObjects.rosterLike(name[i])).click();
							Thread.sleep(1000);
							break;
							
						case "More":
							driver.findElement(TaskDetailsObjects.rosterMoreOrLess(name[i])).click();
							
							
							gm.checkElementText(driver, TaskDetailsObjects.moreDetailsLabel, applyDetails.substring(3));
							String[]skills = moreDetails.split(",");
							for(int k = 0;k<skills.length;k++){
								List<WebElement> elm = driver.findElements(TaskDetailsObjects.moreDetailsSkills);
					            System.out.println("The len is : "+ elm.size());
					            				            
								gm.checkWebElementText(driver, elm.get(k), skills[k]);	
							}
							gm.checkElementText(driver, TaskDetailsObjects.commentSectionApplicantName,name[i]);
							gm.checkElementText(driver, TaskDetailsObjects.commentSectionComment,applicationComment);	
							break;
							
						default:
					}
				}
				
				
			}
			
			
		} else {
			gm.checkThings(driver, TaskDetailsObjects.textWhenEmpty(1)/**1 = APPLIED TAB*/, "No resources under APPLIED TAB so text must be displayed");
			
		}
		
		
		if(actionSet.equals("null"))/**after inspection only*/{
			driver.findElement(TaskDetailsObjects.closeParticipantsModal).click();
			Thread.sleep(3000);
		}
			
		else{
			if(actionSet.contains("Decline")){/**you can simultaneously offer or decline multiple participants, but decline modal will appear. offer will be covered.*/
				driver.findElement(TaskDetailsObjects.applyChanges).click();
				gm.waitForObject(driver, TaskDetailsObjects.declinedModal);
				driver.findElement(TaskDetailsObjects.cancelDecline).click();
				Thread.sleep(1000);
				driver.findElement(TaskDetailsObjects.applyChanges).click();
				gm.waitForObject(driver, TaskDetailsObjects.declinedModal);
				driver.findElement(TaskDetailsObjects.confirmDecline).click();
				driver.findElement(TaskDetailsObjects.closeParticipantsModal).click();
				Thread.sleep(3000);
			}else{/**if purely offer or have flag or click more regardless if multiple or not*/
				driver.findElement(TaskDetailsObjects.applyChanges).click();
				gm.waitForObject(driver, TaskDetailsObjects.offerModal);
				driver.findElement(TaskDetailsObjects.cancelOffer).click();
				Thread.sleep(1000);
				driver.findElement(TaskDetailsObjects.applyChanges).click();
				gm.waitForObject(driver, TaskDetailsObjects.offerModal);
				driver.findElement(TaskDetailsObjects.confirmOffer).click();
				driver.findElement(TaskDetailsObjects.closeParticipantsModal).click();
				Thread.sleep(3000);
			}
		}
		
	}	
	
	
	
//=========================================================================================================================================	

	/**
	 * 14453	As a task poster, I am able to access the 'OFFERED' tab of Manage Participants screen.
	 * 14050	Task Poster able to see the Withdraw Offer from the Manage Application Screen.
	 * 11107	Task Poster is able to withdraw offer through the Withdraw Offer option from the Manage Application screen.
	 * 
	 * @author jan.carlo.l.sabanal
	 * @since 02/16/2017
	 * 
	 * 
	*/
	
	public void offerTabActions(WebDriver driver, String data1, String withdrawOrNot, String moreDetails, String applyDetails, String applicationComment) throws InterruptedException{
		
		String[]name = data1.split(",");
		String[]actionPerUser = withdrawOrNot.split(",");
		
		if(!data1.equals("null")){
			gm.checkThings(driver, TaskDetailsObjects.applyChangesDisabled, "Apply Changes button should be disabled");
			for(int i=0;i<name.length;i++){
				Thread.sleep(2000);
				gm.checkThings(driver, TaskDetailsObjects.rosterName(name[i]), "Applicant's name should be displayed - " + name[i]);
				gm.checkThings(driver, TaskDetailsObjects.rosterPosition(name[i]), "Applicant's position should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterLocation(name[i]), "Applicant's location should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterDigital(name[i]), "Applicant's digital reputation should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterCareer(name[i]), "Applicant's career level should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterTalent(name[i]), "Applicant's talent segment should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterWithdrawOffer(name[i]), "Applicant offer should be able to be withdrawn");
			
				String[]action = actionPerUser[i].split("-");
				for(int j=0;j<action.length;j++){
					switch(action[j]){
						case "Withdraw":
							driver.findElement(TaskDetailsObjects.rosterWithdrawOffer(name[i])).click();
							break;
							
						case "Flag":
							driver.findElement(TaskDetailsObjects.rosterLike(name[i])).click();
							Thread.sleep(1000);
							break;
							
						case "More":
							driver.findElement(TaskDetailsObjects.rosterMoreOrLess(name[i])).click();
							gm.checkElementText(driver, TaskDetailsObjects.moreDetailsLabel, applyDetails.substring(3));
							String[]skills = moreDetails.split(",");
							for(int k = 0;k<skills.length;k++){
								List<WebElement> elm = driver.findElements(TaskDetailsObjects.moreDetailsSkills);
					            System.out.println("The len is : "+ elm.size());
					            				            
								gm.checkWebElementText(driver, elm.get(k), skills[k]);	
							}
							gm.checkElementText(driver, TaskDetailsObjects.commentSectionApplicantName,name[i]);
							gm.checkElementText(driver, TaskDetailsObjects.commentSectionComment,applicationComment);	
							break;
							
						default:
					}
				}
				
			}
		} else {
			gm.checkThings(driver, TaskDetailsObjects.textWhenEmpty(2)/**2 = OFFERED TAB*/, "No resources under OFFERED TAB so text must be displayed");
			
		}
		
		if(withdrawOrNot.contains("Withdraw")){//if one or more participants are tagged for withdrawal of offer
			driver.findElement(TaskDetailsObjects.applyChanges).click();
			gm.waitForObject(driver, TaskDetailsObjects.withdrawOfferModal);
			driver.findElement(TaskDetailsObjects.cancelWithdraw).click();
			Thread.sleep(1000);
			driver.findElement(TaskDetailsObjects.applyChanges).click();
			gm.waitForObject(driver, TaskDetailsObjects.withdrawOfferModal);
			driver.findElement(TaskDetailsObjects.withdrawOffer).click();
			driver.findElement(TaskDetailsObjects.closeParticipantsModal).click();
			Thread.sleep(3000);
		} else {
			driver.findElement(TaskDetailsObjects.closeParticipantsModal).click();
			Thread.sleep(3000);
		}
		
		
		
	}
	
	
//=========================================================================================================================================	

	/**
	 * 14453	As a task poster, I am able to access the 'OFFERED' tab of Manage Participants screen.
	 * 14050	Task Poster able to see the Withdraw Offer from the Manage Application Screen.
	 * 11107	Task Poster is able to withdraw offer through the Withdraw Offer option from the Manage Application screen.
	 * 11108	Applicant able to decline the offer of Task poster through the Decline Option from the Task Details read mode screen.
	 * 11111	Applicant is able to accept the offer of Task poster through the Accept Offer option from the Task Details read mode screen.0.+3
	 * 14051	Applicant able to see my application status that has been offered in the Task Details Page.
	 * 
	 * @author jan.carlo.l.sabanal
	 * @since 02/16/2017
	 * 
	 * 
	*/
	
	public void respondToOffer(WebDriver driver, String action, String reason, String taskTitle) throws InterruptedException{

		
		switch(action){
		
		case "NothingNow":		
			gm.waitForObject(driver, TaskDetailsObjects.applicantFirstTimeOfferModal);
			String congratsMessage = driver.findElement(TaskDetailsObjects.congratsMessage).getText();
			if(congratsMessage.contains(taskTitle))
			gm.setLogMsg("Pass", "Message with correct task title is displayed");
			else
			gm.logScreenshot("Pass", "Message with correct task title is displayed", driver);
		
			driver.findElement(TaskDetailsObjects.nothingNowButton).click();
			gm.checkThings(driver, TaskDetailsObjects.nothingNowMessage, "Modal with message appears after clicking Nothing Now");
			driver.findElement(TaskDetailsObjects.nothingNowOk).click();
			Thread.sleep(1000);
			gm.checkThings(driver, TaskDetailsObjects.acceptStickyHeaderButton, "Accept button should be available");
			gm.checkThings(driver, TaskDetailsObjects.declineStickyHeaderButton, "Decline button should be available");
			gm.checkThings(driver, TaskDetailsObjects.applicationMessageAfterOffer, "offer details message should be displayed");
			break;
		
		case "AcceptNow":
			gm.waitForObject(driver, TaskDetailsObjects.applicantFirstTimeOfferModal);
			String congratsMessage3 = driver.findElement(TaskDetailsObjects.congratsMessage).getText();
			if(congratsMessage3.contains(taskTitle))
			gm.setLogMsg("Pass", "Message with correct task title is displayed");
			else
			gm.logScreenshot("Pass", "Message with correct task title is displayed", driver);
			
			driver.findElement(TaskDetailsObjects.acceptButton).click();
			gm.checkThings(driver, TaskDetailsObjects.acceptModal, "Modal with onboarding message appears after clicking Accept");
			gm.checkThings(driver, TaskDetailsObjects.acceptModalMessage1, "Accept Modal Message 1 should be visible");
			gm.checkThings(driver, TaskDetailsObjects.acceptModalMessage2, "Accept Modal Message 2 should be visible");
			driver.findElement(TaskDetailsObjects.acceptModalCancel).click();
			Thread.sleep(1000);
			driver.findElement(TaskDetailsObjects.acceptButton).click();
			driver.findElement(TaskDetailsObjects.acceptModalOk).click();
			Thread.sleep(1000);
			gm.checkThings(driver, TaskDetailsObjects.applicationMessageAfterAccept, "accept details message should be displayed");
			break;
		
		case "DeclineNow":
			gm.waitForObject(driver, TaskDetailsObjects.applicantFirstTimeOfferModal);
			String congratsMessage2 = driver.findElement(TaskDetailsObjects.congratsMessage).getText();
			if(congratsMessage2.contains(taskTitle))
			gm.setLogMsg("Pass", "Message with correct task title is displayed");
			else
			gm.logScreenshot("Pass", "Message with correct task title is displayed", driver);
			
			driver.findElement(TaskDetailsObjects.declineButton).click();
			gm.checkThings(driver, TaskDetailsObjects.declineModal, "Modal with sorry message appears after clicking Accept");
			gm.checkThings(driver, TaskDetailsObjects.declineModalMessage1, "Decline Modal Message 1 should be visible");
			gm.checkThings(driver, TaskDetailsObjects.declineModalReason, "Decline Modal Reason must be selectable");
			driver.findElement(TaskDetailsObjects.declineModalCancel).click();
			Thread.sleep(1000);
			driver.findElement(TaskDetailsObjects.declineButton).click();
			gm.checkThings(driver, TaskDetailsObjects.declineModalReason, "Decline Modal Reason must be selectable");
			driver.findElement(TaskDetailsObjects.declineModalReason).click();
			driver.findElement(TaskDetailsObjects.declineModalOk).click();
			Thread.sleep(1000);
			
			gm.checkThings(driver, TaskDetailsObjects.applicationMessageAfterOffer, "offer details message should STILL be displayed even after decline");/**US15493*/
			gm.checkThings(driver, TaskDetailsObjects.applicationMessageAfterDecline, "Decline details message should be displayed");
			gm.checkThings(driver, TaskDetailsObjects.applicantWithdrawButton, "Withdraw button should be present");
			break;
		
		case "Accept":
			driver.findElement(TaskDetailsObjects.acceptStickyHeaderButton).click();
			gm.checkThings(driver, TaskDetailsObjects.acceptModal, "Modal with onboarding message appears after clicking Accept");
			gm.checkThings(driver, TaskDetailsObjects.acceptModalMessage1, "Accept Modal Message 1 should be visible");
			gm.checkThings(driver, TaskDetailsObjects.acceptModalMessage2, "Accept Modal Message 2 should be visible");
			driver.findElement(TaskDetailsObjects.acceptModalCancel).click();
			Thread.sleep(1000);
			driver.findElement(TaskDetailsObjects.acceptStickyHeaderButton).click();
			driver.findElement(TaskDetailsObjects.acceptModalOk).click();
			Thread.sleep(1000);
			gm.checkThings(driver, TaskDetailsObjects.applicationMessageAfterAccept, "accept details message should be displayed");
			break;
			
		case "Decline":
			driver.findElement(TaskDetailsObjects.declineStickyHeaderButton).click();
			gm.checkThings(driver, TaskDetailsObjects.declineModal, "Modal with sorry message appears after clicking Accept");
			gm.checkThings(driver, TaskDetailsObjects.declineModalMessage1, "Decline Modal Message 1 should be visible");
			gm.checkThings(driver, TaskDetailsObjects.declineModalReason, "Decline Modal Reason must be selectable");
			driver.findElement(TaskDetailsObjects.declineModalCancel).click();
			Thread.sleep(1000);
			driver.findElement(TaskDetailsObjects.declineStickyHeaderButton).click();
			gm.checkThings(driver, TaskDetailsObjects.declineModalReason, "Decline Modal Reason must be selectable");
			driver.findElement(TaskDetailsObjects.declineModalReason).click();
			driver.findElement(TaskDetailsObjects.declineModalOk).click();
			Thread.sleep(1000);
			gm.checkThings(driver, TaskDetailsObjects.applicationMessageAfterOffer, "offer details message should STILL be displayed even after decline");/**US15493*/
			gm.checkThings(driver, TaskDetailsObjects.applicationMessageAfterDecline, "decline details message should be displayed");
			gm.checkThings(driver, TaskDetailsObjects.applicantWithdrawButton, "Withdraw button should be present");
			break;
			
		default:
		
		}
		
		
	}
	
	
//=========================================================================================================================================	

	/**
	 * 14455	As a task poster, I am able to access the 'ACCEPTED' tab of Manage Participants screen.
	 * 11127	User can see the Accepted status in the Task Details read mode screen.
	 * 11107	Task Poster is able to withdraw offer through the Withdraw Offer option from the Manage Application screen.
	 * 14054	Task Poster able to see the application status change in my Manage Application screen after the user accepts my offer.
	 * 
	 * @author jan.carlo.l.sabanal
	 * @since 02/16/2017
	 * 
	 * 
	*/
	
	public void acceptedTabActions(WebDriver driver, String data1, String FlagOrMore, String moreDetails, String applyDetails, String applicationComment) throws InterruptedException{
		
		String[]name = data1.split(",");
		String[]actionPerUser = FlagOrMore.split(",");
		
		if(!data1.equals("null")){
			
			
			for(int i=0;i<name.length;i++){
				Thread.sleep(2000);
				gm.checkThings(driver, TaskDetailsObjects.rosterName(name[i]), "Applicant's name should be displayed - " + name[i]);
				gm.checkThings(driver, TaskDetailsObjects.rosterPosition(name[i]), "Applicant's position should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterLocation(name[i]), "Applicant's location should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterDigital(name[i]), "Applicant's digital reputation should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterCareer(name[i]), "Applicant's career level should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterTalent(name[i]), "Applicant's talent segment should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterAcceptedLabel(name[i]), "Applicant should be displayed as ACCEPTED");
			
				String[]action = actionPerUser[i].split("-");
				for(int j=0;j<action.length;j++){
					switch(action[j]){
					
					case "Flag":
						driver.findElement(TaskDetailsObjects.rosterLike(name[i])).click();
						Thread.sleep(1000);
						break;
						
					case "More":
						driver.findElement(TaskDetailsObjects.rosterMoreOrLess(name[i])).click();
						gm.checkElementText(driver, TaskDetailsObjects.moreDetailsLabel, applyDetails.substring(3));
						String[]skills = moreDetails.split(",");
						for(int k = 0;k<skills.length;k++){
							List<WebElement> elm = driver.findElements(TaskDetailsObjects.moreDetailsSkills);
				            System.out.println("The len is : "+ elm.size());
				            				            
							gm.checkWebElementText(driver, elm.get(k), skills[k]);	
						}
						gm.checkElementText(driver, TaskDetailsObjects.commentSectionApplicantName,name[i]);
						gm.checkElementText(driver, TaskDetailsObjects.commentSectionComment,applicationComment);	
						break;
						
					default:
						
					}
				}
					
			}
			
		} else {
			gm.checkThings(driver, TaskDetailsObjects.textWhenEmpty(3)/**3 = ACCEPTED TAB*/, "No resources under ACCEPTED TAB so text must be displayed");
		}
		
		driver.findElement(TaskDetailsObjects.closeParticipantsModal).click();	
		Thread.sleep(3000);
	}
	
//=========================================================================================================================================	

	/**
	 * 14456	As a task poster, I am able to access the 'DECLINED' tab of Manage Participants screen.
	 * 
	 * @author jan.carlo.l.sabanal
	 * @since 02/16/2017
	 * 
	 * 
	*/
	
	public void declinedTabActions(WebDriver driver, String data1,String moreDetails, String applyDetails, String applicationComment) throws InterruptedException{
		
		String[]name = data1.split(",");
		
		
		if(!data1.equals("null")){
			
			
			for(int i=0;i<name.length;i++){
				Thread.sleep(2000);
				gm.checkThings(driver, TaskDetailsObjects.rosterName(name[i]), "Applicant's name should be displayed - " + name[i]);
				gm.checkThings(driver, TaskDetailsObjects.rosterPosition(name[i]), "Applicant's position should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterLocation(name[i]), "Applicant's location should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterDigital(name[i]), "Applicant's digital reputation should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterCareer(name[i]), "Applicant's career level should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterTalent(name[i]), "Applicant's talent segment should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterDeclinedLabel(name[i]), "Applicant should be displayed as DECLINED");
			
				/**click more always*/
				driver.findElement(TaskDetailsObjects.rosterMoreOrLess(name[i])).click();
				gm.checkElementText(driver, TaskDetailsObjects.moreDetailsLabel, applyDetails.substring(3));
				
				String[]skills = moreDetails.split(",");
				for(int k = 0;k<skills.length;k++){
					List<WebElement> elm = driver.findElements(TaskDetailsObjects.moreDetailsSkills);
		            System.out.println("The len is : "+ elm.size());
		            				            
					gm.checkWebElementText(driver, elm.get(k), skills[k]);	
				}
				gm.checkElementText(driver, TaskDetailsObjects.commentSectionApplicantName,name[i]);
				gm.checkElementText(driver, TaskDetailsObjects.commentSectionComment,applicationComment);	
				
			}
			
		} else {
			gm.checkThings(driver, TaskDetailsObjects.textWhenEmpty(4)/**4 = DECLINED TAB*/, "No resources under DECLINED TAB so text must be displayed");
		}
		
		driver.findElement(TaskDetailsObjects.closeParticipantsModal).click();	
		Thread.sleep(3000);
	}
	
	
//=========================================================================================================================================	

	/**
	 * 11112	Task Poster is able to see the Complete Task option from the Task Details read mode screen.
	 * 11113	Task Poster is able to mark task as completed through the Complete Task option in the Task Details Page
	 * 14057	Applicant able to see that the status of my task is completed in the Task Details read mode screen.
	 * 
	 * @author jan.carlo.l.sabanal
	 * @since 02/20/2017
	 * 
	 * 
	*/
	
	public void completeTask(WebDriver driver) throws InterruptedException{
		
		
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        String dateInString = driver.findElement(TaskDetailsObjects.startDate).getText();
        String startDate = dateInString.substring(7,18);
        
        try {
        	System.out.println(startDate);
            Date date = formatter.parse(startDate);
            Date today = Calendar.getInstance().getTime();
    	    
    	    if(today.compareTo(date) > 0){
    	    	gm.checkThings(driver, TaskDetailsObjects.completeButton, "Complete Button should be Visible");
    	    	driver.findElement(TaskDetailsObjects.completeButton).click();
    	    	gm.waitForObject(driver, TaskDetailsObjects.completeTaskModal);
    	    	driver.findElement(TaskDetailsObjects.completeTaskModalCancel).click();
    	    	Thread.sleep(1000);
    	    	driver.findElement(TaskDetailsObjects.completeButton).click();
    	    	gm.waitForObject(driver, TaskDetailsObjects.completeTaskModal);
    	    	driver.findElement(TaskDetailsObjects.completeTaskModalConfirm).click();
    	    	Thread.sleep(3000);
    	    	gm.checkThings(driver, TaskDetailsObjects.applicationCompletionDetails, "Completeion details should display");
    	    }
    	      	    
    	    else{
    	    	gm.checkHiddenThings(driver, TaskDetailsObjects.completeButton, "Complete Button should NOT be Visible");
    	    	/**insert anything needed here*/
    	    }
    	    	
            
            
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        
        
	}
	
	
	
//=========================================================================================================================================	

	/**
	 * 
	 * 14458	As a task poster, I am able to access the 'WITHDRAWN' tab of Manage Participants screen.
	 * 	
	 * 
	 * @author jan.carlo.l.sabanal
	 * @since 02/20/2017
	 * 
	 * 
	*/
	
	public void withdrawnTabActionsByPoster(WebDriver driver, String data1, String withdrawnActions, String moreDetails, String applyDetails, String applicationComment) throws InterruptedException{
		
		String[]name = data1.split(",");
		String[]actionPerUser = withdrawnActions.split(",");
		
		if(!data1.equals("null")){
			gm.checkThings(driver, TaskDetailsObjects.applyChangesDisabled, "Apply Changes button should be disabled");
			for(int i=0;i<name.length;i++){
				Thread.sleep(2000);
				gm.checkThings(driver, TaskDetailsObjects.rosterNameWithdrawnByPoster(name[i]), "Applicant's name should be displayed - " + name[i]);
				gm.checkThings(driver, TaskDetailsObjects.rosterPositionWithdrawnByPoster(name[i]), "Applicant's position should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterLocationWithdrawnByPoster(name[i]), "Applicant's location should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterDigitalWithdrawnByPoster(name[i]), "Applicant's digital reputation should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterCareerWithdrawnByPoster(name[i]), "Applicant's career level should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterTalentWithdrawnByPoster(name[i]), "Applicant's talent segment should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterOfferWithdrawnByPoster(name[i]), "Poster should be able to offer this applicant - " + name[i]);
				
				String[]action = actionPerUser[i].split("-");
				for(int j=0;j<action.length;j++){
					switch(action[j]){
					case "Offer":
						driver.findElement(TaskDetailsObjects.rosterOffer(name[i])).click();
						break;
					case "Flag":
						driver.findElement(TaskDetailsObjects.rosterLike(name[i])).click();
						Thread.sleep(1000);
						break;
						
					case "More":
						driver.findElement(TaskDetailsObjects.rosterMoreOrLess(name[i])).click();
						gm.checkElementText(driver, TaskDetailsObjects.moreDetailsLabel, applyDetails.substring(3));
						String[]skills = moreDetails.split(",");
						for(int k = 0;k<skills.length;k++){
							List<WebElement> elm = driver.findElements(TaskDetailsObjects.moreDetailsSkills);
				            System.out.println("The len is : "+ elm.size());
				            				            
							gm.checkWebElementText(driver, elm.get(k), skills[k]);	
						}
						gm.checkElementText(driver, TaskDetailsObjects.commentSectionApplicantName,name[i]);
						gm.checkElementText(driver, TaskDetailsObjects.commentSectionComment,applicationComment);	
						break;
					default:
					
					}
				}
				
			}
		} else {
			gm.checkThings(driver, TaskDetailsObjects.textWhenEmpty(5)/**5 = WITHDRAWN TAB*/, "No resources under WITHDRAWN TAB so text must be displayed");
		}
		
		
		if(withdrawnActions.equals("null")){/**after inspection only*/
			driver.findElement(TaskDetailsObjects.closeParticipantsModal).click();
		Thread.sleep(3000);
		}
		else{
				driver.findElement(TaskDetailsObjects.applyChanges).click();
				gm.waitForObject(driver, TaskDetailsObjects.withdrawnOfferModal);
				driver.findElement(TaskDetailsObjects.withdrawnCancelOffer).click();
				Thread.sleep(1000);
				driver.findElement(TaskDetailsObjects.applyChanges).click();
				gm.waitForObject(driver, TaskDetailsObjects.withdrawnOfferModal);
				driver.findElement(TaskDetailsObjects.withdrawnConfirmOffer).click();
				driver.findElement(TaskDetailsObjects.closeParticipantsModal).click();
				Thread.sleep(3000);
		}
		
	}
	
	public void withdrawnTabActionsByApplicant(WebDriver driver, String data1, String withdrawnActions, String moreDetails, String applyDetails, String applicationComment) throws InterruptedException{
		
		String[]name = data1.split(",");
		String[]actionPerUser = withdrawnActions.split(",");
		
		if(!data1.equals("null")){
			gm.checkThings(driver, TaskDetailsObjects.applyChangesDisabled, "Apply Changes button should be disabled");
			for(int i=0;i<name.length;i++){
				Thread.sleep(2000);
				gm.checkThings(driver, TaskDetailsObjects.rosterNameWithdrawnByPoster(name[i]), "Applicant's name should be displayed - " + name[i]);
				gm.checkThings(driver, TaskDetailsObjects.rosterPositionWithdrawnByPoster(name[i]), "Applicant's position should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterLocationWithdrawnByPoster(name[i]), "Applicant's location should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterDigitalWithdrawnByPoster(name[i]), "Applicant's digital reputation should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterCareerWithdrawnByPoster(name[i]), "Applicant's career level should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterTalentWithdrawnByPoster(name[i]), "Applicant's talent segment should be displayed");
				gm.checkThings(driver, TaskDetailsObjects.rosterOfferWithdrawnByPoster(name[i]), "Poster should be able to offer this applicant - " + name[i]);
				
				String[]action = actionPerUser[i].split("-");
				for(int j=0;j<action.length;j++){
					switch(action[j]){
					case "Offer":
						driver.findElement(TaskDetailsObjects.rosterOffer(name[i])).click();
						break;
					case "Flag":
						driver.findElement(TaskDetailsObjects.rosterLike(name[i])).click();
						Thread.sleep(1000);
						break;
						
					case "More":
						driver.findElement(TaskDetailsObjects.rosterMoreOrLess(name[i])).click();
						gm.checkElementText(driver, TaskDetailsObjects.moreDetailsLabel, applyDetails.substring(3));
						String[]skills = moreDetails.split(",");
						for(int k = 0;k<skills.length;k++){
							List<WebElement> elm = driver.findElements(TaskDetailsObjects.moreDetailsSkills);
				            System.out.println("The len is : "+ elm.size());
				            				            
							gm.checkWebElementText(driver, elm.get(k), skills[k]);	
						}
						
						gm.checkElementText(driver, TaskDetailsObjects.commentSectionApplicantName,name[i]);
						gm.checkElementText(driver, TaskDetailsObjects.commentSectionComment,applicationComment);	
						break;
					default:
						
					}
				}
				
			}
		} else {
			gm.checkThings(driver, TaskDetailsObjects.textWhenEmpty(5)/**5 = WITHDRAWN TAB*/, "No resources under WITHDRAWN TAB so text must be displayed");
		}
		
		
		if(withdrawnActions.equals("null")){/**after inspection only*/
			driver.findElement(TaskDetailsObjects.closeParticipantsModal).click();
		Thread.sleep(3000);
		}
		else{
				driver.findElement(TaskDetailsObjects.applyChanges).click();
				gm.waitForObject(driver, TaskDetailsObjects.withdrawnOfferModal);
				driver.findElement(TaskDetailsObjects.withdrawnCancelOffer).click();
				Thread.sleep(1000);
				driver.findElement(TaskDetailsObjects.applyChanges).click();
				gm.waitForObject(driver, TaskDetailsObjects.withdrawnOfferModal);
				driver.findElement(TaskDetailsObjects.withdrawnConfirmOffer).click();
				driver.findElement(TaskDetailsObjects.closeParticipantsModal).click();
				Thread.sleep(3000);
		}
		
	}
}
