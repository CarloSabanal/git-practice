/**
 * 
 */
package methods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.*;

import driver.GenericMethods;
import pageObjects.DashboardObjects;
import pageObjects.SearchPageObjects;

import java.util.*;


/**
 * @author somnath.bhunia
 * @since- 2/20/2017
 */
public class SearchPageMethods
{
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();

	public String selectFromDropdown(WebDriver driver, By locator, String str)throws InterruptedException 
	{
		String selectedText ="Value is not available";
		int flag = 0;
		if(locator.equals(SearchPageObjects.searchMarketplace))
		{
			List<WebElement> optionList = driver.findElements(By.xpath("//*[@id='divfilterleft']/div[1]/div[1]/span/div/ul[2]/li"));
			//driver.findElement(SearchPageObjects.searchMarketplaceButton).click();
			//Thread.sleep(5000);
			String xpath1 = "//*[@id='divfilterleft']/div[1]/div[1]/span/div/ul[2]/li[";
			String xpath2 = "]/a/label";
			System.out.println("WORKING - " + optionList.size());
			for(int i=2;i<=optionList.size();i++)
			{
				String finalXpath = xpath1 + i + xpath2;
				System.out.println(finalXpath);
				WebElement optionSelected = driver.findElement(By.xpath(finalXpath));
				String optionText = optionSelected.getText();
				System.out.println(optionText);
				if(optionText.trim().equalsIgnoreCase(str.trim()))
				{
					System.out.println("Clicking "+optionText);
					optionSelected.click();
					selectedText = optionText;
					gm.logScreenshot("Pass", "Option selected", driver);
					flag = 1;
					break;
				}
				else
				{
					while(i==12)
					{
						gm.logScreenshot("Fail", "Option - \""+str+"\" is not available", driver);
					}
				}
			}
		}
		else if(locator.equals(SearchPageObjects.searchCategory))
		{
			driver.findElement(SearchPageObjects.searchCategoryButton).click();
		}
		else
		{

			driver.findElement(locator).click();
			Select dropdown = new Select(driver.findElement(locator));
			List<WebElement> allOptions = dropdown.getOptions();
			System.out.println(allOptions.size());
			for(WebElement option:allOptions)
			{

				if(option.getText().trim().equalsIgnoreCase(str.trim()))
				{
					System.out.println(option.getText());
					option.click();
					selectedText = option.getText();
					gm.logScreenshot("Pass", "Selected options from dropdown", driver);
					flag = 1;
					break;

				}
				flag++;

			}
			System.out.println(flag);
			if(flag!=1)
			{
				gm.logScreenshot("Fail", "Option is not available", driver);
			}
		}
		return selectedText;

	}

	public void typeAndSearch(WebDriver driver, String text)throws InterruptedException
	{
		if(gm.elementExist(driver, DashboardObjects.searchBox))
		{
			gm.setLogMsg("Info", "SearchBox located");
		}
		else
		{
			gm.logScreenshot("Fail", "SearchBox could not be located", driver);
		}
		driver.findElement(DashboardObjects.searchBox).sendKeys(text);
		driver.findElement(DashboardObjects.searchBoxArrow).click();

	}

	public void selectCheckBox(WebDriver driver, By locator, String message)
	{
		WebElement chkBox = driver.findElement(locator);

		String msg = message, msg2="";

		if(message.equalsIgnoreCase("Pass"))
		{
			msg2 = "Fail";
		}
		else
		{
			msg2  = "Pass";
		}

		if(chkBox.isEnabled())
		{
			chkBox.click();
			gm.logScreenshot(msg, "Checkbox is enabled", driver);
			if(chkBox.isSelected())
			{
				gm.logScreenshot(msg, "Checkbox is selected", driver);
			}
			else
			{
				gm.logScreenshot(msg2, "Checkbox is not selected", driver);
			}
		}
		else
		{
			gm.logScreenshot(msg2, "Checkbox is disabled", driver);
		}

	}

}
