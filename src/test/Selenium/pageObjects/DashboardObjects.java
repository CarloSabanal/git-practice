package pageObjects;

import org.openqa.selenium.By;

public class DashboardObjects {
	public static By menuHome = By.xpath("//*[@rel='Dashboard']");
	public static By mpCarousel = By.xpath("//div[@id='carousel']");
	
	public static By carousel = By.xpath("//div[@class='owl-wrapper']/div");
	public static By marketPlace(int i){return By.xpath("(//div[@class='owl-item'])["+i+"]");}
	public static By marketPlaceName(int i){return By.xpath("(//div[@class='owl-item'])["+i+"]//*[@class='marketplace-name']");}
	public static By marketPlaceOptions(int i){return By.xpath("(//div[@class='owl-item'])["+i+"]//*[@class='marketplace-options']");}
	public static By marketPlaceLogo(int i){return By.xpath("((//div[@class='owl-item'])["+i+"]//img)[1]");}
	public static By marketPlaceMembers(int i){return By.xpath("(//div[@class='owl-item'])["+i+"]//*[@class='marketplace-stats']/span[1]");}
	public static By marketPlaceOpenTask(int i){return By.xpath("(//div[@class='owl-item'])["+i+"]//*[@class='marketplace-stats']/span[2]");}
	public static By searchBox = By.id("TaskSearchInput");
	public static By searchBoxArrow = By.xpath("//*[@id='app-container']//div[1]/div/img[@class = 'icn_search_bar-copy-2']"); 
	
	public static By marketingTileFK(int i){return By.xpath("(//div[@class='owl-item'])["+i+"]//*[@class='marketing-tile-FK']");}
	public static By marketingTileSK(int i){return By.xpath("(//div[@class='owl-item'])["+i+"]//*[@class='marketing-tile-SK']");}
	
	public static By btnNext = By.xpath("//div[@class='owl-next']");
	public static By btnPrev = By.xpath("//div[@class='owl-prev']");
	
	public static By marketplaceList = By.xpath("//*[@class='MarketplaceList']"); 
	
	/**Post Section*/
	public static By inProgressPosts(String active){return By.xpath("//li[@class='postBoxNavigation']//descendant::span[@class='txt_dash_tab' and text()='IN PROGRESS']");}
	public static By completePosts(String active){return By.xpath("//li[@class='postBoxNavigation']//descendant::span[@class='txt_dash_tab' and text()='COMPLETE']");}
	public static By expandPost = By.xpath("//li[@class='txt_dash_header' and text()='Posts']/ancestor::div[@class='row']/descendant::*[@class='post-toggle-button fa fa-chevron-down fa-2x']");
	
	/**post card elements*/
	public static By postCardTaskTitle(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Posts']/ancestor::div[@class='row']/descendant::div[@class='txt_card_title'])["+i+"]");}
	public static By postCardMarketplaceName(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Posts']/ancestor::div[@class='row']/descendant::div[@class='txt_marketplace_card'])["+i+"]");}
	public static By postCardEffort(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Posts']/ancestor::div[@class='row']/descendant::div[@class='txt_card_effort'])["+i+"]");}
	public static By postCardStartDate(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Posts']/ancestor::div[@class='row']/descendant::div[@class='txt_start_block'])["+i+"]");}
	public static By postCardEndDate(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Posts']/ancestor::div[@class='row']/descendant::div[@class='txt_end_block'])["+i+"]");}
	public static By postCardContact(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Posts']/ancestor::div[@class='row']/descendant::div[@class='txt_marketplace_card'])["+i+"]");}
	public static By postCardDescription(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Posts']/ancestor::div[@class='row']/descendant::div[@class='txt_card_description'])["+i+"]");}
	public static By postCardActivityInfo1(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Posts']/ancestor::div[@class='row']/descendant::div[@class='txt_card_activityInfo'])["+i+"]");}
	public static By postCardActivityInfo2(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Posts']/ancestor::div[@class='row']/descendant::div[@class='txt_card_activityInfo'])["+i+"]");}
	
	/**Task Section*/
	public static By inProgressTasks(String active){return By.xpath("//li[@class='taskBoxNavigation"+ active +"']//descendant::span[@class='txt_dash_tab' and text()='IN PROGRESS']");}
	public static By completeTasks(String active){return By.xpath("//li[@class='taskBoxNavigation"+ active +"']//descendant::span[@class='txt_dash_tab' and text()='COMPLETE']");}
	public static By suggestedTasks(String active){return By.xpath("//li[@class='taskBoxNavigation"+ active +"']//descendant::span[@class='txt_dash_tab' and text()='SUGGESTED']");}
	public static By appliedTasks(String active){return By.xpath("//li[@class='taskBoxNavigation"+ active +"']//descendant::span[@class='txt_dash_tab' and text()='APPLIED']");}
	public static By expandTask = By.xpath("//li[@class='txt_dash_header' and text()='Tasks']/ancestor::div[@class='row']/descendant::*[@class='task-toggle-button fa-2x fa fa-chevron-down']");
	
	/**post card elements*/
	public static By taskCardTaskTitle(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Tasks']/ancestor::div[@class='row']/descendant::div[@class='txt_card_title'])["+i+"]");}
	public static By taskCardMarketplaceName(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Tasks']/ancestor::div[@class='row']/descendant::div[@class='txt_marketplace_card'])["+i+"]");}
	public static By taskCardEffort(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Tasks']/ancestor::div[@class='row']/descendant::div[@class='txt_card_effort'])["+i+"]");}
	public static By taskCardStartDate(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Tasks']/ancestor::div[@class='row']/descendant::div[@class='txt_start_block'])["+i+"]");}
	public static By taskCardEndDate(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Tasks']/ancestor::div[@class='row']/descendant::div[@class='txt_end_block'])["+i+"]");}
	public static By taskCardContact(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Tasks']/ancestor::div[@class='row']/descendant::div[@class='txt_marketplace_card'])["+i+"]");}
	public static By taskCardDescription(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Tasks']/ancestor::div[@class='row']/descendant::div[@class='txt_card_description'])["+i+"]");}
	public static By taskCardActivityInfo(int i){return By.xpath("(//li[@class='txt_dash_header' and text()='Tasks']/ancestor::div[@class='row']/descendant::div[@class='txt_card_activityInfo'])["+i+"]");}
	
	
	
	
}
