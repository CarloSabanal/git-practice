package scripts;

import java.util.Set;

import javax.swing.JOptionPane;

/**5 minutes execution time*/

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import methods.ALW_Methods;
import methods.ApplicantMethods;
import methods.FooterMethods;
import methods.JobPosterActionsInTaskDetailsPageMethods;
import methods.PostSectionMethods;
import methods.PostTaskMethods;
import methods.ValidateTaskDetailsMethods;
import pageObjects.FilterObjects;
import pageObjects.HeaderObjects;
import pageObjects.PostTaskObjects;
import pageObjects.TaskDetailsObjects;


public class PosterApplicantFlow {
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();
	PostTaskMethods ptm = new PostTaskMethods();
	ALW_Methods am = new ALW_Methods();
	ValidateTaskDetailsMethods tdm = new ValidateTaskDetailsMethods();
	ApplicantMethods afj = new ApplicantMethods();
	JobPosterActionsInTaskDetailsPageMethods jpa = new JobPosterActionsInTaskDetailsPageMethods();
	
	@BeforeClass()
	public void openBrowser() throws InterruptedException	
	{	
		gm.setLogMsg("info", "Starting Post - Apply Sequences");	
		gm.clearLogs();
	}

	/**
	 * Sprint 4
	 * Description: ALW ReArch Regression - Post Task Section
	 * @author jan.carlo.l.sabanal
	 * @since 2016/12/19
	 * 
	 */
	
	String applyDetails = null;
	
	@Test(priority = 0, description ="ALW",dataProvider="TestData")
	@Parameters("sheetName")
	public void testPostsTask(String flag, String scriptId, String action, String data1, String data2, String data3) throws InterruptedException	
	{
		
		if(flag.equals("y")){
			
			By element = By.xpath(data3);
			
			switch(action){
			
			case "NewSession":
				
				Thread.sleep(5000);
				System.setProperty(EnvironmentVariables.driverType, EnvironmentVariables.driverPath);
				driver = new ChromeDriver();//InternetExplorerDriver();ChromeDriver
				gm.setLogMsg("Info", "Successfully launched the browser");
				driver.manage().window().maximize();
				driver.navigate().to(EnvironmentVariables.URL);
				break;
				
			case "Login":
				
				am.login(driver, data1, EnvironmentVariables.PASSWORD);
				gm.waitForObject(driver, HeaderObjects.header);
				driver.navigate().to(EnvironmentVariables.TaskURL + data2);
				Thread.sleep(5000);
				break;
				
			case "ApplyToTask":
				
				String[]applyToTaskData = data1.split(",");
				afj.applyForJob(driver, "test apply", applyToTaskData[1], applyToTaskData[2]);
				applyDetails = driver.findElement(TaskDetailsObjects.applicationMessageAfterApplying).getText();
				System.out.println(applyDetails);
				break;
			
			case "CheckStatus":
				
				/**11126	User can see the Open - Offered status in the Task Details read mode screen
				 * 11128	User can see the Completed status in the Task Details read mode screen.
				 * 11127	User can see the Accepted status in the Task Details read mode screen.
				 *
				 */
				
				gm.waitForObject(driver, TaskDetailsObjects.taskStatus);
				String taskStatus = driver.findElement(TaskDetailsObjects.taskStatus).getText();
				if(taskStatus.equals(data1))
					gm.setLogMsg("Pass", "Task Status is correct");
				else
					gm.logScreenshot("Fail", "Task Status is incorrect - " + taskStatus, driver);
				break;
				
			case "CheckThings":
				/**14049	Applicant can see that the offer made by Task Poster was withdrawn when I access the Task Details page.
				 * 14052	Applicant able to see my status in the Task Details page as Declined.
				 * 14057	Applicant able to see that the status of my task is completed in the Task Details read mode screen.
				 * */
				gm.checkThings(driver, element, data2);
				break;
				
			case "CloseSession":
					
				driver.quit();
				break;
				
			case "Wait":
				
				gm.waitForObject(driver, element);
				break;
				
			case "ParticipantCount"://check participant count displayed in the participants section
				Thread.sleep(2000);
				String participantCount = driver.findElement(TaskDetailsObjects.participantCount(data2)).getText();
				if(participantCount.equals(data1))
					gm.setLogMsg("Pass", "Participant count is correct");
				else
					gm.logScreenshot("Fail", "Participant count is incorrect", driver);
				break;
				
			case "Click":
				
				driver.findElement(element).click();
				break;
				
			case "StaffCount":
				
				String staffCount = driver.findElement(TaskDetailsObjects.staffCount).getText();
				if(staffCount.equals(data1 + " of " + data2 + " Resources Staffed"))
					gm.setLogMsg("Pass", "Staff count is correct");
				else
					gm.logScreenshot("Fail", "Staff count is incorrect", driver);
				break;
				
			case "InspectManageParticipantsModal":
				
				tdm.inspectManageParticipantsMethods(driver, data1);
				break;
				
			case "AppliedTab":
				System.out.println(applyDetails);
				
				tdm.applyTabActions(driver, data1, data2, data3, applyDetails, "test apply");
				break;
				
			case "WithdrawApplication":
				gm.waitForObject(driver, TaskDetailsObjects.applicantWithdrawButton);
				driver.findElement(TaskDetailsObjects.applicantWithdrawButton).click();
				Thread.sleep(5000);
				break;	
				
			case "OfferedTab":
				System.out.println(applyDetails);
				
				tdm.offerTabActions(driver, data1, data2, data3, applyDetails, "test apply");
				break;	

			case "AcceptedTab":
				System.out.println(applyDetails);
				
				tdm.acceptedTabActions(driver, data1, data2, data3, applyDetails, "test apply");
				break;	
				
			case "DeclinedTab":
				System.out.println(applyDetails);
				
				tdm.declinedTabActions(driver, data1, data3, applyDetails, "test apply");
				break;	
				
			case "WithdrawnTab":
				System.out.println(applyDetails);
				
				tdm.withdrawnTabActionsByPoster(driver, data1, data2, data3, applyDetails, "test apply");
				break;	
				
			case "WithdrawnTab2":
				System.out.println(applyDetails);
				
				tdm.withdrawnTabActionsByApplicant(driver, data1, data2, data3, applyDetails, "test apply");
				break;
				
			case "RespondToOffer":
				tdm.respondToOffer(driver, data1, data2, data3);
				break;
				
				
			case "CompleteTask":
				tdm.completeTask(driver);
				break;
				
			default:
				
				gm.setLogMsg("Info", action + " - " + data1);
			
			}
			
			
		}
		
	}
	
	@DataProvider(name = "TestData")
	public Object[][] getValueFromExcel(ITestContext context)
	{
		String sheetName = "PosterApplicantSingle";//PosterApplicantMulti//PosterApplicantSingle// context.getCurrentXmlTest().getParameter("sheetName").toString().trim();
		Object[][] data = gm.getExcelData(EnvironmentVariables.dataPoolPath, sheetName);		
		return data;
	}
	

	@AfterClass
	public void closeTheAction() throws InterruptedException
	{
		Thread.sleep(5000);
		JOptionPane.showMessageDialog(null,"Execution is done. To close browser, press ok");
		//driver.close();
		gm.setLogMsg("info", "END of Post - Apply Sequences");		
	}
}