package pageObjects;

import org.openqa.selenium.By;

public class TaskDetailsObjects {
	
	/**header*/
	public static By taskTitle = By.xpath("//div[@id='taskHeader']/div[@class='taskTitle']");
	public static By poster = By.xpath("//div[@id='taskHeader']/div/span[@class='taskPoster']");
	public static By postedDate = By.xpath("//div[@id='taskHeader']/div/span[@class='taskPostedDate']");
	public static By newTag = By.xpath("//div[@id='taskHeader']/div[@class='taskTitle']/span[@class='taskStatus' and text()='New']");
	
	
	/**additional details*/
	public static By supervisorInfoYes = By.xpath("//div[@id='taskSupervisorApproval']//p[@class='sectionContent' and text()='Yes']");
	public static By supervisorInfoNo = By.xpath("//div[@id='taskSupervisorApproval']//p[@class='sectionContent' and text()='No']");
	public static By supervisorInfo = By.xpath("//div[@id='taskSupervisorApproval']");
	public static By urgentTag = By.xpath("//div[@id='taskHeader']/div/span[@id='status-urgent' and contains(@style,'display: block;')]");
	public static By withdrawnTag = By.xpath("//div[@id='taskHeader']/div/span[@id='status-withdrawn' and contains(@style,'display: block;')]");
	public static By urgentTagHidden = By.xpath("//div[@id='taskHeader']/div/span[@id='status-urgent']");
	public static By wbsDiv = By.id("taskChargeability");
	public static By wbsNo = By.xpath("//div[@id='taskChargeability']//p[text()='WBS Provided?']/ancestor::div/p[@class='sectionContent' and text()='No']");
	public static By wbsYes = By.xpath("//div[@id='taskChargeability']//p[text()='WBS Provided?']/ancestor::div/p[@class='sectionContent' and text()='Yes']");
	public static By wbsChargeNo = By.xpath("//div[@id='taskChargeability']//p[text()='Chargeable?']/ancestor::div/p[@class='sectionContent' and text()='No']");
	public static By wbsChargeYes = By.xpath("//div[@id='taskChargeability']//p[text()='Chargeable?']/ancestor::div/p[@class='sectionContent' and text()='Yes']");
	public static By multiNo = By.xpath("//div[@id='taskResourceRequired']//p[text()='Multi Offer?']/ancestor::div/p[@class='sectionContent' and text()='No']");
	public static By multiYes = By.xpath("//div[@id='taskResourceRequired']//p[text()='Multi Offer?']/ancestor::div/p[@class='sectionContent' and text()='Yes']");
	public static By multiResource = By.xpath("//div[@id='taskResourceRequired']//p[text()='Resources Needed']/ancestor::div/p[@class='sectionContent']");
	public static By workLocationDiv = By.id("taskLocation");
	public static By workLocationVirtual = By.xpath("//div[@id='taskLocation']//p[@class='sectionContent' and text()='Virtual']");
	public static By workLocationDetails = By.xpath("//div[@id='taskLocation']//p[@class='sectionContent']");
	public static By postOnBehalfOf = By.xpath("//div[@id='taskDelegate']//p[@class='sectionContent']");
	public static By behalfDiv = By.id("taskDelegate");
	
	/**subheader*/
	public static By subHeaderLocation = By.xpath("//div[@id='taskSubHeader']/ul/li[1]");
	public static By marketplace = By.xpath("//div[@id='taskSubHeader']/ul/li[2]");
	public static By category = By.xpath("//div[@id='taskSubHeader']/ul/li[3]");
	public static By effort = By.xpath("//div[@id='taskSubHeader']/ul/li[4]");
	public static By taskStatus = By.xpath("//div[@id='taskSubHeader']/ul/li[@id='OpenClose']/span");
	
	/**taskDates*/
	public static By startDate = By.xpath("//div[@id='taskDates']/ul/li[1]");
	public static By endDate = By.xpath("//div[@id='taskDates']/ul/li[2]");
	public static By expireDate = By.xpath("//div[@id='taskDates']/ul/li[3]");
	
	/**taskWorkTime*/
	public static By workTimeDiv = By.id("taskWorkTime");
	public static By startTime = By.xpath("//div[@id='taskWorkTime']//p[text()='Work Day Start']/ancestor::div/p[@class='sectionContent']");
	public static By endTime = By.xpath("//div[@id='taskWorkTime']//p[text()='Work Day End']/ancestor::div/p[@class='sectionContent']");
	public static By timeZone = By.xpath("//div[@id='taskWorkTime']//p[text()='Work Day Timezone']/ancestor::div/p[@class='sectionContent timezone']");
	
	/**Filters*/
	public static By languageFilters = By.xpath("//div[@role='tabpanel'][1]//div[@id='languageFilter']//p[@class='sectionTitle' and text()='Language Proficiency']");
	public static By languageFilterSelected(String language){return By.xpath("//div[@role='tabpanel'][1]//div[@id='languageFilter']//p[@class='sectionContent' and text()='" + language + "']");}
	
	public static By resourceLocFilters = By.xpath("//div[@role='tabpanel'][1]//div[@id='locationfilter']//p[@class='sectionTitle' and text()='Resource Location']");
	public static By resourceLocFilterSelected(String loc){return By.xpath("//div[@role='tabpanel'][1]//div[@id='locationfilter']//p[@class='sectionContent' and contains(text(),'"+loc+"')]");}
	
	public static By careerLevelFilters = By.xpath("//div[@role='tabpanel'][1]//div[@id='CareerLevel']//p[@class='sectionTitle' and text()='Career Level']");
	public static By careerLevelFilterSelected(String level){return By.xpath("//div[@role='tabpanel'][1]//div[@id='CareerLevel']//p[@class='sectionContent' and contains(text(),'" + level + " - ')]");}
	public static By careerLevelFilterLDR = By.xpath("//div[@role='tabpanel'][1]//div[@id='CareerLevel']//p[@class='sectionContent' and contains(text(),'Accenture Leadership')]");
	public static By careerLevelFilterDisplayed(String n){return By.xpath("//div[@role='tabpanel'][1]//div[@id='CareerLevel']//p[@class='sectionContent'][n]");}
	
	public static By talentSegmentFilters = By.xpath("//div[@role='tabpanel'][1]//div[@id='TalentSegment']//p[@class='sectionTitle' and text()='Talent Segment']");
	public static By talentSegmentFilterSelected(String talent){return By.xpath("//div[@role='tabpanel'][1]//div[@id='TalentSegment']//p[@class='sectionContent' and contains(text(),'"+talent+"')]");}
	
	public static By orgUnitFilters = By.xpath("//div[@role='tabpanel'][1]//div[@id='OrganizationUnit']//p[@class='sectionTitle' and text()='Organization Unit']");
	public static By orgUnitFilterSelected(String org){return By.xpath("//div[@role='tabpanel'][1]//div[@id='OrganizationUnit']//p[@class='sectionContent' and contains(text(),'"+org+"')]");}
	
	public static By specialtyFilters = By.xpath("//div[@role='tabpanel'][1]//div[@id='Specialty']//p[@class='sectionTitle' and text()='Specialty']");
	public static By specialtyFilterSelected(String specialty){return By.xpath("//div[@role='tabpanel'][1]//div[@id='Specialty']//p[@class='sectionContent' and contains(text(),'"+specialty+"')]");}
	
	
	/**comments section*/
	public static By commentSection = By.xpath("//div[@class='commentBox']");
	public static By commentField = By.xpath("//div[@class='commentBox']/form/textarea[@placeholder='Type comment here...']");
	public static By commentButtonHidden = By.xpath("//div[@id='ComButtonDiv' and @style='display: none;']");
	public static By commentCancel = By.xpath("//div[@id='ComButtonDiv' and @style='display: block;']/button/span[text()='CANCEL']");
	public static By commentSubmit = By.xpath("//div[@id='ComButtonDiv' and @style='display: block;']/button/span[text()='COMMENT']");
	public static By commentPosted = By.xpath("//div[@class='commentBox']/div[@class='commentList']/div[@class='comment showComment']/div[@class='comment-content']/span");
	public static By commentNewPost(int index){return By.xpath("//div[@class='commentBox']/div[@class='commentList']/div[@class='comment showComment']["+index+"]/div[@class='comment-content']/span");}
	
	/**job poster actions*/
	public static By withdrawButton = By.xpath("//li[@id='poster-withdraw-container']/button");
	public static By withdrawModal = By.xpath("//p[text()='All activity for this task will be removed. This action cannot be undone.']/ancestor::div[@class='modal-content']");
	public static By withdrawModalHeader = By.xpath("//div[@class='col-md-10 popup-modal-header-text']");
	public static By withdrawModalHeaderTitle = By.xpath("//div[@class='col-md-10 popup-modal-header-text']/span");
	public static By withdrawModalInput = By.xpath("//p[text()='All activity for this task will be removed. This action cannot be undone.']/ancestor::div[@class='modal-content']//input[@placeholder='You must enter a reason for withdrawing this task ...']");
	public static By withdrawModalSubmitHidden = By.xpath("//div[@class='popup-modal-footer modal-footer']//button[@id='withdrawButton' and @disabled]");
	public static By withdrawModalSubmit = By.xpath("//div[@class='popup-modal-footer modal-footer']//button[@id='withdrawButton']");
	public static By withdrawModalCancel = By.xpath("//div[@class='popup-modal-footer modal-footer']//button[@id='cancelButton']");
	
	
	public static By copyButton = By.xpath("//li[@id='poster-copy-container']/button");
	
	public static By deleteButton = By.xpath("//li[@id='poster-delete-container']/button");
	public static By deleteModalCancel = By.xpath("//button[@class='btn-delete-close-DeleteTask btn btn-default']");
	public static By deleteModalSubmit = By.xpath("//button[@class='btn-delete-confirm-DeleteTask btn btn-default']");
	public static By deleteModal = By.xpath("//div[text()='All activity for this task will be removed. This task cannot be undone.']/ancestor::div[@class='modal-content']");
	public static By deleteModalHeader = By.xpath("//div[@class='lblActivityTaskDelete']");
	public static By deleteModalHeaderTitle = By.xpath("//div[@class='lblActivityTaskDelete']/b");
	
	public static By editButton = By.xpath("//li[@id='poster-edit-container']/button");
	
	public static By completeButton = By.xpath("//li[@id='poster-markcomplete-container']/button");
	public static By completeTaskModal = By.xpath("//div[@class='popup-modal-container completetask']");
	public static By completeTaskModalCancel = By.xpath("//div[@class='popup-modal-container completetask']//button[@id='cancelButton']");
	public static By completeTaskModalConfirm = By.xpath("//div[@class='popup-modal-container completetask']//button[@id='markCompleteButton']");
	
	
	
	/**applicant actions*/
	public static By applyButton = By.xpath("//li[@id='applicant-apply-container']/button");
	public static By applyModal = By.xpath("//div[@class='apply-modal-container']");
	public static By applyModalTexts1 = By.xpath("//div[@class='apply-modal-container']/div/div/label[text()='Awesome']");
	public static By applyModalTexts2 = By.xpath("//label[@class='lbl-apply']");
	public static By superApproval = By.xpath("//label[@class='lbl-supervisor-approval' and text()='I have approval from my supervisor to apply for this task.']");
	public static By superApprovalCheckBox = By.xpath("//input[@class='chk-supervisor-approval']");
	public static By applyTextInputArea = By.xpath("//textarea[@placeholder='Is there anything else you’d like to add about yourself?']");
	public static By cancelApplyButton = By.id("cancelApplyButton");
	public static By modalApplyButton = By.id("applyButton");
	public static By modalApplyButtonDisabled = By.xpath("//button[@id='applyButton' and @disabled]");
	public static By applicantWithdrawButton = By.id("applicant-withdraw");
	
	
	/**manage participants*/
	public static By participantsSection = By.id("manageApps");
	public static By participantCount(String status){return By.xpath("//div[@id='manageApps']/ul[@class='statusCount']/li[@class='" + status +"']");}
	public static By manageParticipants = By.xpath("//button[@class='manageApplications-btn']");
	public static By staffCount = By.xpath("//p[@class='staffCount']");
	public static By modalStaffCount = By.xpath("//p[@class='manageApplicationCount']");
	public static By manageParticipantsModal = By.xpath("//div[@class='reviewTaskContainer']");
	public static By closeParticipantsModal = By.xpath("//i[@class='fa fa-times-circle-o']");
	public static By manageParticipantsModalTitle = By.xpath("//p[@class='manageApplicationSummary']");
	public static By tabsActive(String tab){return By.xpath("//li[@class='taskBoxNavigation active']/a/span[text()='"+tab+"']");}
	public static By tabsInactive(String tab){return By.xpath("//li[@class='taskBoxNavigation']/a/span[text()='"+tab+"']");}
	public static By tabsStatusCtr(String tab){return By.xpath("//span[text()='"+tab+"']/ancestor::li[@role='presentation']/a/i");}
	public static By textWhenEmpty(int tab){return By.xpath("//div[@role='tabpanel']["+tab+"]/div/p");}
	
	/**APPLICANT ROSTER*/
	public static By rosterName(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@class='avatar_name']/a[text()="+name+"']");}
	public static By rosterPosition(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::div/p[@class='avatar_position']");}
	public static By rosterLocation(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::div/p[@class='avatar_location']");}
	public static By rosterDigital(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='digitalRep']");}
	public static By rosterCareer(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='careerLevel']");}
	public static By rosterTalent(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='talentSegment']");}
	public static By rosterLike(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_likeApplicant']");}
	public static By rosterOffer(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_offerApplicant']");}
	public static By rosterDecline(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_declineApplicant']");}
	public static By rosterMoreOrLess(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_more']");}
	public static By rosterWithdrawOffer(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_withdrawApplicant']");}
	public static By rosterAcceptedLabel(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_acceptedApplicant']");}
	public static By rosterWithdrawnLabel(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_withdrawApplicant']");}
	public static By rosterDeclinedLabel(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_declineApplicant']");}
	
	/**WITHDRAWN BY POSTER*/
	public static By rosterNameWithdrawnByPoster(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='lbl-withdrawn-poster']/following-sibling::ul//p[@class='avatar_name']/a[text()="+name+"']");}
	public static By rosterPositionWithdrawnByPoster(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='lbl-withdrawn-poster']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::div/p[@class='avatar_position']");}
	public static By rosterLocationWithdrawnByPoster(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='lbl-withdrawn-poster']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::div/p[@class='avatar_location']");}
	public static By rosterDigitalWithdrawnByPoster(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='lbl-withdrawn-poster']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='digitalRep']");}
	public static By rosterCareerWithdrawnByPoster(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='lbl-withdrawn-poster']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='careerLevel']");}
	public static By rosterTalentWithdrawnByPoster(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='lbl-withdrawn-poster']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='talentSegment']");}
	public static By rosterLikeWithdrawnByPoster(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='lbl-withdrawn-poster']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_likeApplicant']");}
	public static By rosterOfferWithdrawnByPoster(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='lbl-withdrawn-poster']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_offerApplicant']");}
	public static By rosterMoreOrLessWithdrawnByPoster(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='lbl-withdrawn-poster']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_more']");}
	
	
	/**WITHDRAWN BY APPLICANT*/
	public static By rosterNameWithdrawnByApplicant(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='withdrawn-horizontal-line']/following-sibling::ul//p[@class='avatar_name']/a[text()="+name+"']");}
	public static By rosterPositionWithdrawnByApplicant(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='withdrawn-horizontal-line']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::div/p[@class='avatar_position']");}
	public static By rosterLocationWithdrawnByApplicant(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='withdrawn-horizontal-line']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::div/p[@class='avatar_location']");}
	public static By rosterDigitalWithdrawnByApplicant(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='withdrawn-horizontal-line']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='digitalRep']");}
	public static By rosterCareerWithdrawnByApplicant(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='withdrawn-horizontal-line']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='careerLevel']");}
	public static By rosterTalentWithdrawnByApplicant(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='withdrawn-horizontal-line']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='talentSegment']");}
	public static By rosterLikeWithdrawnByApplicant(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='withdrawn-horizontal-line']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_likeApplicant']");}
	public static By rosterOfferWithdrawnByApplicant(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='withdrawn-horizontal-line']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_offerApplicant']");}
	public static By rosterMoreOrLessWithdrawnByApplicant(String name){return By.xpath("//div[@class='active tab-pane fade in']//p[@id='withdrawn-horizontal-line']/following-sibling::ul//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_more']");}
	
	/**more details*/
	public static By moreDetailsLabel = By.id("lbl-application-date");
	public static By moreDetailsSkills = By.xpath("//div[@class='lbl-div-skills']/p[@id='skills']");
	public static By commentSectionApplicantName = By.xpath("//div[@class='div-comments']//p[@class='applicant-name']");
	public static By commentSectionComment = By.xpath("//div[@class='div-comments']//p[@class='more-comment']");
	
	/*
	public static By rosterName(String name, String i){return By.xpath("//div[@class='active tab-pane fade in']["+i+"]//p[@class='avatar_name']/a[text()="+name+"']");}
	public static By rosterPosition(String name, String i){return By.xpath("//div[@class='active tab-pane fade in']["+i+"]//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::div/p[@class='avatar_position']");}
	public static By rosterLocation(String name, String i){return By.xpath("//div[@class='active tab-pane fade in']["+i+"]//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::div/p[@class='avatar_location']");}
	public static By rosterDigital(String name, String i){return By.xpath("//div[@class='active tab-pane fade in']["+i+"]//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='digitalRep']");}
	public static By rosterCareer(String name, String i){return By.xpath("//div[@class='active tab-pane fade in']["+i+"]//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='careerLevel']");}
	public static By rosterTalent(String name, String i){return By.xpath("//div[@class='active tab-pane fade in']["+i+"]//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='talentSegment']");}
	public static By rosterOffer(String name, String i){return By.xpath("//div[@class='active tab-pane fade in']["+i+"]//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_offerApplicant']");}
	public static By rosterDecline(String name, String i){return By.xpath("//div[@class='active tab-pane fade in']["+i+"]//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_declineApplicant']");}
	public static By rosterWithdrawOffer(String name, String i){return By.xpath("//div[@class='active tab-pane fade in']["+i+"]//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_withdrawApplicant']");}
	public static By rosterAcceptedLabel(String name, String i){return By.xpath("//div[@class='active tab-pane fade in']["+i+"]//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_acceptedApplicant']");}
	public static By rosterWithdrawnLabel(String name, String i){return By.xpath("//div[@class='active tab-pane fade in']["+i+"]//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_withdrawApplicant']");}
	public static By rosterDeclinedLabel(String name, String i){return By.xpath("//div[@class='active tab-pane fade in']["+i+"]//p[@class='avatar_name']/a[text()='"+name+"']/ancestor::li//div[@class='btn_declineApplicant']");}
	*/
	
	
	
	public static By applyChangesDisabled = By.xpath("//div[@class='active tab-pane fade in']//button[@class='done-btn' and @disabled]");
	public static By applyChanges = By.xpath("//div[@class='active tab-pane fade in']//button[@class='done-btn']");
	public static By declinedModal = By.xpath("//div[@class='modal-body-container-declined']");
	public static By cancelDecline = By.xpath("//button[@class='manageApp-cancel']");
	public static By confirmDecline = By.xpath("//button[@class='manageApp-Submit']");
	public static By offerModal = By.xpath("//div[@class='offer-message-container']");//("//div[@class='offer-modal-container']");
	public static By cancelOffer = By.xpath("//button[@class='Offer_manageApp-cancel']");//("//button[@class='manageApp-cancel']");
	public static By confirmOffer = By.xpath("//button[@class='Offer_manageApp-Submit']");//("//button[@class='manageApp-Submit']");
	public static By withdrawOfferModal = By.xpath("//div[@class='withdraw-message-container']");
	public static By cancelWithdraw = By.xpath("//div[@class='withdraw-message-container']//button[text()='CANCEL']");
	public static By withdrawOffer = By.xpath("//div[@class='withdraw-message-container']//button[text()='WITHDRAW']");
	public static By withdrawnOfferModal = By.xpath("//div[@class='offer-message-container']");
	public static By withdrawnCancelOffer = By.xpath("//button[@class='Offer_manageApp-cancel']");
	public static By withdrawnConfirmOffer = By.xpath("//button[@class='Offer_manageApp-Submit']");
	
	/**Applicant after being offered*/
	public static By applicantFirstTimeOfferModal = By.xpath("//div[@class='display-dialog modal-dialog']");
	public static By congratsMessage = By.xpath("//div[@class='display-dialog modal-dialog']//div[@class='userAction-message']");
	public static By nothingNowMessage = By.xpath("//div[@class='nothingNowMessage']");
	public static By nothingNowOk = By.xpath("//div[@class='nothingNowActions']/button");
	public static By nothingNowButton = By.xpath("//div[@class='userAction-modal-footer modal-footer']/button[text()='NOTHING NOW']");
	
	public static By declineButton = By.xpath("//div[@class='userAction-modal-footer modal-footer']/button[text()='DECLINE OFFER']");
	public static By declineModal = By.xpath("//div[@class='display-dialog modal-dialog']//i[@class='fa fa-frown-o large-fonticon']");
	public static By declineModalMessage1 = By.xpath("//div[@class='display-dialog modal-dialog']//p[text()='We’re sorry you can’t participate.']");
	public static By declineModalReason = By.xpath("//div[@class='display-dialog modal-dialog']//div/select/option[2]");
	public static By declineModalOk = By.xpath("//div[@class='display-dialog modal-dialog']//div/button[text()='DECLINE OFFER']");
	public static By declineModalCancel = By.xpath("//div[@class='display-dialog modal-dialog']//div/button[text()='CANCEL']");
	
	public static By acceptButton = By.xpath("//div[@class='userAction-modal-footer modal-footer']/button[text()='ACCEPT OFFER']");
	public static By acceptModal = By.xpath("//div[@class='display-dialog modal-dialog']//i[@class='fa fa-smile-o large-fonticon']");
	public static By acceptModalMessage1 = By.xpath("//div[@class='display-dialog modal-dialog']//p[text()='We\'re happy to have you on board!']");
	public static By acceptModalMessage2 = By.xpath("//div[@class='display-dialog modal-dialog']//p[contains(text(),'The task is scheduled to start in ')]");
	public static By acceptModalOk = By.xpath("//div[@class='display-dialog modal-dialog']//div/button[text()='ACCEPT OFFER']");
	public static By acceptModalCancel = By.xpath("//div[@class='display-dialog modal-dialog']//div/button[text()='CANCEL']");
	
	/**others*/
	public static By applicationMessageAfterOffer = By.xpath("//label[@class='lbl-application-date' and contains(text(),'You were offered this task on ')]");
	public static By applicationMessageAfterAccept = By.xpath("//label[@class='lbl-application-date' and contains(text(),'You accepted this task on ')]");
	public static By applicationMessageAfterDecline = By.xpath("//label[@class='lbl-application-date' and contains(text(),'You declined the offer to this task on')]");
	public static By applicationCompletionDetails = By.xpath("//label[@class='lbl-application-date']/span[contains(text(),'This task was completed on ')]");
	public static By applicationDeclinedDetails = By.xpath("//label[@class='lbl-application-date' and contains(text(),'Your application to this task has been declined on ')]");
	public static By applicationWithdrawnOffer = By.xpath("//label[@class='lbl-application-date']/span[contains(text(),'Your offer was withdrawn by ')]");
	public static By acceptStickyHeaderButton = By.xpath("//span[@id='accept-container']/button");
	public static By declineStickyHeaderButton = By.xpath("//span[@id='decline-container']/button");
	public static By applicationMessageAfterApplying = By.xpath("//label[@class='lbl-application-date']");
	
	
	
}