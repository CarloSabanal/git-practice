package pageObjects;

import org.openqa.selenium.By;

public class PostTaskObjects {
	
	/**info texts*/
	public static By attachInfo = By.xpath("//label[@class='lable-nm control-label lblupload']/ancestor::div[@class='col-sm-1']//i[@class='information-icon fa fa-info-circle']");
	public static By attachInfoText = By.xpath("//body//div//div[@class='tooltip-inner' and text()='Ensure that there is no Confidential data attached. If Confidential data needs to be exchanged, please do so over email.']");
	
	
	/**main*/
	public static By home = By.xpath("//nav//ul/li//font[text()='HOME']");
	public static By marketingTile = By.xpath("//div[@id='carousel']//div[@class='owl-item'][2]//img[contains(@src,'marketing.png')]");
	
	public static By saveButton = By.xpath("//div[@id='divSubmit']//button[@class='btnpostupload btn btn-default']");
	//public static By confirmSave = By.xpath("//div[@class='review-wrapper']//div[@class='review-overview-close']");
	
	/**crowd count*/
	public static By crowdCountMessageHidden = By.xpath("//div[@id='nonCrowdCount' and @style='display: none;']");
	public static By crowdCountMessage1 = By.xpath("//div[@id='nonCrowdCount']//span[text()='Optimize Your Post Reach:']");
	public static By crowdCountMessage2 = By.xpath("//div[@id='nonCrowdCount']//span[text()='Make sure the best candidates apply for your task by selecting the right marketplace and audience filters.']");
	public static By crowdCountMessage3 = By.xpath("//div[@id='nonCrowdCount']//span[3]");
	public static By crowdNoImage = By.xpath("//div[@id='imgNonCount' and @style='float: left; padding-left: 7px; display: none;']");
	public static By postReach1 = By.xpath("//div[@id='CrowdCount']/span[text()='Post Reach:']"); 
	public static By postReach2 = By.xpath("//div[@id='CrowdCount']/span[text()='This post will reach ']");
	public static By targetCrowd = By.id("totalCrowdCount");
	public static By postReach3 = By.xpath("//div[@id='CrowdCount']/span[text()=' people in the liquid workforce.']"); 
	public static By postReach4 = By.xpath("//div[@id='CrowdCount']/span[text()='Refine the post reach by clicking the \"Target Crowd\" button in the toolbar.']");
	
	/**Review Modal*/
	public static By confirmPost = By.xpath("//div[@class='review-wrapper']//div[@id='divreviewbtns']/a[@class='btn btn-custom-green' and text()='POST TASK']");
	public static By reviewModal = By.xpath("//div[@class='review-wrapper']");
	public static By reviewReviewTitle = By.xpath("//div[@class='review-wrapper']//p[text()='Review']");
	public static By reviewReviewTask = By.xpath("//div[@class='review-wrapper']//p[text()='Your task,' and text()=', will reach']");
	public static By reviewReviewTaskName = By.xpath("//div[@class='review-wrapper']//p//span");
	public static By reviewPeopleCount = By.xpath("//div[@class='review-wrapper']//p[@class='review-people-count']");
	public static By reviewOptimize = By.xpath("//div[@class='review-wrapper']//p[text()='Your post reach has been optimized based on the following filters:']");
	public static By reviewFilter = By.xpath("//div[@class='review-wrapper']//div[@class='row filter-container']");
	public static By reviewCloseButton = By.xpath("//div[@class='review-wrapper']//div[@class='review-overview-close']");
	public static By modifyPost = By.xpath("//div[@class='review-wrapper']//div[@id='divreviewbtns']/a[@class='btn btn-custom-blue' and text()='MODIFY POST']");
	public static By startOver = By.xpath("//div[@class='review-wrapper']//div[@id='divreviewbtns']/a[@class='btn btn-custom-blue' and text()='START OVER']");
	public static By loading = By.xpath("//div[@class='review-wrapper']//div[@id='divreviewbtns']//i[@class='fa fa-spinner fa-spin']");
	public static By thumbsUp = By.xpath("//img[contains(@src,'ThumbsUp_img.png')]");
	public static By awesome = By.xpath("//div[@id='app-container']//p[text()='Awesome!']");
	public static By liveNow = By.xpath("//div[@id='app-container']//p[text()='Your task is now live on Accenture Liquid Workforce. ']"); 
	public static By whatNow = By.xpath("//div[@id='app-container']//p[text()='What do you want to do now? ']");
	public static By viewMyPost = By.xpath("//div[@id='app-container']//a[text()='VIEW MY POST']");
	public static By home2 = By.xpath("//div[@id='app-container']//a[text()='HOME']");
	public static By postNewtask = By.xpath("//div[@id='app-container']//a[text()='POST A NEW TASK']");
	public static By signOut = By.xpath("//div[@id='app-container']//a[text()='SIGN OUT']");
	public static By reviewAppliedFilterCount(String filterName){return By.xpath("//div[@class='review-wrapper']//div[@class='row filter-container']//p[text()='" + filterName + "']/ancestor::div[@class='filter-icon']/span[@class='filter-count']");}
	public static By reviewAppliedFilter(String filterName){return By.xpath("//div[@class='review-wrapper']//div[@class='row filter-container']/div[@class='filter-icon']/p[text()='" + filterName + "']");}
	public static By reviewAppliedFilterIcon(String filterName, String image){return By.xpath("//div[@class='review-wrapper']//div[@class='row filter-container']//p[text()='" + filterName + "']/ancestor::div[@class='filter-icon']/img[contains(@src,'" + image + ".png')]");}
	public static By restrictMessage = By.xpath("//div[@class='review-wrapper']//label[@class='Restrictlabel checkbox-inline' and text()=' Only allow people in the target reach to apply for this task.']");
	public static By restrictCheckBox = By.xpath("//div[@class='review-wrapper']//label[@class='Restrictlabel checkbox-inline']/input");
	
	
	
	
	/**work location*/
	public static By postATask = By.xpath("//nav//ul//div[text()='POST A TASK ']");
	public static By workLocationDiv = By.xpath("//label[text()='WORK LOCATION']/ancestor::div[@class='col-md-12']");
	public static By virtualLocationText = By.xpath("//div[@class='WorkLocationDiv']//input[@disabled]");
	public static By virtualLocationButton = By.xpath("//div[@class='col-md-12']//label[@class='radio-inline labeltext']//input[@value='0']");
	public static By clientSiteButton = By.xpath("//div[@class='col-md-12']//label[@class='radio-inline labeltext']//input[@value='1']");
	public static By clientSiteText = By.xpath("//div[@class='WorkLocationDiv']//input[@name='txtworkLocationText']");
	public static By accentureLocationButton = By.xpath("//div[@class='col-md-12']//label[@class='radio-inline labeltext']//input[@value='2']");
	public static By accentureLocationfield = By.xpath("//label[text()='WORK LOCATION']/ancestor::div[@class='col-md-12']//div[@id='Locationsection']//div[@class='Select-placeholder']");
	public static By accentureLocationfieldInput= By.xpath("//label[text()='WORK LOCATION']/ancestor::div[@class='col-md-12']//div[@id='Locationsection']//span[@class='Select-multi-value-wrapper']//input");
	public static By accentureLocationComboBox = By.xpath("//label[text()='WORK LOCATION']/ancestor::div[@class='col-md-12']//div[@id='Locationsection']//div[@role='listbox']//div[@role='option'][1]");
	public static By accentureLocationClear = By.xpath("//div[@class='col-md-12']//span[@class='Select-clear']");/**to be updated*/
	public static By clientSitePlaceHolder = By.xpath("//div[@class='WorkLocationDiv']//input[@placeholder='Enter the name of the client site...']");
	public static By accentureLocationPlaceHolder = By.xpath("//label[text()='WORK LOCATION']/ancestor::div[@class='col-md-12']//div[@id='Locationsection']//div[text()='Enter the city name of the Accenture Location...']");
	
	public static By workLocationErrorMessage1 = By.xpath("//div[@class='WorkLocationErrorMessage' and @style='display: block;']//p[text()='Please enter a Client Site Location.']");
	public static By workLocationErrorMessage2 = By.xpath("//div[@class='WorkLocationErrorMessage' and @style='display: block;']//p[text()='Please enter the city name of the Accenture Location.']");
	
	
	
	/**title*/
	public static By jobTitleField = By.xpath("//div[@class='col-md-3 col-size']//input");
	public static By titlePlaceHolder = By.xpath("//div[@class='col-md-3 col-size']//input[@placeholder='Enter a short and descriptive title for your task']");
	public static By jobTitleFieldLabel= By.xpath("//div[@class='col-md-3 col-size']//label[text()='TITLE']");
	
	public static By titleErrorMessage = By.xpath("//div[@class='TitleErrorMessage' and @style='display: block;']//p[text()='Please enter the task Title.']");
	
	
	/**additional details*/
	public static By addDetails = By.xpath("//div[@class='col-md-9']//label[text()='ADDITIONAL DETAILS']");
	public static By wbs = By.xpath("//div[@class='col-md-9']//input[@id='chkwbsprovided']");
	public static By urgent = By.xpath("//div[@class='col-md-9']//input[@id='chkurgenttask']");
	public static By requireApproval = By.xpath("//div[@class='col-md-9']//input[@id='chksupervisorapproval']");
	public static By wbsHide = By.xpath("//div[@class='WBSHide']");
	public static By charge = By.xpath("//label[text()='Chargeable']/input");
	
	/**multioffer*/
	public static By multiOfferDiv = By.xpath("//div[@class='col-md-4']//label[text()='MULTI OFFER?']");
	public static By resourceNumberDiv = By.xpath("//div[@class='col-md-6']//label[text()='NUMBER OF RESOURCES NEEDED']");
	public static By multiOfferYes = By.xpath("//div[@class='col-md-4']//input[@name='rbtnmultiOfferRadio' and @value='true']");
	public static By multiOfferNo = By.xpath("//div[@class='col-md-4']//input[@name='rbtnmultiOfferRadio' and @value='false']");
	public static By readOnlyResourceNumberField = By.xpath("//label[text()='NUMBER OF RESOURCES NEEDED']/ancestor::div[@class='col-md-6']//input[@id='txtresourcerequired' and @readonly]");
	public static By readWriteResourceNumberField = By.xpath("//label[text()='NUMBER OF RESOURCES NEEDED']/ancestor::div[@class='col-md-6']//input[@id='txtresourcerequired']");
	public static By resourceRequiredErrorMessage1 = By.xpath("//div[@class='ResourceRequiredErrorMessage' and contains(@style,'display: block;')]//p[text()='Please provide the resources required for the task.']");
	public static By resourceRequiredErrorMessage2 = By.xpath("//div[@class='ResourceRequiredErrorMessage' and contains(@style,'display: block;')]//p[text()='Please enter numeric value only.']");
	public static By resourceRequiredErrorMessage3 = By.xpath("//div[@class='ResourceRequiredErrorMessage' and contains(@style,'display: block;')]//p[text()='Please enter${nbsp}a value${nbsp}greater than or equal to 2.']");
	public static By resourceRequiredErrorMessage4 = By.xpath("//div[@class='ResourceRequiredErrorMessage' and contains(@style,'display: block;')]//p[text()='Please enter a value less than or equal to 32767.']");
	
	
	
	/**description*/
	public static By description = By.xpath("//label[text()='DESCRIPTION']/ancestor::div[@class='col-md-7']");
	
	public static By body = By.xpath("//body[@id='tinymce']");
	public static By descriptionField = By.xpath("//*[@id='tinymce']/p");
	public static By descriptionErrorMessage = By.xpath("//div[@class='descriptionErrorMessage' and @style='display: block;']//p[text()='Please provide the task description.']");
	public static By descriptionPlaceHolder = By.xpath("//*[@id='tinymce']/p/span[text()='Provide a complete and clear description of the task.']");
	
	/**behalf*/ /**to be updated*/
	public static By behalf = By.xpath("//div[@class='col-md-3']//label[text()='POSTING ON BEHALF OF']");
	public static By behalfFieldDisabled = By.xpath("//label[text()='POSTING ON BEHALF OF']/ancestor::div[@class='col-md-3']//div[@class='Select txt-fnt Select--single is-searchable']");
	public static By behalfFieldEnabled = By.xpath("//label[text()='POSTING ON BEHALF OF']/ancestor::div[@class='col-md-3']//div[@class='Select txt-fnt Select--single is-focused is-open is-searchable']//input");
	public static By behalfComboBox(String eid) {return By.xpath("//label[text()='POSTING ON BEHALF OF']/ancestor::div[@class='col-md-3']//div[@class='Select-menu-outer']//div[text()='"+eid+"']");}
	public static By behalfPlaceHolder = By.xpath("//div[text()='Enter Enterprise ID.']/ancestor::div[@class='col-md-3']//label[text()='POSTING ON BEHALF OF']/ancestor::div[@class='col-md-3']//div[@class='Select txt-fnt Select--single is-searchable']");
	
	/**marketplace*/
	public static By marketplace = By.xpath("//div[@class='col-md-3 col-size']//label[text()='MARKETPLACE']");
	
	public static By marketplaceField = By.id("ddlMarketPlace");
	public static By marketplaceDropdown(String market) {return By.xpath("//select[@id='ddlMarketPlace']//option[text()='"+market+"']");}
	public static By marketplacePlaceholder1 = By.xpath("//select[@id='ddlMarketPlace']//option[text()='Choose the right marketplace']");
	public static By marketplacePopUp = By.id("myModalLabel");
	public static By iAgreePopUp = By.xpath("//div[@class='divTaskCategory_confirm_buttons']//button[text()='I understand']");
	public static By mktErrorMessage = By.xpath("//div[@class='MktErrorMessage' and @style='display: block;']//p[text()='Please select a Marketplace.']");
	
	
	public static By category = By.xpath("//div[@class='col-md-3 col-size-category']//label[text()='CATEGORY']");
	
	
	public static By categoryField = By.id("ddlMarketPlaceTaskCategory");
	public static By categoryDropdown(String category) {return By.xpath("//select[@id='ddlMarketPlaceTaskCategory']//option[text()='"+category+"']");}
	public static By categoryErrorMessage = By.xpath("//div[@class='categoryErrorMessage' and @style='display: block;']//p[text()='Please select a Category.']");
	public static By categoryPlaceholder1 = By.xpath("//select[@id='ddlMarketPlaceTaskCategory']//option[text()='Choose the right category']");
	
	
	/**workHours*/
	public static By info = By.xpath("//label[text()='WORK HOURS START']/ancestor::div[@class='col-md-3']//i");
	public static By workStartHour = By.xpath("//div[@class='col-md-3']//label[text()='WORK HOURS START']");
	public static By workStartHourField = By.xpath("//label[text()='WORK HOURS START']/ancestor::div[@class='col-md-3']//input[@class='form-control workingHours txt-fnt']");
	public static By workStartHourPicker = By.xpath("//label[text()='WORK HOURS START']/ancestor::div[@class='col-md-3']//span[@class='input-group-addon']");
	public static By workStartHourPickerDiv = By.xpath("//label[text()='WORK HOURS START']/ancestor::div[@class='col-md-3']//div[@class='timepicker']");
	public static By timePickerHour = By.xpath("//label[text()='WORK HOURS START']/ancestor::div[@class='col-md-3']//div[@class='timepicker']//span[@class='timepicker-hour']");
	public static By timePickerMinute = By.xpath("//label[text()='WORK HOURS START']/ancestor::div[@class='col-md-3']//div[@class='timepicker']//span[@class='timepicker-minute']");
	public static By selectUp = By.xpath("//label[text()='WORK HOURS START']/ancestor::div[@class='col-md-3']//div[@class='timepicker']//span[@class='glyphicon glyphicon-chevron-up'][1]");
	public static By selectDown = By.xpath("//label[text()='WORK HOURS START']/ancestor::div[@class='col-md-3']//div[@class='timepicker']//span[@class='glyphicon glyphicon-chevron-down'][2]");
	public static By timePickerAMPM = By.xpath("//label[text()='WORK HOURS START']/ancestor::div[@class='col-md-3']//div[@class='timepicker']//button[@class='btn btn-primary']");
	public static By startHoursErrorMessage = By.xpath("//*[@class='startHoursErrorMessage' and @style='display: inline;']//p[text()='Please provide a valid time format.']");
	public static By startHourPlaceHolder = By.xpath("//label[text()='WORK HOURS START']/ancestor::div[@class='col-md-3']//input[@placeholder='Work day start time']");
	
	
	public static By workEndHourFieldEnabled = By.xpath("//label[text()='WORK HOURS END']/ancestor::div[@class='col-md-3']//div[@style='display: none;']");
	public static By workEndHour = By.xpath("//div[@class='col-md-3']//label[text()='WORK HOURS END']");
	public static By workEndHourField = By.xpath("//label[text()='WORK HOURS END']/ancestor::div[@class='col-md-3']//input[@class='form-control workHoursEnd workingHours txt-fnt']");
	public static By workEndHourPicker = By.xpath("//label[text()='WORK HOURS END']/ancestor::div[@class='col-md-3']//span[@class='input-group-addon']");
	public static By workEndHourPickerDiv = By.xpath("//label[text()='WORK HOURS END']/ancestor::div[@class='col-md-3']//div[@class='timepicker']");
	public static By timePickerHourEnd = By.xpath("//label[text()='WORK HOURS END']/ancestor::div[@class='col-md-3']//div[@class='timepicker']//span[@class='timepicker-hour']");
	public static By timePickerMinuteEnd = By.xpath("//label[text()='WORK HOURS END']/ancestor::div[@class='col-md-3']//div[@class='timepicker']//span[@class='timepicker-minute']");
	public static By selectUpEnd = By.xpath("//label[text()='WORK HOURS END']/ancestor::div[@class='col-md-3']//div[@class='timepicker']//span[@class='glyphicon glyphicon-chevron-up'][1]");
	public static By selectDownEnd = By.xpath("//label[text()='WORK HOURS END']/ancestor::div[@class='col-md-3']//div[@class='timepicker']//span[@class='glyphicon glyphicon-chevron-down'][2]");
	public static By timePickerAMPMEnd = By.xpath("//label[text()='WORK HOURS END']/ancestor::div[@class='col-md-3']//div[@class='timepicker']//button[@class='btn btn-primary']");
	public static By endHoursErrorMessage = By.xpath("//*[@class='endHoursErrorMessage' and @style='display: inline;']//p[text()='Please provide a valid time format.']");
	public static By endHourPlaceHolder = By.xpath("//label[text()='WORK HOURS END']/ancestor::div[@class='col-md-3']//input[@placeholder='Work day end time']");
	
	public static By timeZone = By.xpath("//div[@class='col-md-3']//label[text()='WORK HOURS TIMEZONE']");
	public static By timeZoneField = By.xpath("//option[text()='Work day timezone']/ancestor::select[@id='ddlworktimezone']");
	public static By timeZoneFieldOption(String timeZone){return By.xpath("//select[@id='ddlworktimezone']//option[text()='"+timeZone+"']");}
	public static By timeZoneplaceholder1 = By.xpath("//select[@id='ddlworktimezone']//option[text()='Work day timezone']");
	
	
	/**effort*/
	public static By effort = By.xpath("//div[@class='col-md-3']//label[text()='ESTIMATED EFFORT']");
	public static By effortField = By.xpath("//label[text()='ESTIMATED EFFORT']/ancestor::div[@class='col-md-3']//input[@name='unitOfEfforField']");
	public static By effortDropDown(String unit) {return By.xpath("//label[text()='ESTIMATED EFFORT']/ancestor::div[@class='col-md-3']//select[@id='ddlunitoffeffort']//option[text()='"+unit+"']");}
	public static By effortUnitSpan = By.xpath("//label[text()='ESTIMATED EFFORT']/ancestor::div[@class='col-md-3']//span[@class='input-group-btn']");
	public static By unitOfEffortErrorMessage = By.xpath("//div[@class='unitOfEffortErrorMessage' and @style='display: block;']//p[text()='Please provide an Estimated Effort of the task.']");
	public static By effortFieldDisabled = By.xpath("//label[text()='ESTIMATED EFFORT']/ancestor::div[@class='col-md-3']//input[@name='unitOfEfforField' and @disabled]");
	public static By effortPlaceHolder = By.xpath("//label[text()='ESTIMATED EFFORT']/ancestor::div[@class='col-md-3']//input[@placeholder='How long will it take?']");
	
	
	/**startday*/
	public static By workStartDay = By.xpath("//label[text()='START DATE']/ancestor::div[@class='col-md-3']");
	
	public static By startDateErrorMessage = By.xpath("//div[@class='startDateErrorMessage' and @style='display: block;']//p[text()='Please provide the Start Date of the task.']");
	public static By startDateField = By.xpath("//label[text()='START DATE']/ancestor::div[@class='col-md-3']//input[@name='startDate']");
	public static By calendarOpened = By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtDays']");
	public static By timeButton = By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//td[@class='rdtTimeToggle']");
	public static By timePickerOpened = By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtTime']");
	public static By timePickerMinuteStart = By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtTime']//div[@class='rdtCount'][2]");
	public static By timePickerHourStart = By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtTime']//div[@class='rdtCount'][1]");
	public static By timePickerAMPMStart = By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtTime']//div[@class='rdtCount'][3]");
	public static By timePickerMinuteSelectUp = By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtTime']//span[@class='rdtBtn'][3]");
	public static By timePickerHourSelectUp = By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtTime']//span[@class='rdtBtn'][1]");
	public static By timePickerAMPMSelectDown = By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtTime']//span[@class='rdtBtn'][6]");
	public static By calendarPickerButton = By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtTime']//th[@class='rdtSwitch']");
	public static By monthPickerButton = By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtDays']//th[@class='rdtSwitch']");
	public static By monthPicker = By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtMonths']");
	public static By yearPickerButton = By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtMonths']//th[@class='rdtSwitch']");
	public static By yearPicker = By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtYears']");
	public static By yearPickerOption(String startYear){return By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtYears']//td[text()='"+startYear+"']");}
	public static By monthPickerOption(String startMonth){return By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtMonths']//td[text()='"+startMonth+"']");}
	public static By dayPickerOption(String startday){return By.xpath("//div[@class='rdt react-date-picker startDate rdtOpen']//div[@class='rdtDays']//td[@class='rdtDay' and text()='"+startday+"']");}
	public static By workStartLabel = By.xpath("//label[text()='START DATE']");
	public static By startDatePlaceHolder = By.xpath("//label[text()='START DATE']/ancestor::div[@class='col-md-3']//input[@placeholder='When will your task start?']");
	
	public static By workStartDayStar = By.xpath("//label[text()='START DATE']/span[@class='glyphicon glyphicon-star starv']");
	public static By workEndDayStar = By.xpath("//label[text()='END DATE']/span[@class='glyphicon glyphicon-star starv']");
	public static By workExpireDayStar = By.xpath("//label[text()='EXPIRY DATE']/span[@class='glyphicon glyphicon-star starv']");
	public static By jobTitleFieldLabelStar= By.xpath("//div[@class='col-md-3 col-size']//label[text()='TITLE']/span[@class='glyphicon glyphicon-star starv']");
	public static By descriptionStar = By.xpath("//label[text()='DESCRIPTION']/span[@class='glyphicon glyphicon-star starv']");
	public static By marketplaceStar = By.xpath("//div[@class='col-md-3 col-size']//label[text()='MARKETPLACE']/span[@class='glyphicon glyphicon-star starv']");
	public static By categoryStar = By.xpath("//div[@class='col-md-3 col-size-category']//label[text()='CATEGORY']/span[@class='glyphicon glyphicon-star starv']");
	public static By effortStar = By.xpath("//div[@class='col-md-3']//label[text()='ESTIMATED EFFORT']/span[@class='glyphicon glyphicon-star starv']");
	public static By highlightedFieldsNote = By.xpath("//span[@class='legend']/span[@class='glyphicon glyphicon-star starv leg']");
	
	/**end date*/
	public static By workEndDay = By.xpath("//label[text()='END DATE']/ancestor::div[@class='col-md-3']");
	
	public static By endDateErrorMessage = By.xpath("//div[@class='endDateErrorMessage' and @style='display: block;']//p[text()='Please provide the End Date of the task.']");
	public static By endDateField = By.xpath("//label[text()='END DATE']/ancestor::div[@class='col-md-3']//input[@name='endDate']");
	public static By calendarOpenedEnd = By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtDays']");
	public static By timeButtonEnd = By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//td[@class='rdtTimeToggle']");
	public static By timePickerOpenedEnd = By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtTime']");
	public static By timePickerMinuteEndDate = By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtTime']//div[@class='rdtCount'][2]");
	public static By timePickerHourEndDate = By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtTime']//div[@class='rdtCount'][1]");
	public static By timePickerAMPMEndDate = By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtTime']//div[@class='rdtCount'][3]");
	public static By timePickerMinuteSelectUpEnd = By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtTime']//span[@class='rdtBtn'][3]");
	public static By timePickerHourSelectUpEnd = By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtTime']//span[@class='rdtBtn'][1]");
	public static By timePickerAMPMSelectDownEnd = By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtTime']//span[@class='rdtBtn'][6]");
	public static By calendarPickerButtonEnd = By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtTime']//th[@class='rdtSwitch']");
	public static By monthPickerButtonEnd = By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtDays']//th[@class='rdtSwitch']");
	public static By monthPickerEnd = By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtMonths']");
	public static By yearPickerButtonEnd = By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtMonths']//th[@class='rdtSwitch']");
	public static By yearPickerEnd = By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtYears']");
	public static By yearPickerOptionEnd(String Year){return By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtYears']//td[text()='"+Year+"']");}
	public static By monthPickerOptionEnd(String Month){return By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtMonths']//td[text()='"+Month+"']");}
	public static By dayPickerOptionEnd(String day){return By.xpath("//div[@class='rdt react-date-picker endDate rdtOpen']//div[@class='rdtDays']//td[@class='rdtDay' and text()='"+day+"']");}
	public static By workEndLabel = By.xpath("//label[text()='END DATE']");
	public static By endDatePlaceHolder = By.xpath("//label[text()='END DATE']/ancestor::div[@class='col-md-3']//input[@placeholder='When will your task due?']");
	
	/**expire date*/
	public static By workExpireDay = By.xpath("//label[text()='EXPIRY DATE']/ancestor::div[@class='col-md-3']");
	public static By expirationDateErrorMessage = By.xpath("//div[@class='expirationDateErrorMessage' and @style='display: block;']//p[text()='Please provide the Expiry Date of the task.']");
	public static By expireDateField = By.xpath("//label[text()='EXPIRY DATE']/ancestor::div[@class='col-md-3']//input[@name='expirationDate']");
	public static By calendarOpenedExpire = By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtDays']");
	public static By timeButtonExpire = By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//td[@class='rdtTimeToggle']");
	public static By timePickerOpenedExpire = By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtTime']");
	public static By timePickerMinuteExpire = By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtTime']//div[@class='rdtCount'][2]");
	public static By timePickerHourExpire = By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtTime']//div[@class='rdtCount'][1]");
	public static By timePickerAMPMExpire = By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtTime']//div[@class='rdtCount'][3]");
	public static By timePickerMinuteSelectUpExpire = By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtTime']//span[@class='rdtBtn'][3]");
	public static By timePickerHourSelectUpExpire = By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtTime']//span[@class='rdtBtn'][1]");
	public static By timePickerAMPMSelectDownExpire = By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtTime']//span[@class='rdtBtn'][6]");
	public static By calendarPickerButtonExpire = By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtTime']//th[@class='rdtSwitch']");
	public static By monthPickerButtonExpire = By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtDays']//th[@class='rdtSwitch']");
	public static By monthPickerExpire = By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtMonths']");
	public static By yearPickerButtonExpire = By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtMonths']//th[@class='rdtSwitch']");
	public static By yearPickerExpire = By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtYears']");
	public static By yearPickerOptionExpire(String Year){return By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtYears']//td[text()='"+Year+"']");}
	public static By monthPickerOptionExpire(String Month){return By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtMonths']//td[text()='"+Month+"']");}
	public static By dayPickerOptionExpire(String day){return By.xpath("//div[@class='rdt react-date-picker expirationDate rdtOpen']//div[@class='rdtDays']//td[@class='rdtDay' and text()='"+day+"']");}
	public static By workExpireLabel = By.xpath("//label[text()='EXPIRY DATE']");
	public static By expireDatePlaceHolder = By.xpath("//label[text()='EXPIRY DATE']/ancestor::div[@class='col-md-3']//input[@placeholder='When will your task expire?']");
	
}

