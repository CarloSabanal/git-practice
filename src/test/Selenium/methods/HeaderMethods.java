package methods;


import org.openqa.selenium.WebDriver;


//import driver.EnvironmentVariables;
import driver.GenericMethods;
import methods.VerifyLinks;

import pageObjects.HeaderObjects;



public class HeaderMethods {
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();
	VerifyLinks vl = new VerifyLinks();
	

//--------------------------------------------------------------------------------------------------------------------------------
	
	public void verifyHeader(WebDriver driver) throws InterruptedException	
	{
		
		Thread.sleep(10000);
		/**6988.1  Validate that page title is : Liquid Workforce  on  Default Page Header after login */
		if(gm.elementExist(driver, HeaderObjects.header)){
			System.out.println("==============================yehey==============================Header");
			gm.logScreenshot("Pass", "Header exists!", driver);
		} else {
			gm.logScreenshot("Fail", "No Header!!!", driver);
		}
		
	}
	
	
	public void verifyHeaderContents(WebDriver driver, String value1, String value2, String value3, String value4) throws InterruptedException	
	{
		boolean click = false;
//--------------------------------------------------------------------------------------------------------------------------------	
		/**6988.1  Validate that page title is : Liquid Workforce  on  Default Page Header after login */
		if(gm.elementExist(driver, HeaderObjects.liquidWorkforceLogo)){
			System.out.println("==============================yehey==============================Logo");
			gm.logScreenshot("Pass", "Logo exists!", driver);
		} else {
			gm.logScreenshot("Fail", "No logo!!!", driver);
		}
//--------------------------------------------------------------------------------------------------------------------------------		
		/**6988.2
		 * 
		 * Validate that below Navigations are available in default page header
			Main Navigation:
               HOME
               MARKETPLACES
               TASKS
               APPLICATIONS
               POSTS
		 * 
		 */
		if(gm.elementExist(driver, HeaderObjects.headerLinks)){
			System.out.println("==============================yehey==============================headerLinks");
			String[] links = value1.split(",");
			String j = "";//not used for header, only for footer. just pass this.
			
			for (int i = 0; i < links.length; i++) {
				vl.checkLinks(driver, links[i], j, click, "head", value1, value2, value3, value4);						
		      }
			gm.logScreenshot("info", "Kindly check if there are failed tests!", driver);
			
		} else {
			gm.logScreenshot("Fail", "Header links missing!!!", driver);
		}
		
	}
//--------------------------------------------------------------------------------------------------------------------------------	
	public void verifyUserName(WebDriver driver, String userName){
		/**6988.4 Validate that user name displayed at upper right corner of page header */
				if(gm.elementExist(driver, HeaderObjects.userNameTopRight)){
					System.out.println("==============================yehey==============================username");
					
					/**INSERT HERE CODE THAT WILL GET USERNAME FROM DATAPOOL AS USED FOR LOGING IN*/
					if(driver.findElement(HeaderObjects.userNameTopRight).getText().contains(userName)){
						gm.logScreenshot("Pass", "Username is displayed correctly", driver);
					}else {
							gm.logScreenshot("Fail", "Username is incorrect", driver);
						}
					
				} else {
					gm.logScreenshot("Fail", "No Username DIV", driver);
				}
	}
//--------------------------------------------------------------------------------------------------------------------------------	
	/**6988.10 to 14 Validate when clicked on each links, user will be redirected to corresponding pages */
	public void checkHeaderOfAllPages(WebDriver driver, String value1, String value2, String value3, String value4) throws InterruptedException	
	{
		String[] links = value1.split(",");
		String j = "";//just so to be compatible with the method checklinks
		boolean click = true;
		
		for (int i = 0; i < links.length; i++) {
			
			vl.checkLinks(driver, links[i], j, click, "head", value1, value2, value3, value4);
			
	      }
		
	}
	

	
}