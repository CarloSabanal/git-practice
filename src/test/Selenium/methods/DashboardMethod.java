package methods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;


import driver.GenericMethods;
import pageObjects.DashboardObjects;

public class DashboardMethod {
	GenericMethods gm = new GenericMethods();

	/**
	 * This methods is for UserStory: 6978 - Add Marketplace to Personalized Dashboard
	 * @author arnold.r.osorio
	 * @param driver
	 * @param menu
	 */

	public void navigateTo(WebDriver driver,String menu)
	{
		if(menu.equalsIgnoreCase("home")){
			//Click home and verify if successfully navigated to Dashboard page.
			driver.findElement(DashboardObjects.menuHome).click();
			String url = driver.getCurrentUrl();
			if(url.equals("https://liquidworkforcev2-test.accenture.com/Dashboard")){
				gm.logScreenshot("Pass", "Successfully navigated to Dashboard page", driver);
				System.out.println("Successfully navigated to Dashboard page");
			}
			else{
				gm.logScreenshot("Fail", "Unsuccessfully navigated to Dashboard page", driver);
			}
		}
	}

	public void waitforCarousel(WebDriver driver) throws InterruptedException{
		try{
			driver.findElement(DashboardObjects.mpCarousel).isDisplayed();
		}
		catch(Exception e){
			Thread.sleep(5000);
			waitforCarousel(driver);
		}
	}

	public void verifyMarketPlace(WebDriver driver) throws InterruptedException{
		//Thread.sleep(120000);
		//		while(driver.findElement(DashboardObjects.mpCarouselLoading).isDisplayed()){
		//			Thread.sleep(5000);
		//		}
		//User Story 7784: Verify that the Marketplaces tiles in carousel on the "Dashboard" Page is displayed.
		if(driver.findElement(DashboardObjects.mpCarousel).isDisplayed()){
			gm.logScreenshot("Pass", "Marketplace tiles in carousel is displayed in the dashboard page.", driver);
			System.out.println("Marketplace tiles in carousel is displayed in the dashboard page.");
		}
		else{
			gm.logScreenshot("Fail", "Marketplace tiles in carousel is NOT displayed in the dashboard page.", driver);
		}

		//verify that there are only 9 recommended marketplaces
		int obj = driver.findElements(DashboardObjects.carousel).size();
		if(obj==9){
			gm.logScreenshot("Pass", "There are 9 recommended marketplaces.", driver);
		}
		else{
			gm.logScreenshot("Fail", "There are "+obj+" recommended marketplaces.", driver);
		}

		//verify that initially only 3 marketplaces are displayed in the carousel
		int flag=0;
		for(int i=1;i<=obj;i++){
			if(driver.findElement(DashboardObjects.marketPlace(i)).isDisplayed()){
				flag++;							
				try{
					String mpName = driver.findElement(DashboardObjects.marketPlaceName(i)).getText();
					String mpOptions = driver.findElement(DashboardObjects.marketPlaceOptions(i)).getText();
					String mpMembers = driver.findElement(DashboardObjects.marketPlaceMembers(i)).getText();

					//Sprint2[7781]: Verify that the new Recommended Marketplace has  0 Member displayed in the carousel.
					if(mpOptions.equalsIgnoreCase("RECOMMENDED FOR YOU")){
						if(mpMembers.equalsIgnoreCase("0 Members |")){
							gm.setLogMsg("Pass", "The new Recommended Marketplace has  0 Member displayed in the carousel");
						}
						else{
							gm.setLogMsg("Fail", "The new Recommended Marketplace '"+mpName+"' has  "+mpMembers+" displayed in the carousel");
						}
					}


					String mpOpenTask = driver.findElement(DashboardObjects.marketPlaceOpenTask(i)).getText();
					String mpLogo = driver.findElement(DashboardObjects.marketPlaceLogo(i)).getAttribute("src");

					gm.setLogMsg("Pass", "MP Name:"+mpName+
							"\nMP Members:"+mpMembers+
							"\nMP OpenTask:"+mpOpenTask+
							"\nMP Logo:"+mpLogo);
				}

				catch(Exception e){

					try{
						String marketingTileFK = driver.findElement(DashboardObjects.marketingTileFK(i)).getText();
						String marketingTileSK = driver.findElement(DashboardObjects.marketingTileSK(i)).getText();
						String mpLogo = driver.findElement(DashboardObjects.marketPlaceLogo(i)).getAttribute("src");

						gm.setLogMsg("Pass", "Marketing Tile FK:"+marketingTileFK+
								"\nMarketing Tile FK"+marketingTileSK+
								"\nMP Logo:"+mpLogo);

						//User Story 7784: Verify that the 2nd tile of the first carousel slide should be a Marketing tile.
						if(i==2){
							gm.setLogMsg("Pass", "The 2nd tile of the first carousel slide is a Marketing tile");
						}
						else{
							gm.setLogMsg("Fail", "The 2nd tile of the first carousel slide is NOT a Marketing tile");
						}
					}
					catch(Exception ex){
						gm.setLogMsg("Fail", "Error in getting marketplace details, please check manually.");
					}
				}
			}
		}

		if(flag==3){
			gm.logScreenshot("Pass", "Only 3 marketplace are displayed.", driver);
		}
		else{
			gm.logScreenshot("Fail", "There are "+flag+" marketplace displayed.", driver);
		}

		//verify arrow will appear when mouse hovers on the 3rd tile
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(DashboardObjects.marketPlace(2))).build().perform();
		Thread.sleep(5000);

		if(gm.elementExist(driver, DashboardObjects.btnNext)){
			gm.logScreenshot("Pass", "Arrow appeared when mouse hovers on the carousel.", driver);
		}
		else{
			gm.logScreenshot("Fail", "Arrow did not appear when mouse hovers on the carousel.", driver);
		}

		//Verify that the next page with another 3 recommended marketplace will be shown by clicking the arrow on the 3rd tile of recommended marketplaces.
		//hover to carousel
		action.moveToElement(driver.findElement(DashboardObjects.marketPlace(2))).build().perform();
		Thread.sleep(5000);
		//click next button
		driver.findElement(DashboardObjects.btnNext).click();
		Thread.sleep(3000);

		//reset the flag
		flag = 0;
		for(int i=4;i<=obj;i++){
			if(driver.findElement(DashboardObjects.marketPlace(i)).isDisplayed()){
				flag++;
			}
		}

		if(flag==3){
			gm.logScreenshot("Pass", "Another 3 marketplaces are displayed when arrow button is clicked.", driver);
		}
		else{
			gm.logScreenshot("Fail", "There are "+flag+" marketplace displayed.", driver);
		}

		/**Verify that ALW user should be able to click the carousel "Arrow"(left and right arrow) to see the Available Marketplaces 
		 tiles in carousel on the "Dashboard Page".*/
		//hover to carousel
		action.moveToElement(driver.findElement(DashboardObjects.marketPlace(5))).build().perform();
		Thread.sleep(5000);
		//click next button
		driver.findElement(DashboardObjects.btnPrev).click();
		Thread.sleep(3000);

		/**Verify that All Marketplaces tiles in the carousel are clickable and when clicked it will redirect to specific 
		 Marketplace Landing Page(See Diagram1).*/
		String mptoClik = driver.findElement(DashboardObjects.marketPlaceName(1)).getText().trim();
		driver.findElement(DashboardObjects.marketPlace(1)).click();
		
		if(driver.findElement(DashboardObjects.marketplaceList).isDisplayed()){
			String mpDisplayed = driver.findElement(DashboardObjects.marketplaceList).getText().trim();
			if(mptoClik.equalsIgnoreCase(mpDisplayed)){
				gm.logScreenshot("Pass", "Marketplace tile is clickable and navigated to correct marketplace page."+driver.getCurrentUrl(), driver);
			}
			else{
				gm.logScreenshot("Fail", "Marketplace tile is Not clickable or do not navigated to correct marketplace page."+driver.getCurrentUrl(), driver);
			}
		}

		/**Verify when clicking "Home" Tab on specific Marketplace Landing Page the Marketplaces tiles in the carousel should be 
		 displayed.*/
		navigateTo(driver, "Home");


	}
}
