/**
 * 
 */
package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import methods.ALW_Methods;
import methods.DashboardMethod;
import pageObjects.MarketplaceObjects;
/**This script is for testing UserStory: 13995 - ALW User is able to navigate to MyMarketPlace through 'Marketplace' link
 * @author somnath.bhunia
 * @since 2/14/2017
 */
public class Marketplaces
{
	WebDriver driver = null;
	GenericMethods gm = new GenericMethods();
	DashboardMethod dm = new DashboardMethod();
	ALW_Methods am = new ALW_Methods();
	
	@BeforeTest()
	public void loadURL() throws InterruptedException	
	{
		System.setProperty(EnvironmentVariables.driverType, EnvironmentVariables.driverPath);
		driver = new ChromeDriver();
		//driver = new InternetExplorerDriver();
		driver.manage().window().maximize();
		driver.navigate().to(EnvironmentVariables.URL);
		gm.setLogMsg("Info", "Successfully launched Chrome");
		
	}
	
	@Test(priority = 0, description ="MarketPlaces navigation")
	public void marketplaces() throws InterruptedException
	{
		Thread.sleep(10000);
		am.login(driver, EnvironmentVariables.USERNAME, EnvironmentVariables.PASSWORD);
		gm.setLogMsg("Info", "Successful Login");
		Thread.sleep(10000);
		dm.navigateTo(driver, "Market");
		Thread.sleep(5000);
		//dm.verifyMarketPlace(driver);
		if(driver.getCurrentUrl().contains("Marketplaces"))
		{
			gm.setLogMsg("Pass", "Successfully navigated to Marketplaces section");
			gm.logScreenshot("Pass", "Able to navigate to MyMarketplaces", driver);
		}
		else
		{
			
			gm.setLogMsg("Fail", "Navigation to Marketplaces section was unsuccessful");
			gm.logScreenshot("Fail", "Unable to navigate to MyMarketplaces", driver);
		}
		
	}
	
	@Test(priority = 1, description = "MyMarketplaces tab clicked")/**User story -13995*/
	public void myMarketTab() throws InterruptedException
	{
		driver.findElement(MarketplaceObjects.myMarketplaces).click(); 
		Thread.sleep(25000);
		String bottomBorderColor = driver.findElement(MarketplaceObjects.myMarketplaces).getCssValue("border-bottom-color");
		System.out.println(bottomBorderColor);
		String textColor = driver.findElement(MarketplaceObjects.myMarketplaces).getCssValue("color");
		System.out.println(textColor);
		if((bottomBorderColor.equals("rgba(66, 180, 238, 1)"))&&(textColor.equals("rgba(66, 180, 238, 1)")))
		{
			gm.setLogMsg("Pass", "MyMarketplaces tab is clicked and highlighted");
			gm.logScreenshot("Pass", "Tab is highlighted", driver);
		}
		else
		{
			
			gm.setLogMsg("Fail", "MyMarketplaces tab is not clicked/highlighted");
			gm.logScreenshot("Fail", "Tab is not highlighted", driver);
		}
		//driver.findElement(By.xpath("//*[@id='app-container']//div[@rel = 'Crowd Citizenship - China']")).click();
		String member = driver.findElement(MarketplaceObjects.myMarketplacesMemberText(3)).getText();
		System.out.println(member);
		if(member.equals("MEMBER"))
		{
			gm.setLogMsg("Pass", "'MEMBER' is displayed within the tile");
			gm.logScreenshot("Pass", "Expected text 'MEMBER' is displayed", driver);
		}
		else
		{
			gm.setLogMsg("Fail", "'MEMBER' is not displayed within the tile");
			gm.logScreenshot("Fail", "Expected text 'MEMBER' is not displayed", driver);
		}
		String names = driver.findElement(MarketplaceObjects.myMarketplacesName(3)).getText();
		System.out.println(names);
		String memberCount = driver.findElement(MarketplaceObjects.myMarketplacesMemberCount(3)).getText();
		System.out.println(memberCount);
		System.out.println(gm.getNoFromString(memberCount));
		String openTasks = driver.findElement(MarketplaceObjects.myMarketplacesTasks(3)).getText();
		System.out.println(openTasks);
		System.out.println(gm.getNoFromString(openTasks));
		
	}
	
	@Test(priority = 2, description = "Recommended tab clicked")/** User Story - 13996*/
	public void recommendedMarketTab() throws InterruptedException
	{
		driver.findElement(MarketplaceObjects.recommended).click(); 
		Thread.sleep(25000);
		String bottomBorderColor = driver.findElement(MarketplaceObjects.recommended).getCssValue("border-bottom-color");
		System.out.println(bottomBorderColor);
		String textColor = driver.findElement(MarketplaceObjects.recommended).getCssValue("color");
		System.out.println(textColor);
		if((bottomBorderColor.equals("rgba(66, 180, 238, 1)"))&&(textColor.equals("rgba(66, 180, 238, 1)")))
		{
			gm.setLogMsg("Pass", "Recommended tab is clicked and highlighted");
			gm.logScreenshot("Pass", "Tab is highlighted", driver);
		}
		else
		{
			gm.setLogMsg("Fail", "Recommended tab is not clicked/highlighted");
			gm.logScreenshot("Fail", "Tab is not highlighted", driver);
			
		}
		String recommended = driver.findElement(MarketplaceObjects.myMarketplacesMemberText(2)).getText();
		System.out.println(recommended);
		if(recommended.equals("RECOMMENDED FOR YOU"))
		{
			gm.setLogMsg("Pass", "'RECOMMENDED FOR YOU' is displayed within the tile");
			gm.logScreenshot("Pass", "Expected text 'RECOMMENDED FOR YOU' is  displayed", driver);
		}
		else
		{
			gm.setLogMsg("Fail", "'RECOMMENDED FOR YOU' is not displayed within the tile");
			gm.logScreenshot("Fail", "Expected text 'RECOMMENDED FOR YOU' is not displayed", driver);
		}		
		String names = driver.findElement(MarketplaceObjects.RecommendedName(2)).getText();
		System.out.println(names);
		String memberCount = driver.findElement(MarketplaceObjects.RecommendedMemberCount(2)).getText();
		System.out.println(memberCount);
		System.out.println(gm.getNoFromString(memberCount));
		String openTasks = driver.findElement(MarketplaceObjects.RecommendedTasks(2)).getText();
		System.out.println(openTasks);
		System.out.println(gm.getNoFromString(openTasks));
		
	}

}
