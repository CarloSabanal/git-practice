/**
 * 
 */
package pageObjects;

import org.openqa.selenium.By;

/**
 * @author somnath.bhunia
 * @since 2/20/2017
 * 
 */
public class SearchPageObjects
{
	public static By searchResultsText = By.xpath("//*[@id='app-container']//div/div[1]/ul/li[@class='txt_dash_header']");//Text label displaying - "Search Results"
	public static By searchFilterButton = By.xpath("//*[@id='app-container']//div/div[1]/button[@class='advfilterbtn btn btn-default']");//Filter button to hide/show filter section.
	
	//Dropdown selectors for filter criteria - 
	public static By searchMarketplace = By.id("ddlMarketplace");
	public static By searchMarketplaceButton = By.xpath("//*[@id='divfilterleft']/div[1]/div[1]/span/div/button[@class='multiselect dropdown-toggle btn btn-default']");//required for clicking the button inside the Marketplace filter box.
	public static By searchCategory = By.id("ddlcategory");
	public static By searchCategoryButton = By.xpath("//*[@id='divfilterleft']/div[1]/div[2]/span/div/button[@class='multiselect dropdown-toggle btn btn-default']");//required for clicking the button inside the Category filter box.
	public static By searchStartDate = By.id("ddstartOn");
	public static By searchEffort = By.id("ddestimatedeffort");
	
	//Urgent, New and Requires Approval checkboxes - 
	public static By searchUrgent = By.name("chkurgent");
	public static By searchNew = By.name("NewTaskcheckbox");
	public static By searchApproval = By.name("ApprovalRequiredcheckbox");
	
	//WBS Provided Checkboxes - 
	public static By searchWBS= By.name("chkwbsprovided");
	public static By searchWBSChargeable = By.id("Chargeablecheckbox");
	public static By searchWBSNonChargeable = By.id("NonChargeablecheckbox");
	
	//Single or Multi offer checkboxes - 
	public static By searchSingleOffer = By.name("rbtsingleOffercheckbox");
	public static By searchMultiOffer = By.name("rbtnmultiOffercheckbox");
	
	//Office location radio buttons - 
	public static By searchVirtual = By.xpath("//*[@id='divfilterright']/div/div[4]/label[1]/input[@value='0']");
	public static By searchClientSite = By.xpath("//*[@id='divfilterright']/div/div[4]/label[2]/input[@value='1']");
	public static By searchOnsite = By.xpath("//*[@id='divfilterright']/div/div[4]/label[3]/input[@value='2']");
	
	public static By applyButton = By.id("btnApplyFilters");
	
	
}
