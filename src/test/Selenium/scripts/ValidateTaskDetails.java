package scripts;

import javax.swing.JOptionPane;

/**5 minutes execution time*/

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.openqa.selenium.JavascriptExecutor;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import methods.ALW_Methods;
import methods.FooterMethods;
import methods.PostSectionMethods;
import methods.PostTaskMethods;
import methods.ValidateTaskDetailsMethods;
import pageObjects.FilterObjects;
import pageObjects.PostTaskObjects;


public class ValidateTaskDetails {
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();
	PostTaskMethods ptm = new PostTaskMethods();
	ALW_Methods am = new ALW_Methods();
	ValidateTaskDetailsMethods tdm = new ValidateTaskDetailsMethods();
	
	@BeforeClass()
	public void openBrowser() throws InterruptedException	
	{
		System.setProperty(EnvironmentVariables.driverType, EnvironmentVariables.driverPath);
		driver = new ChromeDriver();//InternetExplorerDriver();ChromeDriver
		gm.setLogMsg("Info", "Successfully launched the browser");
		driver.manage().window().maximize();
		driver.navigate().to(EnvironmentVariables.TaskURL + EnvironmentVariables.TaskID);//task ID, can be variable passed by postatask script, or another column in datapool where task id is dynamically saved.
		gm.setLogMsg("info", "Starting Validation of Task Details Page Test");
	}

	/**Sprint 7
	 * User Story 11082: User can access the Task Details read mode screen and see the details section.
	 * @author jan.carlo.l.sabanal
	 * @since 1/27/2017
	 * 
	 * 
	*/
	
	@Test(priority = 0, description ="ALW",dataProvider="TestData")
	@Parameters("sheetName")
	public void testPostsTask(String flag, String validity, String comment1, String comment2, String userName, String passWord, 
			String workLocationType, String workLocation, String title, String addDetails, String charge, 
			String multiOffer, String resources, String description, String eid, String market, String category, String startHours, 
			String startMinutes, String startAMPM, String mode, String endHours, String endMinutes, String endAMPM, String timeZone, 
			String effortUnit, String effort, String startDateDay, String startDateMonth, String startDateYear, String endDateDay, 
			String endDateMonth, String endDateYear, String expireDateHour, String expireDateMinute, String expireDateAMPM, 
			String expireDateDay, String expireDateMonth, String expireDateYear, String isFilterRequired, String requiredFilters, 
			String language, String languageAny, String resourceLoc, String careerLevel, String talentSegment, String orgUnit, String specialty, String skills, 
			String marketAttributes,String restrictApplicants,String validateTaskDetails, String comment, String applicant, String applyNotes, String withdrawApp, 
			String withdrawTask, String withdrawReason) throws InterruptedException	
	{
		
		if(flag.equalsIgnoreCase("y")){
			if(!passWord.equalsIgnoreCase("null")){//first iteration is for login only.
				am.login(driver, userName, passWord);
			} else {
				
				if(validity.contains("valid")){//task read mode possible only on suucessfully saved tasks

					if(driver.getCurrentUrl().equals(EnvironmentVariables.TaskURL + EnvironmentVariables.TaskID)){
						gm.logScreenshot("Pass", "Navigated to the Task " + EnvironmentVariables.TaskID + " Details Read Mode!", driver);
						
						Thread.sleep(5000);
						tdm.methodsInvoker(driver, userName, workLocationType, workLocation, title, addDetails, 
								charge, multiOffer, resources, description, eid, market, category, startHours, startMinutes, startAMPM, mode, endHours, 
								endMinutes, endAMPM, timeZone, effortUnit, effort, startDateDay, startDateMonth, startDateYear, endDateDay, endDateMonth, 
								endDateYear, expireDateHour, expireDateMinute, expireDateAMPM, expireDateDay, expireDateMonth, expireDateYear, 
								isFilterRequired, requiredFilters, language, languageAny, resourceLoc, careerLevel, talentSegment, orgUnit, specialty, 
								skills, marketAttributes,restrictApplicants, comment, "no");/**no because he is the applicant US13326*/
						
					} else {
						gm.logScreenshot("Fail", "wrong page!!", driver);
					}
					gm.logScreenshot("info", "screen shot before save or go to home!!", driver);
				}
			}
		}
		
	}
	
	@DataProvider(name = "TestData")
	public Object[][] getValueFromExcel(ITestContext context)
	{
		String sheetName = "postTask";// context.getCurrentXmlTest().getParameter("sheetName").toString().trim();
		Object[][] data = gm.getExcelData(EnvironmentVariables.dataPoolPath, sheetName);		
		return data;
	}
	

	@AfterClass
	public void closeTheAction() throws InterruptedException
	{
		Thread.sleep(5000);
		JOptionPane.showMessageDialog(null,"Execution is done. To close browser, press ok");
		//driver.close();
		gm.setLogMsg("info", "Starting Validation of Task Details Page Test");		
	}
}