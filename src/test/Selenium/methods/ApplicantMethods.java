package methods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import pageObjects.DashboardObjects;
import pageObjects.FilterObjects;
import pageObjects.HeaderObjects;
import pageObjects.PostTaskObjects;
import pageObjects.TaskDetailsObjects;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;

public class ApplicantMethods {
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();
	
	ValidateTaskDetailsMethods tdm = new ValidateTaskDetailsMethods();
	
//=========================================================================================================================================	
	/**Sprint 7
	 * User Story 11099:Applicant is able to apply to a task through the Apply to Task option from the Tasks Details read mode screen.
	 * @author jan.carlo.l.sabanal
	 * @since 1/31/2017
	 * 
	*/ 
	public void applyForJob(WebDriver driver, String applyNotes, String addDetails, String useCase ) throws InterruptedException	
	{
		String taskStatus = null;
		
		System.out.println(useCase);
		
			switch(useCase.replaceAll(".0", "")){
			
			case "1"://1. if applicant is NOT a marketplace member
				applyCase1(driver);
				break;
				
			case "2"://2. if applicant IS a marketplace member BUT task is restricted AND applicant is not target
				applyCase1(driver);//same with case 1
				break;
				
			case "3"://3. if applicant IS a marketplace member and even if task is restricted, he is qualified to apply
				applyCase3(driver, applyNotes, addDetails);
				break;
				
			case "4"://1. if task is withdrawn before application
				applyCase1(driver);
				break;
			
			default:
			}//end switch
	}
	
	
//=========================================================================================================================================	
	
	public void applyCase3(WebDriver driver, String applyNotes, String addDetails) throws InterruptedException	
	{
				if(gm.elementExist(driver, TaskDetailsObjects.applyButton)){
					driver.findElement(TaskDetailsObjects.applyButton).click();
					Thread.sleep(2000);
					driver.findElement(TaskDetailsObjects.cancelApplyButton).click();
					Thread.sleep(2000);
					driver.findElement(TaskDetailsObjects.applyButton).click();
					Thread.sleep(2000);
					
					if(gm.elementExist(driver, TaskDetailsObjects.applyModal)){
						System.out.println("im here modal");
						gm.checkThings(driver, TaskDetailsObjects.applyModalTexts1, "Texts should be correct");
						gm.checkThings(driver, TaskDetailsObjects.applyModalTexts2, "Texts should be correct");						
						
						if(addDetails.contains("Supervisor Approval Required")){
							System.out.println("im here supervisor");
							gm.checkThings(driver, TaskDetailsObjects.superApproval, "Supervisor Approval Required checkbox should exist");
							gm.checkThings(driver, TaskDetailsObjects.modalApplyButtonDisabled, "Apply button should be disabled prior to ticking Supervisor Approval Required checkbox");
							driver.findElement(TaskDetailsObjects.superApprovalCheckBox).click();
						} else {
							gm.checkHiddenThings(driver, TaskDetailsObjects.superApproval, "Supervisor Approval Required checkbox should NOT exist");
						}
						
						if(!applyNotes.equals("null"))
							driver.findElement(TaskDetailsObjects.applyTextInputArea).sendKeys(applyNotes);
						
						driver.findElement(TaskDetailsObjects.modalApplyButton).click();
						
						Thread.sleep(3000);
						String taskStatus = driver.findElement(TaskDetailsObjects.taskStatus).getText();
						if(taskStatus.equals("OPEN - APPLIED")&& gm.elementExist(driver, TaskDetailsObjects.applicantWithdrawButton))
							gm.setLogMsg("Pass", "Application successful!");
						else
							gm.logScreenshot("Fail", "Application is not successful!!", driver);
						
					}else{
						gm.logScreenshot("Fail", "Apply Modal is not displayed, Applicant cannot apply!", driver);
					}
					
					
				}else{
					gm.logScreenshot("Fail", "Apply Button is not displayed, Applicant cannot apply!", driver);
				}
		
	}
	
	

//=========================================================================================================================================	

	public void applyCase1(WebDriver driver) throws InterruptedException	
	{
				if(gm.elementExist(driver, TaskDetailsObjects.applyButton)){
					gm.logScreenshot("Fail", "Apply Button should NOT be displayed, Applicant must not able to apply!", driver);
				}else{
					gm.logScreenshot("Pass", "Apply Button is not displayed, Applicant cannot apply!", driver);
				}
	}
	
//=========================================================================================================================================	
	/**
	 * Sprint 7
	 * User Story 11100:Applicant is able to see the Withdraw Application option from the Task Details read mode screen.
	 * @author jan.carlo.l.sabanal
	 * @since 2017/01/31
	 * 
	 */
	public void withdrawApp(WebDriver driver, String reason, String title) throws InterruptedException	
	{
		String taskStatus = driver.findElement(TaskDetailsObjects.taskStatus).getText();
		if(taskStatus.equals("OPEN - APPLIED")&&gm.elementExist(driver, TaskDetailsObjects.applicantWithdrawButton)){
			gm.setLogMsg("Pass", "Status is correct - " + taskStatus);
			Thread.sleep(2000);
			driver.findElement(TaskDetailsObjects.applicantWithdrawButton).click();
			Thread.sleep(5000);
			
			taskStatus = driver.findElement(TaskDetailsObjects.taskStatus).getText();
			if(taskStatus.equals("OPEN - POSTED")&&gm.elementExist(driver, TaskDetailsObjects.applyButton))
				gm.setLogMsg("Pass", "Application successfully withdrawn! - " + taskStatus);
			else
				gm.logScreenshot("Fail", "Application is NOT successfully withdrawn!",driver);
		}
		else
			gm.logScreenshot("Fail", "Status is Incorrect",driver);
	}
	
	
	
	
	
	
	
}
