package methods;


import org.openqa.selenium.WebDriver;

//import driver.EnvironmentVariables;
import driver.GenericMethods;
import methods.VerifyLinks;
import pageObjects.FooterObjects;



/**
 * Sprint 1
 * Description: ALW ReArch Regression - 6991 Verify Footer Existence
 * @author jan.carlo.l.sabanal
 * @since 2016/11/09
 *
 */
public class FooterMethods {
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();
	VerifyLinks vl = new VerifyLinks();
	

//--------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * @param driver
	 * @throws InterruptedException
	 * 
	 */
	public void verifyFooter(WebDriver driver) throws InterruptedException	
	{
		
		Thread.sleep(10000);
		/**6991.1 Verify that the Footer is added at the bottom of the page.*/
		if(gm.elementExist(driver, FooterObjects.footer)){
			System.out.println("==============================yehey==============================Footer");
			gm.logScreenshot("Pass", "Footer exists!", driver);
		} else {
			gm.logScreenshot("Fail", "No footer!!!", driver);
		}
		
	}
	
	
	public void verifyFooterContents(WebDriver driver, String value1, String value2, String value3, String value4) throws InterruptedException	
	{
		boolean click = false;
//--------------------------------------------------------------------------------------------------------------------------------	
		/**6991.2 Verify that the Footer has Accenture Operations logo at the left part of the footer.*/
		if(gm.elementExist(driver, FooterObjects.acnLogo)){
			System.out.println("==============================yehey==============================Logo");
			gm.logScreenshot("Pass", "Logo exists!", driver);
		} else {
			gm.logScreenshot("Fail", "No logo!!!", driver);
		}
//--------------------------------------------------------------------------------------------------------------------------------		
		/**6991.4 Verify that the column header "LIQUID WORKFORCE" located in the footer contains the following categories:
			-Home
			-Marketplaces
			-Tasks
			-Applications
			-Posts
		 */
		if(gm.elementExist(driver, FooterObjects.liquidWorkforceColumn)){
			System.out.println("==============================yehey==============================lwColumn");
			String[] links = value1.split(",");
			String j = "3";
			
			for (int i = 0; i < links.length; i++) {
				vl.checkLinks(driver, links[i], j, click, "foot", value1, value2, value3, value4);						
		      }
			gm.logScreenshot("info", "Kindly check if there are failed tests!", driver);
			
		} else {
			gm.logScreenshot("Fail", "No LIQUID WORKFORCE!!!", driver);
		}
//--------------------------------------------------------------------------------------------------------------------------------		
		/**6991.11"Verify that the column header ""SUPPORT"" located in the footer contains the following categories:
			-FAQ
			-User Guide
			-Feedback"
		*/
		if(gm.elementExist(driver, FooterObjects.supportColumn)){
			System.out.println("==============================yehey==============================supportColumn");
			String[] links = value2.split(",");
			String j = "2";
			for (int i = 0; i < links.length; i++) {
				vl.checkLinks(driver, links[i], j, click, "foot", value1, value2, value3, value4);						
		      }
			gm.logScreenshot("info", "Kindly check if there are failed tests!", driver);
			
		} else {
			gm.logScreenshot("Fail", "No SUPPORT!!!", driver);
		}
//--------------------------------------------------------------------------------------------------------------------------------		
		/**6991.15 "Verify that the column header ""FOLLOW US"" located in the footer contains the following categories:
			-Circles"
		*/
		if(gm.elementExist(driver, FooterObjects.followUsColumn)){
			System.out.println("==============================yehey==============================fuColumn");
			String[] links = value3.split(",");
			String j = "4";
			for (int i = 0; i < links.length; i++) {
				vl.checkLinks(driver, links[i], j,click, "foot", value1, value2, value3, value4);						
		      }
			gm.logScreenshot("info", "Kindly check if there are failed tests!", driver);
			
		} else {
			gm.logScreenshot("Fail", "No FOLLOW US!!!", driver);
		}
		
	}
	
	/**6991.5 to 6991.10, 12 to 14, 17
	 * 
	 * Verify that all the links are clickable links that will direct the user to the designated landing page.
	 * @param driver
	 * @throws InterruptedException
	 */
	public void checkFooterOfAllPages(WebDriver driver, String value1, String value2, String value3, String value4, String value5) throws InterruptedException	
	{
		String[] links = value4.split(",");
		String[] j = value5.split(",");
		boolean click = true;
		
		for (int i = 0; i < links.length; i++) {
			vl.checkLinks(driver, links[i], j[i], click, "foot", value1, value2, value3, value4);
	      }
		
		
	}
	
	/**6991.19
	 * 
	 * "Verify that the Footer has below wordings at the center bottom of the page.
		©2016 Accenture All rights reserved. Privacy Policy l Terms & Conditions"

	 * */
		public void footerWordings(WebDriver driver) throws InterruptedException	
		{
			
			
			if(gm.elementExist(driver, FooterObjects.footerWordings)&& gm.elementExist(driver, FooterObjects.dataPrivacyStatement) && gm.elementExist(driver, FooterObjects.termsOfUse)){
				System.out.println("==============================yehey==============================footerWords");
				
				gm.logScreenshot("Pass", "Footer wordings are present!", driver);
				
			} else {
				gm.logScreenshot("Fail", "Footer wordings are NOT present, or some parts missing!!", driver);
			}
			
		}

}