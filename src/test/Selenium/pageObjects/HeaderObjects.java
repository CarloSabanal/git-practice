package pageObjects;

import org.openqa.selenium.By;

public class HeaderObjects {
	//login
	public static By header = By.xpath("//nav");
	public static By liquidWorkforceLogo = By.xpath("//img[contains(@src,'liquid_logo.png')]");
	public static By headerLinks = By.xpath("//*[@class='nav navbar-nav']");
	/**replace object below*/
	public static By userNameTopRight = By.xpath("//img[contains(@src,'user.png')]//ancestor::*[@class='col-sm-push-2']");
	public static By home = By.xpath("//nav//descendant::*[text()='Home']");

}
