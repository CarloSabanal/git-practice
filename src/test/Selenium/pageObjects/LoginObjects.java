package pageObjects;

import org.openqa.selenium.By;

public class LoginObjects {
	//login
	public static By txtUsername = By.id("userNameInput");
    public static By txtPassword = By.id("passwordInput");
    public static By btnSignin = By.id("submitButton");
	public static By firstPopUp = By.xpath("//*[@class='div_tc_dp_text']");
	public static By agree = By.xpath("//*[@class='but_accept w-button buttonAlignRight']");
	public static By dataPrivacyStatement = By.xpath("//*[@class='txt_tc']//descendant::a[text()='Data Privacy Statement']");
	public static By termsOfUse = By.xpath("//*[@class='txt_tc']//descendant::a[text()='Terms of Use']");
	public static By accessDenied = By.xpath("//*[@class='alert alert-danger']");
	
	
    public static By btnSecurityCode=By.xpath("//input[@id='otpInput']");

	
	
	
	//headerRibbon
	public static By headerRibbon = By.className("navbar-collapse");
	public static By headerLogo = By.className("col-sm-pull-1");
	public static By headerHome = By.xpath("//*[@class='activeLinkHeader activeLink']/descendant::*[text()='HOME']");
	public static By headerMarket = By.xpath("//*[@class='activeLinkHeader']/descendant::*[text()='MARKETPLACES']");
	public static By headerTasks = By.xpath("//*[@class='activeLinkHeader']/descendant::*[text()='TASKS']");
	public static By headerApp = By.xpath("//*[@class='activeLinkHeader']/descendant::*[text()='APPLICATIONS']");
	public static By headerPosts = By.xpath("//*[@class='activeLinkHeader']/descendant::*[text()='POSTS']");
	public static By headerEID = By.xpath("//*[@class='col-sm-push-3']");
	public static By headerCaret = By.xpath("//*[@class='caret']");
	public static By headerAwayFromHome = By.xpath("//*[@class='activeLinkHeader']/descendant::*[text()='HOME']");
	public static By pageTitle = By.xpath("//div[@class='content']");
	
	

}
