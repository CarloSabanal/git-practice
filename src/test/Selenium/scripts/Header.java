package scripts;

/**5 minutes execution time*/

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import methods.ALW_Methods;
import methods.FooterMethods;
import methods.HeaderMethods;


public class Header {
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();
	HeaderMethods hm = new HeaderMethods();
	ALW_Methods am = new ALW_Methods();

	@BeforeClass()
	public void openBrowser() throws InterruptedException	
	{
		System.setProperty(EnvironmentVariables.driverType, EnvironmentVariables.driverPath);
		driver = new ChromeDriver();//InternetExplorerDriver();ChromeDriver
		gm.setLogMsg("Info", "Successfully launched the browser");
		driver.manage().window().maximize();
		driver.navigate().to(EnvironmentVariables.URL);
		gm.setLogMsg("info", "Start of Header Test");	
	}

	@Test(priority = 0, description ="ALW",dataProvider="TestData")
	@Parameters("sheetName")
	public void testHeader(String flag, String action, String userName, String password, String liquidWorkforce, String support, String followUs, String clickableLinks, String thisIsOnlyFiller) throws InterruptedException	
	{
		if(flag.equalsIgnoreCase("y")&& action.equalsIgnoreCase("header")){
			Thread.sleep(10);
			am.login(driver, userName, password);
			hm.verifyHeader(driver);
			hm.verifyHeaderContents(driver, liquidWorkforce, support, followUs, clickableLinks);
			/**commenting out this line due to fix for bug 7574*///hm.verifyUserName(driver, userName);
			hm.checkHeaderOfAllPages(driver, liquidWorkforce, support, followUs, clickableLinks);
		}
		
	}
	
	@DataProvider(name = "TestData")
	public Object[][] getVauleFromExcel(ITestContext context)
	{
		String sheetName = "header_footer";// context.getCurrentXmlTest().getParameter("sheetName").toString().trim();
		Object[][] data = gm.getExcelData(EnvironmentVariables.dataPoolPath, sheetName);		
		return data;
	}

	@AfterClass
	public void closeTheAction()
	{
		//driver.close();
		gm.setLogMsg("info", "End of Header Test");	
		
	}
}