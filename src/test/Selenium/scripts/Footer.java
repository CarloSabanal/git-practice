package scripts;

/**5 minutes execution time*/

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import methods.ALW_Methods;
import methods.FooterMethods;


public class Footer {
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();
	FooterMethods fm = new FooterMethods();
	ALW_Methods am = new ALW_Methods();

	@BeforeClass()
	public void openBrowser() throws InterruptedException	
	{
		System.setProperty(EnvironmentVariables.driverType, EnvironmentVariables.driverPath);
		driver = new ChromeDriver();//InternetExplorerDriver();ChromeDriver
		gm.setLogMsg("Info", "Successfully launched the browser");
		driver.manage().window().maximize();
		driver.navigate().to(EnvironmentVariables.URL);
		gm.setLogMsg("info", "Start of Footer Test");	
	}

	/**
	 * Sprint 1
	 * Description: ALW ReArch Regression - 6991 Verify Footer Existence
	 * @author jan.carlo.l.sabanal
	 * @since 2016/11/09
	 * 
	 */
	
	@Test(priority = 0, description ="ALW",dataProvider="TestData")
	@Parameters("sheetName")
	public void testFooter(String flag, String action, String userName, String password, String liquidWorkforce, String support, String followUs, String clickableLinks, String colMD) throws InterruptedException	
	{
		if(flag.equalsIgnoreCase("y")&& action.equalsIgnoreCase("footer")){
			am.login(driver, userName, password);
			fm.verifyFooter(driver);
			fm.verifyFooterContents(driver, liquidWorkforce, support, followUs, clickableLinks);
			fm.checkFooterOfAllPages(driver, liquidWorkforce, support, followUs, clickableLinks, colMD);
		}
		
	}
	
	@DataProvider(name = "TestData")
	public Object[][] getVauleFromExcel(ITestContext context)
	{
		String sheetName = "header_footer";// context.getCurrentXmlTest().getParameter("sheetName").toString().trim();
		Object[][] data = gm.getExcelData(EnvironmentVariables.dataPoolPath, sheetName);		
		return data;
	}
	

	@AfterClass
	public void closeTheAction()
	{
		driver.close();
		gm.setLogMsg("info", "End of Footer Test");		
	}
}