package pageObjects;

import org.openqa.selenium.By;

public class Terms_Policies_PageObjects {
	public static By importantDocumentDiv = By.xpath("//*[@class='bg_f9']");
	public static By TermsOfUse = By.xpath("//*[@class='hdr_terms_use' and text()='Terms of Use']");
	public static By DataPrivacy = By.xpath("//*[@class='hdr_terms_use' and text()='Data Privacy Statement']");
	public static By backButton = By.id("btnBack");
	
}
