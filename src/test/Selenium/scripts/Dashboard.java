package scripts;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import methods.ALW_Methods;
import methods.DashboardMethod;

public class Dashboard {
	WebDriver driver = null;
	GenericMethods gm = new GenericMethods();
	DashboardMethod dm = new DashboardMethod();
	ALW_Methods am = new ALW_Methods();
	String sheetName = "";

	@BeforeTest()
	public void loadDashboard() throws InterruptedException	
	{
		System.setProperty(EnvironmentVariables.driverType, EnvironmentVariables.driverPath);
		//driver = new ChromeDriver();
		driver = new InternetExplorerDriver();
		gm.setLogMsg("Info", "Successfully launched the Chrome");
		driver.manage().window().maximize();
		driver.navigate().to(EnvironmentVariables.URL);
		
	}

	@Test(priority = 0, description ="ALW ReArch Dashboard")//,dataProvider="TestData")
	public void dashboard() throws InterruptedException
	{
		Thread.sleep(120000);
		am.login(driver, EnvironmentVariables.USERNAME, EnvironmentVariables.PASSWORD);
		Thread.sleep(60000);
		dm.navigateTo(driver, "Home");
		Thread.sleep(5000);
		dm.verifyMarketPlace(driver);
	}


//	@DataProvider(name = "TestData")
//	public Object[][] getVauleFromExcel(ITestContext context)
//	{
//		String sheetName = context.getCurrentXmlTest().getParameter("sheetName").toString().trim();
//		Object[][] data = gm.getExcelData(EnvironmentVariables.dataPoolPath, sheetName);		
//		return data;
//	} 

//	@AfterTest
//	public void closeTheAction()
//	{
//		gm.setLogMsg("info", "Successfully closing the Driver");
//		driver.close();
//		driver.quit();
//	}
}
