package methods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import pageObjects.DashboardObjects;
import pageObjects.FilterObjects;
import pageObjects.HeaderObjects;
import pageObjects.PostTaskObjects;
import pageObjects.TaskDetailsObjects;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;

public class JobPosterActionsInTaskDetailsPageMethods {
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();
	

	
//=========================================================================================================================================	
	/**Sprint 7
	 * User Story 11094:Task Poster is able to withdraw the task I posted through a Withdraw Task option from the Tasks Details read mode screen.
	 * @author jan.carlo.l.sabanal
	 * @since 1/31/2017
	 * 
	*/
	public void withdrawTask(WebDriver driver, String title, String withdrawReason) throws InterruptedException	
	{
		String status = driver.findElement(TaskDetailsObjects.taskStatus).getText();
		
		if(status.equals("OPEN - POSTED")||status.equals("OPEN - APPLIED")||status.equals("OPEN - OFFERED")||status.equals("ACCEPTED"))
			gm.checkThings(driver, TaskDetailsObjects.withdrawButton, "Withdraw button should be displayed");
		else
			gm.checkThings(driver, TaskDetailsObjects.withdrawButton, "EXPECT FAILED - Withdraw button should NOT be displayed");
		
		driver.findElement(TaskDetailsObjects.withdrawButton).click();
		Thread.sleep(2000);
		driver.findElement(TaskDetailsObjects.withdrawModalCancel).click();
		Thread.sleep(2000);
		driver.findElement(TaskDetailsObjects.withdrawButton).click();
		
		
		if(gm.elementExist(driver, TaskDetailsObjects.withdrawModal)){
			
			String header = driver.findElement(TaskDetailsObjects.withdrawModalHeader).getText();
			
			if(header.contains("Are you sure you want to withdraw the task: "))
				gm.setLogMsg("Pass", "Withdraw Modal Header is displayed");
			else
				gm.logScreenshot("Fail", "Withdraw Modal Header is not displayed", driver);
			
			
			header = driver.findElement(TaskDetailsObjects.withdrawModalHeaderTitle).getText();
			if(header.equals(title))
				gm.setLogMsg("Pass", "Withdraw Modal Header Title is correct");
			else
				gm.logScreenshot("Fail", "Withdraw Modal Header is incorrect", driver);
			
			if(gm.elementExist(driver, TaskDetailsObjects.withdrawModalInput)){
				gm.checkThings(driver, TaskDetailsObjects.withdrawModalSubmitHidden, "Withdraw submit button should be disabled");
				driver.findElement(TaskDetailsObjects.withdrawModalInput).click();
				driver.findElement(TaskDetailsObjects.withdrawModalInput).sendKeys(withdrawReason);
				gm.checkThings(driver, TaskDetailsObjects.withdrawModalSubmitHidden, "Expect FAILED - Button is enabled");
				driver.findElement(TaskDetailsObjects.withdrawModalSubmit).click();
			} else 
				gm.logScreenshot("Fail", "Unable to withdraw job, input field cannot be found", driver);
			
		}else{
			gm.logScreenshot("Fail", "Unable to withdraw job, pop-up modal did not appear", driver);
		}
		Thread.sleep(3000);
		gm.checkThings(driver, TaskDetailsObjects.withdrawnTag, "Withdrawn Tag should be displayed.");
		gm.checkThings(driver, TaskDetailsObjects.withdrawButton, "Expect FAIL - Withdraw button should not be visible.");
		gm.checkThings(driver, TaskDetailsObjects.editButton, "Expect FAIL - Edit button should not be visible.");
		//gm.checkThings(driver, TaskDetailsObjects.emailButton, "Email button should not be visible.");
		gm.checkThings(driver, TaskDetailsObjects.deleteButton, "Expect FAIL - Delete button should not be visible.");
		gm.checkThings(driver, TaskDetailsObjects.copyButton, "Copy Button should remain displayed.");
	
	}
	
	
	
//=========================================================================================================================================	
	/**Sprint 7
	 * User Story 11093:Task Poster is able to delete the task I posted through a Delete Task option from the Tasks Details read mode screen.
	 * @author jan.carlo.l.sabanal
	 * @since 02/01/2017
	 * 
	*/
	public void deleteTask(WebDriver driver, String title, String taskUrl) throws InterruptedException	
	{
		driver.findElement(TaskDetailsObjects.deleteButton).click();
		Thread.sleep(2000);
		driver.findElement(TaskDetailsObjects.deleteModalCancel).click();
		Thread.sleep(2000);
		driver.findElement(TaskDetailsObjects.deleteButton).click();
		
		
		if(gm.elementExist(driver, TaskDetailsObjects.deleteModal)){
			
			String header = driver.findElement(TaskDetailsObjects.deleteModalHeader).getText();
			
			if(header.contains("Are you sure you want to delete the task: "))
				gm.setLogMsg("Pass", "Delete Modal Header is displayed");
			else
				gm.logScreenshot("Fail", "Delete Modal Header is not displayed", driver);
			
			
			header = driver.findElement(TaskDetailsObjects.deleteModalHeaderTitle).getText();
			if(header.equals(title))
				gm.setLogMsg("Pass", "Delete Modal Header Title is correct");
			else
				gm.logScreenshot("Fail", "Delete Modal Header is incorrect", driver);
			
			Thread.sleep(2000);
			driver.findElement(TaskDetailsObjects.deleteModalSubmit).click();
			
		}else{
			gm.logScreenshot("Fail", "Unable to delete job, pop-up modal did not appear", driver);
		}
		
		gm.waitForObject(driver, DashboardObjects.carousel);
		if(driver.getCurrentUrl().equals(EnvironmentVariables.URL + "DashboardMain")){
			gm.setLogMsg("Pass", "Successfully directed to Home Page");
			driver.navigate().to(taskUrl);
			
			Thread.sleep(5000);
			if(driver.getCurrentUrl().equals(EnvironmentVariables.URL + "DashboardMain"))
				gm.setLogMsg("Pass", "Task URL cannot be accessed, Task is deleted successfully!");
			else 
				gm.setLogMsg("Fail", "Wrong URL - " + driver.getCurrentUrl());
			
		} else {
			gm.setLogMsg("Fail", "Wrong URL - " + driver.getCurrentUrl());
		}
		
		
	}
	
}
