package scripts;

/**5 minutes execution time*/

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import methods.ALW_Methods;
import methods.FooterMethods;
import methods.PostSectionMethods;


public class PostsSection {
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();
	PostSectionMethods psm = new PostSectionMethods();
	ALW_Methods am = new ALW_Methods();

	@BeforeClass()
	public void openBrowser() throws InterruptedException	
	{
		System.setProperty(EnvironmentVariables.driverType, EnvironmentVariables.driverPath);
		driver = new InternetExplorerDriver();//InternetExplorerDriver();ChromeDriver
		gm.setLogMsg("Info", "Successfully launched the browser");
		//driver.manage().window().maximize();
		driver.navigate().to(EnvironmentVariables.URL);
		gm.setLogMsg("info", "Starting Posts Section Test");	
	}

	/**
	 * Sprint 1
	 * Description: ALW ReArch Regression - 7789 Post Section on Dashboard
	 * @author jan.carlo.l.sabanal
	 * @since 2016/11/21
	 * 
	 */
	
	@Test(priority = 0, description ="ALW",dataProvider="TestData")
	@Parameters("sheetName")
	public void testPostsSection(String flag, String action, String userName, String password, String taskId, String postId) throws InterruptedException	
	{
		if(flag.equalsIgnoreCase("y")&& action.equalsIgnoreCase("post cards")){
			am.login(driver, userName, password);
			Thread.sleep(10000);
			psm.verifyPostsSectionLinks(driver);
			Thread.sleep(3000);
			psm.clickLink(driver);
			Thread.sleep(3000);
			psm.completedPostsDetails(driver, postId);
		}
		
	}
	
	@DataProvider(name = "TestData")
	public Object[][] getVauleFromExcel(ITestContext context)
	{
		String sheetName = "dashboard";// context.getCurrentXmlTest().getParameter("sheetName").toString().trim();
		Object[][] data = gm.getExcelData(EnvironmentVariables.dataPoolPath, sheetName);		
		return data;
	}
	

	@AfterClass
	public void closeTheAction()
	{
		driver.close();
		gm.setLogMsg("info", "End of Posts Section Test");		
	}
}