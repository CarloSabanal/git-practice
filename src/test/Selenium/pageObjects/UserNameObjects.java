/**
 * 
 */
package pageObjects;

import org.openqa.selenium.By;
/**
 * @author somnath.bhunia
 * @since 2/17/2017
 * 
 */
public class UserNameObjects 
{
	public static By userNameButton = By.id("dropdown-custom-1");
	public static By userImage = By.xpath("//*[@id='app-container']/div/nav//div/ul/li[1]/a/img[@class = 'img-rounded userImage']");
	public static By userNameText = By.xpath("//*[@id='app-container']//a/div/label[@class = 'lblName']");
	public static By userProfileButton = By.xpath("//*[@id='app-container']//div/ul/li[1]/a/div/a[@class = 'btn btn-default btnViewProfile']");
	public static By userFeedback = By.xpath("//*[@id='app-container']//div/ul/li[3]/a/a[@class = 'profileMenu']");
	public static By userSignOut = By.xpath("//*[@id='app-container']//div/ul/li[4]/a/a[@class = 'profileMenu']");
    
}
