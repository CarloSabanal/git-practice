/**
 * 
 */
package pageObjects;

import org.openqa.selenium.By;
/**
 * @author somnath.bhunia
 *
 */
public class MarketplaceObjects
{
	/**Header*/
	public static By marketplaceTitle = By.className("marketplaces-title"); 
	public static By recommended = By.xpath("//*[@id='app-container']//div[1]/ul/li[2]/a/span[@class='txt_dash_tab']");
	public static By myMarketplaces = By.xpath("//*[@id='app-container']//div[1]/ul/li[3]/a/span[@class='txt_dash_tab']");
	
	/**My Marketplaces*/
	public static By marketplaceCount = By.xpath("//*[@id='app-container']//div[2]/div/div[2]/div/div[1]/span[@class = 'marketplaces-count']");
	
	/**My Marketplaces Tiles*/
	public static By myMarketplacesTile(int i){return By.xpath("//*[@id='app-container']//div["+i+"]/div/a/div");}
	public static By myMarketplacesMemberText(int i){return By.xpath("//*[@id='app-container']//div["+i+"]/div/a/div/span/span");}
	public static By myMarketplacesName(int i){return By.xpath("//*[@id='app-container']//div["+i+"]//div/p");}
	public static By myMarketplacesLogo(int i){return By.xpath("//*[@id='app-container']//div["+i+"]//img[@id='marketplacelist-card'");}
	public static By myMarketplacesIcon(int i){return By.xpath("//*[@id='app-container']//div["+i+"]//img[2]");}
	public static By myMarketplacesMemberCount(int i){return By.xpath("//*[@id='app-container']//div["+i+"]//div/a/div/div/div[1]/span[1]");}
	public static By myMarketplacesTasks(int i){return By.xpath("//*[@id='app-container']//div["+i+"]//span[2]");}
	
	/**Recommended */
	public static By recommendedCount = By.xpath("//*[@id='app-container']//div[2]/div/div[1]/div/div[1]/span[@class='marketplaces-count']");
	
	/**Recommended Tiles*/
	
	public static By RecommendedTile(int i){return By.xpath("//*[@id='app-container']//div["+i+"]/div/a/div");}
	public static By RecommmendedForYouText(int i){return By.xpath("//*[@id='app-container']//div["+i+"]/div/a/div/span/span");}
	public static By RecommendedName(int i){return By.xpath("//*[@id='app-container']//div["+i+"]//div/p");}
	public static By RecommendedLogo(int i){return By.xpath("//*[@id='app-container']//div["+i+"]//img[@id='marketplacelist-card'");}
	public static By RecommendedIcon(int i){return By.xpath("//*[@id='app-container']//div["+i+"]//img[2]");}
	public static By RecommendedMemberCount(int i){return By.xpath("//*[@id='app-container']//div["+i+"]//div/a/div/div/div[1]/span[1]");}
	public static By RecommendedTasks(int i){return By.xpath("//*[@id='app-container']//div["+i+"]//span[2]");}
	
}
