package driver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import org.apache.commons.io.FileUtils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

public class GenericMethods {

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
	/**@author somnath.bhunia
	 * @since 2/16/2017
	 */
	public int getNoFromString(String label)
	{
		int intValue = 0, i=0; 
		char value[] = new char[255]; 
		String stringValue="";
		for(i=0;i<label.length();i++)
		{
			value[i]=label.charAt(i);
			if(Character.isDigit(value[i]))
			{
				stringValue += value[i];
			}
		}
		intValue = Integer.parseInt(stringValue);
		return intValue;
	}
	
//=========================================================================================================================================

	public void checkThings(WebDriver driver, By element, String comment) throws InterruptedException	
	{			
		if(elementExist(driver, element)){
			setLogMsg("Pass", comment);
		} else {
			if(comment.contains("FAILED"))
				setLogMsg("Fail", comment);
			else		
				logScreenshot("Fail", comment, driver);
		}
		
	}
	
	
	
	public void checkHiddenThings(WebDriver driver, By element, String comment) throws InterruptedException	
	{			
		if(elementExist(driver, element)){
			logScreenshot("Fail", comment, driver);
		} else {
			setLogMsg("Pass", comment);	
		}
		
	}
	
	public void checkElementText(WebDriver driver, By element, String text) throws InterruptedException	
	{			
		if(driver.findElement(element).getText().contains(text)){
			setLogMsg("Pass", element + " exists!");
		} else {
			setLogMsg("Fail", element + " either does not exist or text is incorrect: " + driver.findElement(element).getText() + " - " + text);	
		}
		
	}
	
	public void checkWebElementText(WebDriver driver, WebElement element, String text) throws InterruptedException	
	{			
		if(element.getText().contains(text)){
			setLogMsg("Pass", element + " exists!");
		} else {
			setLogMsg("Fail", element + " either does not exist or text is incorrect: " + element.getText() + " - " + text);	
		}
		
	}
		
	
//=========================================================================================================================================
	public void checkErrorMessages(WebDriver driver, By error, String message, By field) throws InterruptedException{
		String leftBorderColor = driver.findElement(field).getCssValue("border-left-color");
		if(elementExist(driver, error)){
			setLogMsg("Pass", "error is displayed!! " + message);
			
			if(leftBorderColor.equals("rgba(255, 68, 68, 1)"))
				setLogMsg("Pass", "field is red!!");
			else 
				logScreenshot("Fail", "field is not red!!!" + leftBorderColor + " " + message, driver);
			
			
		} else {
			
			logScreenshot("Fail", "error is not displayed!! " + message, driver);
			
			leftBorderColor = driver.findElement(field).getCssValue("border-left-color");
			if(leftBorderColor.equals("rgba(192, 155, 209, 1)"))
				setLogMsg("Fail", "Wrong!!! field is purple!!!! " + message);
			else 
				setLogMsg("info", "error message not shown but field is not red: " + leftBorderColor + " " + message);
			
		}
	}
	
//=========================================================================================================================================
	public void checkErrorMessagesAreHidden(WebDriver driver, By error, String message, By field) throws InterruptedException{
		String leftBorderColor = driver.findElement(field).getCssValue("border-left-color");
		if(elementExist(driver, error)){
			
			setLogMsg("Fail", "error is displayed even when not expected!! " + message);
			
			leftBorderColor = driver.findElement(field).getCssValue("border-left-color");
			if(leftBorderColor.equals("rgba(192, 155, 209, 1)"))
				setLogMsg("Pass", "Field is purple even when error is displayed!!!! "+ leftBorderColor + " " + message);
			else 
				setLogMsg("Fail", "field is not purple!! " + leftBorderColor);
						
		} else {
			
			setLogMsg("Pass", "error is not displayed!! " + message);
			
			if(leftBorderColor.equals("rgba(255, 68, 68, 1)"))
				logScreenshot("Fail", "field is red!! It should be purple!!", driver);
			else 
				setLogMsg("Passed", "field is not red!!!" + leftBorderColor + " " + message);
			
		}
	}
	
//=========================================================================================================================================	
	public boolean elementExist(WebDriver driver, By locator){
		boolean exist = false;
		try{
			driver.findElement(locator);
			exist=true;
		}
		catch(Exception e){
			exist=false;
		}
		return exist;
	} 

	public void clearLogs(){
		Reporter.clear();	
	}
	
	public void logScreenshot(String condition,String msg, WebDriver driver)
	{	
		String userDirector = System.getProperty("user.dir") + "/"; 

		String s1 = null,s2 ="";
		System.setProperty("org.uncommons.reportng.escape-output", "false");
		if(true)
		{
			try {

				String failureImageFileName =  new SimpleDateFormat("MM-dd-yyyy_HH-ss").format(new GregorianCalendar().getTime())+ ".png"; 
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile, new File("Screenshot\\"+failureImageFileName)); 
				s2 = "<a href=\""+ userDirector +"\\Screenshot\\" + failureImageFileName +"\"><img src=\"file:///" + userDirector +"\\Screenshot\\" + failureImageFileName + "\" alt=\"\""+ "height='300' width='300' border =1/> "+"<br />";


			} catch (IOException e1) {
				e1.printStackTrace();
			}


			if (condition.equalsIgnoreCase("info"))	
			{
				s1 = "<table width= 100% ; border = 1.5; rules =rows >"

				+ "<tbody>"

					+ "<tr>"
					+ "<td> <font style=\\width:130px;text-align:left;color:black;font-size:12px;font-family:verdana;\\><strong>Info</strong></td>" 
					+ "<td> <font style=\\width:130px;text-align:left;color:black;font-size:12px;font-family:verdana;\\>"+msg+"</td>" 
					+ "</tr>"

					+ "<tr>"
					+ "<td colspan=2>" 									
					+ "<table width= 100% ; rules =rows >"

												+ "<tbody>"

													+ "<tr>"
													+ "<td> <font style=\\width:130px;text-align:left;color:black;font-size:12px;font-family:verdana;\\><strong>"+"Screenshot"+"</strong></td>" 
													+ "<td> <font style=\\width:130px;text-align:left;color:black;font-size:12px;font-family:verdana;\\><strong>"+s2+"</strong></td>"

													+ "</tr>"
													+"</tbody>"
													+"</table>"

								+ "</td>"	
								+"</tr>"

				+ "</tbody>"
				+"</table>";

			}

			if (condition.equalsIgnoreCase("Pass"))	
			{
				s1 = "<table width= 100% ; border = 1.5; rules =rows >"

				+ "<tbody>"

					+ "<tr>"
					+ "<td> <font style=\\width:130px;text-align:left;color:green;font-size:12px;font-family:verdana;\\><strong>Pass</strong></td>" 
					+ "<td> <font style=\\width:130px;text-align:left;color:black;font-size:12px;font-family:verdana;\\>"+msg+"</td>" 
					+ "</tr>"

					+ "<tr>"
					+ "<td colspan=2>" 
					+ "<table width= 100% ;rules =rows >"

												+ "<tbody>"

													+ "<tr>"
													+ "<td> <font style=\\width:130px;text-align:left;color:black;font-size:12px;font-family:verdana;\\><strong>"+"Screenshot"+"</strong></td>" 
													+ "<td> <font style=\\width:130px;text-align:left;color:black;font-size:12px;font-family:verdana;\\><strong>"+s2+"</strong></td>"

													+ "</tr>"
													+"</tbody>"
													+"</table>"

								+ "</td>"	
								+"</tr>"

				+ "</tbody>"
				+"</table>";

			}
			if (condition.equalsIgnoreCase("Fail"))	
			{
				s1 = "<table width= 100% ; border = 1.5; rules =rows >"

				+ "<tbody>"

					+ "<tr>"
					+ "<td> <font style=\\width:130px;text-align:left;color:red;font-size:12px;font-family:verdana;\\><strong>Fail</strong></td>" 
					+ "<td> <font style=\\width:130px;text-align:left;color:black;font-size:12px;font-family:verdana;\\>"+msg+"</td>" 
					+ "</tr>"

					+ "<tr>"
					+ "<td colspan=2>" 
					+ "<table width= 100% ; rules =rows >"

												+ "<tbody>"

													+ "<tr>"
													+ "<td> <font style=\\width:130px;text-align:left;color:black;font-size:12px;font-family:verdana;\\><strong>"+"Screenshot"+"</strong></td>" 
													+ "<td> <font style=\\width:130px;text-align:left;color:black;font-size:12px;font-family:verdana;\\><strong>"+s2+"</strong></td>"

													+ "</tr>"
													+"</tbody>"
													+"</table>"
													+ "</td>"	
													+"</tr>"

				+ "</tbody>"
				+"</table>";

			}
			Reporter.log(s1);
		}
	}
	
	
	public void setLogMsg(String condition, String msg)
	{
		String s1 ="";

		if (condition.equalsIgnoreCase("info"))	
		{
			s1 = "<table width= 100% ; border = 1.5; rules =rows >"

				+ "<tbody>"

					+ "<tr>"
					+ "<td> <font style=\\width:130px;text-align:left;color:black;font-size:12px;font-family:verdana;\\><strong>Info</strong></td>" 
					+ "<td> <font style=\\width:130px;text-align:left;color:black;font-size:12px;font-family:verdana;\\>"+msg+"</td>" 
					+ "</tr>"									
					+ "</tbody>"
					+"</table>";

		}

		if (condition.equalsIgnoreCase("Pass"))	
		{
			s1 = "<table width= 100% ; border = 1.5; rules =rows >"

				+ "<tbody>"

					+ "<tr>"
					+ "<td> <font style=\\width:130px;text-align:left;color:Green;font-size:12px;font-family:verdana;\\><strong>Pass</strong></td>" 
					+ "<td> <font style=\\width:130px;text-align:left;color:black;font-size:12px;font-family:verdana;\\>"+msg+"</td>" 
					+ "</tr>"									
					+ "</tbody>"
					+"</table>";

		}
		if (condition.equalsIgnoreCase("fail"))	
		{
			s1 = "<table width= 100% ; border = 1.5; rules =rows >"

				+ "<tbody>"

					+ "<tr>"
					+ "<td> <font style=\\width:130px;text-align:left;color:red;font-size:12px;font-family:verdana;\\><strong>Fail</strong></td>" 
					+ "<td> <font style=\\width:130px;text-align:left;color:black;font-size:12px;font-family:verdana;\\>"+msg+"</td>" 
					+ "</tr>"									
					+ "</tbody>"
					+"</table>";
		}
		Reporter.log(s1);
	}

	public boolean waitForObject1(WebDriver driver,By object)
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(driver,60);
			wait.until(ExpectedConditions.presenceOfElementLocated(object));
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	} 

	public void waitForObject(WebDriver driver,By object)
	{
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.presenceOfElementLocated(object));
	}
	
	public void waitForObject(WebDriver driver,By object,long secs)
	{
		WebDriverWait wait = new WebDriverWait(driver,secs);
		wait.until(ExpectedConditions.presenceOfElementLocated(object));
	}

	public String[][] getExcelData(String fileName, String sheetName) 
	{
		String[][] arrayExcelData = null;
		org.apache.poi.ss.usermodel.Workbook tempWB;

		try {

			if(fileName.contains(".xlsx")){
				tempWB = new XSSFWorkbook(fileName);
			}
			else{				
				InputStream inp = new FileInputStream(fileName);
				tempWB = (org.apache.poi.ss.usermodel.Workbook) new HSSFWorkbook(new POIFSFileSystem(inp));					
			}

			org.apache.poi.ss.usermodel.Sheet sheet = tempWB.getSheet(sheetName);

			// Total rows counts the top heading row
			int totalNoOfRows = sheet.getLastRowNum();
			Row row = sheet.getRow(0);
			int totalNoOfCols = row.getLastCellNum();
			
			System.out.println("Total rows are : "+ totalNoOfRows);
			System.out.println("Total columns are : "+ totalNoOfCols);

			arrayExcelData = new String[totalNoOfRows][totalNoOfCols];

			try {
				for (int i= 1 ; i < totalNoOfRows+1; i++) 
				{
					for (int j=1; j < totalNoOfCols+1; j++) 
					{
						row = sheet.getRow(i);
					//	System.out.println(row.getCell(j-1).toString().trim());
						arrayExcelData[i-1][j-1] = row.getCell(j-1).toString().trim();
						System.out.println(arrayExcelData[i-1][j-1].toString().trim());
					}
					
					System.out.println("Completed Row number : "+ i );
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			e.printStackTrace();
		}
		return arrayExcelData;
	}
	
	public int findCol(Sheet sheet,String ColName)
	{
		Row row = null;
		int colCount = 0;

		row = sheet.getRow(0);
		if (!(row==null))
		{
			colCount = row.getLastCellNum();
		}
		else
			colCount = 0;

		for (int j=0;j<colCount;j++)
		{
			if(!( row.getCell(j)==null)){
				if (row.getCell(j).toString().trim().equalsIgnoreCase(ColName)|| row.getCell(j).toString().trim().equalsIgnoreCase((ColName+"[][String]"))){
					return j;
				}
			}
		}
		return -1;
	}

	public String getValueFromDatasheet(String SheetName,String colName,int rowNo)
	{
		try
		{
			Workbook tempWB;
			String value ="";
			if (EnvironmentVariables.dataPoolPath.contains(".xlsx"))
				tempWB = new XSSFWorkbook(EnvironmentVariables.dataPoolPath);

			else
			{
				FileInputStream inp = new FileInputStream(EnvironmentVariables.dataPoolPath);
				tempWB = (Workbook) new HSSFWorkbook(new POIFSFileSystem(inp));	
			}

			Sheet sheet = tempWB.getSheet(SheetName);
			Row row = sheet.getRow(rowNo);

			if(row == null){
				return null;
			}
			try{
				value = row.getCell(findCol(sheet, colName)).toString().trim();
				return value;
			}
			finally {}
		}
			catch(FileNotFoundException e)
			{
				setLogMsg("Fail", "File not found in the path : "+ EnvironmentVariables.dataPoolPath);
			}
			catch(IOException e)
			{
				setLogMsg("Fail", "Problem in reading the File");
			}
			return null;
		}
}