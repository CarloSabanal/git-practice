package pageObjects;

import org.openqa.selenium.By;

public class FooterObjects {
	//login
	public static By footer = By.xpath("//footer");
	public static By acnLogo = By.xpath("//img[contains(@src,'img_Accenture_Logo.png')]");
	public static By liquidWorkforceColumn = By.xpath("//*[@class='col-md-3']//descendant::*[text()='LIQUID WORKFORCE']");
	
	public static By supportColumn = By.xpath("(//*[@class='col-md-2']//descendant::*[text()='SUPPORT'])[1]");
	
	
	
	public static By followUsColumn = By.xpath("//*[@class='col-md-4']//descendant::*[text()='FOLLOW US']");
	public static By footerWordings = By.xpath("//*[@class='text-center FooterText' and text()='©2016 Accenture. All rights reserved.']");
	public static By dataPrivacyStatement = By.xpath("//*[@class='text-center FooterText']//descendant::*[text()=' Data Privacy Statement']");
	public static By termsOfUse = By.xpath("//*[@class='text-center FooterText']//descendant::*[text()='Terms of Use']");
	

}
