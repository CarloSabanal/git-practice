package methods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import driver.EnvironmentVariables;
import driver.GenericMethods;
import pageObjects.DashboardObjects;
import pageObjects.FilterObjects;
import pageObjects.HeaderObjects;
import pageObjects.PostTaskObjects;

import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;

/**
 * 
 * @author jan.carlo.l.sabanal
 * @since 11/21/2016
 * Description - this is for validating the post section of the dashboard  
 * 
 * */

public class PostTaskMethods {
	WebDriver driver = null;;
	GenericMethods gm = new GenericMethods();
	
	
//=========================================================================================================================================	

	/**User Story 13109: Able to see purple star as additional indicator for required fields in the Post a Task Screen.
	 * @author jan.carlo.l.sabanal
	 * @since 02/02/2017
	 * 
	 * 
	*/
	public void validateRequiredFieldsIndicator(WebDriver driver) throws InterruptedException	
	{
		
		By[] element = {PostTaskObjects.jobTitleFieldLabelStar,PostTaskObjects.workStartDayStar,PostTaskObjects.workEndDayStar,PostTaskObjects.workExpireDayStar,PostTaskObjects.descriptionStar,PostTaskObjects.jobTitleFieldLabelStar,PostTaskObjects.marketplaceStar,PostTaskObjects.categoryStar,PostTaskObjects.effortStar,PostTaskObjects.highlightedFieldsNote};
		
		
		
		String star = null;
		
		for(int i=0;i<element.length;i++){
			star = driver.findElement(element[i]).getCssValue("color");
			gm.checkThings(driver, element[i], element[i] + " should be visible");
			
			if(star.equals("rgba(192, 155, 209, 1)")){
				gm.setLogMsg("Pass", "Star indicator is present, color is correct!");
			} else {
				gm.setLogMsg("Fail", "Color is incorrect!!! " + element[i] + " color - " + star);
			}
		}
		
	}	
	
//=========================================================================================================================================	

	/**User Story 10343:Task Poster is able to see information of the Targeted Crowd in the Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @since 1/4/2016
	 * Removed under User Story 11591:Task Poster is able to see and access the Target Reach page from the Post a task screen
	 * 
	*/
	public void optimize(WebDriver driver) throws InterruptedException	
	{	
		if(!gm.elementExist(driver, PostTaskObjects.crowdCountMessageHidden)){
			gm.checkThings(driver, PostTaskObjects.crowdCountMessage1, "crowdCountMessage1");
			gm.checkThings(driver, PostTaskObjects.crowdCountMessage2, "crowdCountMessage2");
			String message3 = driver.findElement(PostTaskObjects.crowdCountMessage3).getText();
			if(message3.equals("Refine the post reach by clicking the \"Target Crowd\" button in the toolbar.")){
				gm.setLogMsg("pass", "Message is visible and correct");
			} else {
				gm.logScreenshot("Fail", "Message is either invisible or incorrect: " + message3 , driver);
			}
			gm.checkThings(driver, PostTaskObjects.crowdNoImage, "image");
			/**after selecting marketplace, the message will change. Validation is in marketplace method*/	
		} else {
			gm.logScreenshot("Fail", "Initial message is hidden ", driver);
		}
		
	}
	
//=========================================================================================================================================	
	
	/**User Story 9528:Able to provide information for the Work Location in the Task Detail section of Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @since 12/19/2016
	 * 
	*/
	public void workLocation(WebDriver driver, String workLocationType, String workLocation, String validity) throws InterruptedException	
	{			
		if(gm.elementExist(driver, PostTaskObjects.workLocationDiv)){
			System.out.println("==============================yehey==============================workLocationExists");
			gm.logScreenshot("Pass", "able to select work location!!!", driver);
			
			if(workLocationType.equalsIgnoreCase("virtual")){
				gm.checkThings(driver,PostTaskObjects.virtualLocationText, "virtualLocationText");
				
				/**check if text field will be disabled after clicking other buttons*/
				driver.findElement(PostTaskObjects.clientSiteButton).click();
				driver.findElement(PostTaskObjects.virtualLocationButton).click();
				Thread.sleep(500);
				gm.checkThings(driver,PostTaskObjects.virtualLocationText, "virtualLocationText");
				
				/**check if text field will be disabled after clicking other buttons*/
				driver.findElement(PostTaskObjects.accentureLocationButton).click();
				driver.findElement(PostTaskObjects.virtualLocationButton).click();
				Thread.sleep(500);
				gm.checkThings(driver,PostTaskObjects.virtualLocationText, "virtualLocationText");			
			} 
			
			if(workLocationType.equalsIgnoreCase("client site")){
				
				driver.findElement(PostTaskObjects.clientSiteButton).click();
				Thread.sleep(2000);
				gm.checkThings(driver,PostTaskObjects.clientSiteText, "clientSiteText");
				
				/**check if text field will be disabled after clicking other buttons*/
				driver.findElement(PostTaskObjects.virtualLocationButton).click();
				Thread.sleep(500);
				driver.findElement(PostTaskObjects.clientSiteButton).click();
				Thread.sleep(2000);
				gm.checkThings(driver,PostTaskObjects.clientSiteText, "clientSiteText");
				
				/**check if text field will be disabled after clicking other buttons*/
				driver.findElement(PostTaskObjects.accentureLocationButton).click();
				Thread.sleep(500);
				driver.findElement(PostTaskObjects.clientSiteButton).click();
				Thread.sleep(2000);
				gm.checkThings(driver,PostTaskObjects.clientSiteText, "clientSiteText");
				gm.checkThings(driver,PostTaskObjects.clientSitePlaceHolder, "clientSitePlaceHolder");
							
				
					if(workLocation.equals("null")){
						driver.findElement(PostTaskObjects.clientSiteText).click();
						driver.findElement(PostTaskObjects.jobTitleFieldLabel).click(); //here!
						Thread.sleep(2000);
						gm.checkErrorMessages(driver, PostTaskObjects.workLocationErrorMessage1, "workLocationErrorMessage1", PostTaskObjects.clientSiteText);
										
					} else {
						Thread.sleep(500);
						driver.findElement(PostTaskObjects.clientSiteText).clear();
						Thread.sleep(500);
						driver.findElement(PostTaskObjects.clientSiteText).click();
						Thread.sleep(500);
						driver.findElement(PostTaskObjects.clientSiteText).sendKeys(workLocation);
						
						
						Thread.sleep(2000);
					}
				
				
			}
			
			if(workLocationType.equalsIgnoreCase("Accenture Location")){
				Thread.sleep(500);
				driver.findElement(PostTaskObjects.accentureLocationButton).click();
				Thread.sleep(2000);
				gm.checkThings(driver,PostTaskObjects.accentureLocationfield, "accentureLocationfield");
				
				/**check if text field will be disabled after clicking other buttons*/
				Thread.sleep(500);
				driver.findElement(PostTaskObjects.virtualLocationButton).click();
				Thread.sleep(500);
				driver.findElement(PostTaskObjects.accentureLocationButton).click();
				Thread.sleep(2000);
				gm.checkThings(driver,PostTaskObjects.accentureLocationfield, "accentureLocationfield");
				
				/**check if text field will be disabled after clicking other buttons*/
				Thread.sleep(500);
				driver.findElement(PostTaskObjects.clientSiteButton).click();
				Thread.sleep(500);
				driver.findElement(PostTaskObjects.accentureLocationButton).click();	
				Thread.sleep(2000);
				gm.checkThings(driver,PostTaskObjects.accentureLocationfield, "accentureLocationfield");
				gm.checkThings(driver,PostTaskObjects.accentureLocationPlaceHolder, "accentureLocationPlaceHolder");
				
					if(workLocation.equals("null")){
						driver.findElement(PostTaskObjects.jobTitleFieldLabel).click(); //here!
						Thread.sleep(2000);
						gm.checkErrorMessages(driver, PostTaskObjects.workLocationErrorMessage2, "workLocationErrorMessage2", PostTaskObjects.accentureLocationfield);
					} else {
						if(validity.equals("valid")){
							driver.findElement(PostTaskObjects.accentureLocationfield).click();
				            Thread.sleep(3000);
				            
				            List<WebElement> elm = driver.findElements(PostTaskObjects.accentureLocationfieldInput);
				            System.out.println("The len is : "+ elm.size());
				            
				            elm.get(0).sendKeys(workLocation);
				            Thread.sleep(10000);
				            driver.findElement(PostTaskObjects.accentureLocationComboBox).click();
						} else {
							driver.findElement(PostTaskObjects.jobTitleFieldLabel).click(); //here!
							Thread.sleep(2000);
							gm.checkThings(driver,PostTaskObjects.accentureLocationfield, "accentureLocationfield");
						}
						
			          
					}
						
			}
			
		} else {
			gm.logScreenshot("Fail", "cannot select work location!!!", driver);
		}
	}
	

	

	
//=========================================================================================================================================
	/**User Story 9514:Able to provide Title in the Task Detail section of Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 12/19/2016
	 * 
	*/
	public void addTitle(WebDriver driver, String title) throws InterruptedException{
		if(gm.elementExist(driver, PostTaskObjects.jobTitleField) && gm.elementExist(driver, PostTaskObjects.jobTitleFieldLabel)){
			System.out.println("==============================yehey==============================title field");
			gm.checkErrorMessages(driver, PostTaskObjects.titleErrorMessage, "titleErrorMessage", PostTaskObjects.jobTitleFieldLabel);
			gm.checkThings(driver, PostTaskObjects.titlePlaceHolder, "titlePlaceHolder");
				if(!title.equals("null")){
					driver.findElement(PostTaskObjects.jobTitleField).click();
					driver.findElement(PostTaskObjects.jobTitleField).sendKeys(title);
					Thread.sleep(2000);
					gm.checkErrorMessagesAreHidden(driver, PostTaskObjects.titleErrorMessage, "titleErrorMessage", PostTaskObjects.jobTitleFieldLabel);
					gm.setLogMsg("Pass", "Title is set!!");
					Thread.sleep(2000);
					
				} else {
					driver.findElement(PostTaskObjects.jobTitleField).click();
					driver.findElement(PostTaskObjects.jobTitleFieldLabel).click(); //here!
					Thread.sleep(2000);
					gm.checkErrorMessages(driver, PostTaskObjects.titleErrorMessage, "titleErrorMessage - because NULL", PostTaskObjects.jobTitleFieldLabel);		
				}
					
		} else {
			gm.logScreenshot("Fail", "cannot set title!!!", driver);
		}
		
	}

//=========================================================================================================================================	
	/**User Story 9526:Able to confirm that my task has a WBS Provided or not in the Task Detail section of Post a task screen.
	 * User Story 9524:Able to tag my task as Urgent Task in the Task Detail section of Post a task screen.
	 * User Story 9530:Able to tag if the tasks will require Supervisor Approval Required or not in the Task Detail section of Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 12/19/2016
	 * 
	*/
	public void additionalDetails(WebDriver driver, String details, String charge, String marketplace) throws InterruptedException{
		String[] addDetails = details.split(",");
		if(gm.elementExist(driver, PostTaskObjects.addDetails)){
			gm.setLogMsg("Pass", "additional details div exists!!");
			
				if(!details.equals("null")){
					
					for(int i=0;i<addDetails.length;i++){
						if(addDetails[i].equalsIgnoreCase("Urgent Task")){
							driver.findElement(PostTaskObjects.urgent).click();
						} else {
							if(addDetails[i].equalsIgnoreCase("WBS Provided")){
								gm.checkThings(driver, PostTaskObjects.wbsHide, "wbsHide");
								driver.findElement(PostTaskObjects.wbs).click();
								gm.checkHiddenThings(driver, PostTaskObjects.wbsHide, "wbsHide - buttons should be visible");
								/**
								 * @author jan.carlo.l.sabanal
								 * @since 01/11/2017
								 * Sprint 6
								 * User Story 11588:Task Poster is able to provide WBS information in the Post a task screen.
								 * */
								if(charge.equalsIgnoreCase("yes"))
									driver.findElement(PostTaskObjects.charge).click();
								
									
							} else {
								try{
									driver.findElement(PostTaskObjects.requireApproval).click();
								} catch (WebDriverException wde) {
									
									/**
									 * @author jan.carlo.l.sabanal
									 * @since 01/11/2017
									 * Sprint 6
									 * User Story 11589:Task Poster is able to see Supervisor Approval Required field in the Post a task screen based on the correct configuration of the Marketplace
									 * */
									
									System.out.println(marketplace + " does not have Supervisor Approval Required checkbox");
									gm.setLogMsg("info", marketplace + " does not have Supervisor Approval Required checkbox");
								}
							}
						}
						gm.setLogMsg("info", details);
					}
					gm.logScreenshot("Pass", "additional details checked!!", driver);
				}
				
			
		} else {
			gm.logScreenshot("Fail", "no additional details div!!", driver);
		}
	}
	//=========================================================================================================================================
	
	/**
	 * @author jan.carlo.l.sabanal
	 * User Story 9527:Able to provide information if my task is a Multi-Offer or not in the Task Detail section of Post a task screen.
	 * @since 12/19/2016
	 * */
	
	public void multiOffer(WebDriver driver, String multiOffer, String resource) throws InterruptedException{
		
		String resources = resource.replaceAll(".0", "");
		if(gm.elementExist(driver, PostTaskObjects.multiOfferDiv)&&gm.elementExist(driver, PostTaskObjects.resourceNumberDiv)){
			gm.setLogMsg("Pass", "can offer multiple resource");
			
			if(multiOffer.equalsIgnoreCase("yes")){
				driver.findElement(PostTaskObjects.multiOfferYes).click();
				driver.findElement(PostTaskObjects.readWriteResourceNumberField).click();
				
				switch(resources){
					case "null":
						driver.findElement(PostTaskObjects.readWriteResourceNumberField).click();
						driver.findElement(PostTaskObjects.jobTitleFieldLabel).click(); //here!
						gm.checkErrorMessages(driver, PostTaskObjects.resourceRequiredErrorMessage1, "resourceRequiredErrorMessage1 - resource field requires input", PostTaskObjects.readWriteResourceNumberField);
						Thread.sleep(2000);
						System.out.println("im here null");
						break;
						
					case "abcd": 	
						driver.findElement(PostTaskObjects.readWriteResourceNumberField).sendKeys(resources);
						driver.findElement(PostTaskObjects.jobTitleFieldLabel).click();
						gm.checkErrorMessages(driver, PostTaskObjects.resourceRequiredErrorMessage2, "resourceRequiredErrorMessage2 - resource field requires numeric input", PostTaskObjects.readWriteResourceNumberField);
						Thread.sleep(2000);
						System.out.println("im here abcd");
						break;
						
					case "1":
						driver.findElement(PostTaskObjects.readWriteResourceNumberField).sendKeys(resources);
						driver.findElement(PostTaskObjects.jobTitleFieldLabel).click();
						gm.checkErrorMessages(driver, PostTaskObjects.resourceRequiredErrorMessage3, "resourceRequiredErrorMessage3 - resource field requires input 2 and above", PostTaskObjects.readWriteResourceNumberField);
						Thread.sleep(2000);
						System.out.println("im here 1");
						break;
						
					case "32768":
						driver.findElement(PostTaskObjects.readWriteResourceNumberField).sendKeys(resources);
						driver.findElement(PostTaskObjects.jobTitleFieldLabel).click();
						gm.checkErrorMessages(driver, PostTaskObjects.resourceRequiredErrorMessage4, "resourceRequiredErrorMessage4 - resource field requires input below 32768", PostTaskObjects.readWriteResourceNumberField);
						Thread.sleep(2000);
						System.out.println("im here 32768");
						break;
						
					default :
						driver.findElement(PostTaskObjects.multiOfferNo).click();/**extra test only*/
						driver.findElement(PostTaskObjects.multiOfferYes).click();
						driver.findElement(PostTaskObjects.jobTitleFieldLabel).click();
						driver.findElement(PostTaskObjects.readWriteResourceNumberField).sendKeys(resources);
						Thread.sleep(2000);
						System.out.println("im here default");
						gm.logScreenshot("Pass", "resource field set", driver);
						
				}
					
			} else {
				if(gm.elementExist(driver, PostTaskObjects.readOnlyResourceNumberField)){
					gm.setLogMsg("Pass", "resource field is read only");
				} else {
					gm.logScreenshot("Fail", "user can input in resource field", driver);
				}
			}
			
		} else {
			gm.logScreenshot("Fail", "no multi offer feature", driver);
		}	
	}
	
//=========================================================================================================================================
	/**User Story 9516:Able to provide a Description for my task in the Task Detail section of Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 12/19/2016
	 * 
	*/
	public void description(WebDriver driver, String description) throws InterruptedException{
//		if(gm.elementExist(driver, PostTaskObjects.description)){
			System.out.println("==============================yehey==============================description field");
			
			gm.checkThings(driver, PostTaskObjects.descriptionErrorMessage, "descriptionErrorMessage");
			driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@id,'react-tinymce')]")));/**take note this is changing, 0 for one instance open, 1 if two instances are open*/
			WebElement element = driver.findElement(PostTaskObjects.body);
			gm.checkThings(driver, PostTaskObjects.descriptionPlaceHolder, "descriptionPlaceHolder");
			
			if(description.equals("null")){
				element.click();
				driver.switchTo().defaultContent();
				driver.findElement(PostTaskObjects.jobTitleField).click();
				Thread.sleep(1000);
				gm.checkThings(driver, PostTaskObjects.descriptionErrorMessage, "descriptionErrorMessage - becuase NULL");
			} else {
				element.click();
				element.sendKeys(description);
				driver.switchTo().defaultContent();
				driver.findElement(PostTaskObjects.jobTitleField).click();
				Thread.sleep(1000);
				gm.checkHiddenThings(driver, PostTaskObjects.descriptionErrorMessage, "descriptionErrorMessage - Hidden");
				gm.setLogMsg("Pass", "description is set!!");
			}

	//	} else {
	//		gm.logScreenshot("Fail", "cannot set description!!!", driver);
	//	}
		
	}
	
//=========================================================================================================================================
	/**User Story 9531:Able to provide the Posting on behalf of in the Task Detail section of Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 12/20/2016
	 * 
	*/
	public void postOnBehalf(WebDriver driver, String eid) throws InterruptedException{
		if(gm.elementExist(driver, PostTaskObjects.behalf) && gm.elementExist(driver, PostTaskObjects.behalfFieldDisabled)){
			System.out.println("==============================yehey==============================behalf field");
			gm.checkThings(driver, PostTaskObjects.behalfPlaceHolder, "behalfPlaceHolder");
			
				if(!eid.equals("null")){
					
				 System.out.println("im inside if loop");
					 
					driver.findElement(PostTaskObjects.behalfFieldDisabled).click();
		            Thread.sleep(2000);
		            
		            List<WebElement> elm = driver.findElements(PostTaskObjects.behalfFieldEnabled);
		            System.out.println("The len is : "+ elm.size());
		            
		            elm.get(0).sendKeys(eid);
		            Thread.sleep(10000);
		            driver.findElement(PostTaskObjects.behalfComboBox(eid)).click();
		            driver.switchTo().defaultContent();	
				} else {
					 System.out.println("im inside else loop");
					gm.setLogMsg("info", "not posting on behalf!!");
				}
				
		} else {
			gm.logScreenshot("Fail", "cannot set behalf field!!!", driver);
		}
		
	}	
	
	
//=========================================================================================================================================
	/**User Story 9515:Able to provide Marketplace in the Task Detail section of Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 12/20/2016
	 * 
	*/
	public void marketplace(WebDriver driver, String market, String category, String filterRequired) throws InterruptedException{
		if(gm.elementExist(driver, PostTaskObjects.marketplace) && gm.elementExist(driver, PostTaskObjects.marketplaceField)){
			System.out.println("==============================yehey==============================marketplace field");
			
			gm.checkThings(driver, FilterObjects.filterButtonDisabled, "filter Button Disabled");
			
			gm.checkErrorMessages(driver, PostTaskObjects.mktErrorMessage, "mktErrorMessage", PostTaskObjects.marketplaceField);
			
			
				if(market.equals("null")){
					//check the placeholders if correct
					gm.checkThings(driver, PostTaskObjects.marketplacePlaceholder1, "marketplace Placeholder 1");
					driver.findElement(PostTaskObjects.marketplaceField).click();
					Thread.sleep(3000);
					
					//END check the placeholders if correct
					
					driver.findElement(PostTaskObjects.jobTitleFieldLabel).click(); //here!
					Thread.sleep(5000);
					gm.checkErrorMessages(driver, PostTaskObjects.mktErrorMessage, "mktErrorMessage - because NULL", PostTaskObjects.marketplaceField);
					marketplaceCategory(driver, category,filterRequired);
				} else {
					
						//check the placeholders if correct
						gm.checkThings(driver, PostTaskObjects.marketplacePlaceholder1, "marketplace Placeholder 1");
						driver.findElement(PostTaskObjects.marketplaceField).click();
						Thread.sleep(3000);
						
						//END check the placeholders if correct
						
						Thread.sleep(2000);
						driver.findElement(PostTaskObjects.marketplaceDropdown(market)).click();
						Thread.sleep(2000);
						if(gm.elementExist(driver, PostTaskObjects.marketplacePopUp)){
						gm.logScreenshot("Pass", "Pop-up pops up!!",driver);
						Thread.sleep(2000);
						driver.findElement(PostTaskObjects.iAgreePopUp).click();
						Thread.sleep(2000);
							
						gm.checkErrorMessagesAreHidden(driver, PostTaskObjects.mktErrorMessage, "mktErrorMessage", PostTaskObjects.marketplaceField);
							
						} else {
							gm.logScreenshot("fail", "Pop-up does not pop up!!",driver);
						}
						
						gm.logScreenshot("Pass", "marketplace Field is set!!",driver);
						
						if(filterRequired.equalsIgnoreCase("yes")){
							Thread.sleep(5000);
							gm.checkThings(driver, FilterObjects.filterButtonStar, "filterButtonStar");
						} else {
							gm.checkHiddenThings(driver, FilterObjects.filterButtonStar, "filterButtonStar - should not be visible");
						}
						
						
						Thread.sleep(2000);
						marketplaceCategory(driver, category, filterRequired);
					}
					
				
				
		} else {
			gm.logScreenshot("Fail", "cannot set marketplace field!!!", driver);
		}
		
	}	
	
	
//=========================================================================================================================================
	/**User Story 9519:Able to provide Category in the Task Detail section of Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 12/20/2016
	 * 
	*/
	public void marketplaceCategory(WebDriver driver, String category, String filterRequired) throws InterruptedException{
		if(gm.elementExist(driver, PostTaskObjects.category) && gm.elementExist(driver, PostTaskObjects.categoryField)){
			System.out.println("==============================yehey==============================category field");
			
			gm.checkThings(driver, FilterObjects.filterButtonDisabled, "filter Button Disabled");
			
			gm.checkErrorMessages(driver, PostTaskObjects.categoryErrorMessage, "categoryErrorMessage", PostTaskObjects.categoryField);
			
				if(category.equals("null")){
					//check the placeholders if correct
					gm.checkThings(driver, PostTaskObjects.categoryPlaceholder1, "category Placeholder 1");
					driver.findElement(PostTaskObjects.jobTitleField).click();
					Thread.sleep(3000);
					
					//END check the placeholders if correct
					
					driver.findElement(PostTaskObjects.jobTitleFieldLabel).click(); //here!
					Thread.sleep(5000);
					gm.checkErrorMessages(driver, PostTaskObjects.categoryErrorMessage, "categoryErrorMessage - because NULL", PostTaskObjects.categoryField);
					
					gm.checkThings(driver, FilterObjects.filterButtonDisabled, "filter Button Disabled");
					
				} else {			
					
					//check the placeholders if correct
					gm.checkThings(driver, PostTaskObjects.categoryPlaceholder1, "category Placeholder 1");
					driver.findElement(PostTaskObjects.categoryField).click();
					Thread.sleep(3000);
					
					//END check the placeholders if correct
					
					Thread.sleep(2000);
					driver.findElement(PostTaskObjects.categoryDropdown(category)).click();
					driver.findElement(PostTaskObjects.jobTitleField).click();
					Thread.sleep(2000);
					gm.checkErrorMessagesAreHidden(driver, PostTaskObjects.categoryErrorMessage, "categoryErrorMessage", PostTaskObjects.categoryField);
					gm.logScreenshot("Pass", "category Field is set!!",driver);
					
					gm.checkHiddenThings(driver, FilterObjects.filterButtonDisabled, "filter Button Disabled - should be hidden since button is enabled");
					
					String outline = driver.findElement(FilterObjects.filterButtonEnabled).getCssValue("border-bottom-color");/**US 12563*/
					String background = driver.findElement(FilterObjects.filterButtonEnabled).getCssValue("background-color");/**US 12563*/
					gm.checkThings(driver, FilterObjects.filterButtonEnabled, "filter Button Enabled");
					
					if(filterRequired.equalsIgnoreCase("yes")){
						if(outline.equals("rgba(192, 155, 209, 1)") && background.equals("rgba(255, 255, 255, 1)"))
							gm.setLogMsg("Pass", "Button border is purple!");
						else 
							gm.logScreenshot("Fail", "Button border is NOT purple!" + outline, driver);
					} else {
						if(outline.equals("rgba(66, 180, 238, 1)") && background.equals("rgba(255, 255, 255, 1)"))
							gm.setLogMsg("Pass", "Button border is purple!");
						else 
							gm.logScreenshot("Fail", "Button border is NOT purple!" + outline, driver);
					}
					
					
					
					gm.checkThings(driver, FilterObjects.filterButtonCrowdCount, "filter Button Crowd Count");
				}
				
		} else {
			gm.logScreenshot("Fail", "cannot set category field!!!", driver);
		}
		
	}
	
	
//=========================================================================================================================================
	/**User Story 9529:Able to provide the Working Hours Required in the Task Detail section of Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 12/20/2016
	 * 
	*/
	public void startWorkHours(WebDriver driver, String isNull, String startHours, String startMinutes, String startAMPM, String mode, String endHours, String endMinutes, String endAMPM, String timeZone) throws InterruptedException{
		if(gm.elementExist(driver, PostTaskObjects.workStartHour)){
			
			if(startHours.equals("0"))
				startHours = "00";
			if(startMinutes.equals("0"))
				startMinutes = "00";
			if(endHours.equals("0"))
				endHours = "00";
			if(endMinutes.equals("0"))
				endMinutes = "00";
			
			System.out.println("==============================yehey==============================works start field");
			gm.checkThings(driver, PostTaskObjects.startHourPlaceHolder, "startHourPlaceHolder");
			
			gm.checkHiddenThings(driver, PostTaskObjects.workEndHourFieldEnabled, "workEndHourFieldEnabled - field should be disabled when start time is not yet populated");
				if(!isNull.contains("start time null")){
					if(mode.equals("manual")){
						driver.findElement(PostTaskObjects.workStartHourField).click();
						driver.findElement(PostTaskObjects.workStartHourField).sendKeys(converter(startHours) + ":" + converter(startMinutes) + " " +  startAMPM);
						driver.findElement(PostTaskObjects.jobTitleField).click();
						gm.setLogMsg("Pass", "start time is set!!");
						
						
					}
					
					if(mode.equals("picker")){
						driver.findElement(PostTaskObjects.workStartHourPicker).click();
						if(gm.elementExist(driver, PostTaskObjects.workStartHourPickerDiv)){
							
							String minute = driver.findElement(PostTaskObjects.timePickerMinute).getText();
							driver.findElement(PostTaskObjects.selectDown).click();//test click - because if time in datapool is already the same, it will not be set
							while(!minute.equals(startMinutes)){
								driver.findElement(PostTaskObjects.selectDown).click();
								minute = driver.findElement(PostTaskObjects.timePickerMinute).getText();
							}
							String hour = driver.findElement(PostTaskObjects.timePickerHour).getText();
							driver.findElement(PostTaskObjects.selectUp).click();//test click - because if time in datapool is already the same, it will not be set
							while(!hour.equals(startHours)){
								driver.findElement(PostTaskObjects.selectUp).click();
								hour = driver.findElement(PostTaskObjects.timePickerHour).getText();
							}
							String ampm = driver.findElement(PostTaskObjects.timePickerAMPM).getText();
							driver.findElement(PostTaskObjects.timePickerAMPM).click();//test click - because if time in datapool is already the same, it will not be set
							while(!ampm.equalsIgnoreCase(startAMPM)){
								driver.findElement(PostTaskObjects.timePickerAMPM).click();
								ampm = driver.findElement(PostTaskObjects.timePickerAMPM).getText();
							}
							driver.findElement(PostTaskObjects.jobTitleField).click();
							gm.setLogMsg("Pass", "start time is set!!");
							
							Thread.sleep(3000);
							
						}else{
							gm.logScreenshot("Fail", "time picker is not displayed!!!", driver);
						}
					}
					
					if(mode.equals("invalid")){
						driver.findElement(PostTaskObjects.workStartHourField).click();
						driver.findElement(PostTaskObjects.workStartHourField).sendKeys(startHours + ":" + startMinutes + " " +  startAMPM);
						driver.findElement(PostTaskObjects.jobTitleField).click();
						gm.checkErrorMessages(driver, PostTaskObjects.startHoursErrorMessage, "startHoursErrorMessage", PostTaskObjects.workStartHourField);
						
					}
					try{
						endWorkHours(driver,isNull,endHours, endMinutes, endAMPM, mode);
					} catch (WebDriverException wde) {
					//don't do anything	
					}
					
					setTimeZone(driver, timeZone);
				} //no else statement since null value is valid.
				
				
		} else {
			gm.logScreenshot("Fail", "cannot set start time!!!", driver);
		}
		
	}
	
//=========================================================================================================================================
	/**User Story 9529:Able to provide the Working Hours Required in the Task Detail section of Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 12/20/2016
	 * 
	*/
	public void endWorkHours(WebDriver driver,String isNull, String endHours, String endMinutes, String endAMPM, String mode) throws InterruptedException{
		if(gm.elementExist(driver, PostTaskObjects.workEndHour)){
			System.out.println("==============================yehey==============================works end field");
			gm.checkThings(driver, PostTaskObjects.endHourPlaceHolder, "endHourPlaceHolder");
			
				if(!isNull.equals("end time null")){
					if(mode.equals("manual")){
						driver.findElement(PostTaskObjects.workEndHourField).click();
						driver.findElement(PostTaskObjects.workEndHourField).sendKeys(converter(endHours) + ":" + converter(endMinutes) + " " +  endAMPM);
						driver.findElement(PostTaskObjects.jobTitleField).click();
						gm.setLogMsg("Pass", "end time is set!!");
						
					}
					
					if(mode.equals("picker")){
						driver.findElement(PostTaskObjects.workEndHourPicker).click();
						if(gm.elementExist(driver, PostTaskObjects.workEndHourPickerDiv)){
							
							String minute = driver.findElement(PostTaskObjects.timePickerMinuteEnd).getText();
							driver.findElement(PostTaskObjects.selectDownEnd).click();//test click - because if time in datapool is already the same, it will not be set
							while(!minute.equals(endMinutes)){
								driver.findElement(PostTaskObjects.selectDownEnd).click();
								minute = driver.findElement(PostTaskObjects.timePickerMinuteEnd).getText();
								
							}
							String hour = driver.findElement(PostTaskObjects.timePickerHourEnd).getText();
							driver.findElement(PostTaskObjects.selectUpEnd).click();//test click - because if time in datapool is already the same, it will not be set
							while(!hour.equals(endHours)){
								driver.findElement(PostTaskObjects.selectUpEnd).click();
								hour = driver.findElement(PostTaskObjects.timePickerHourEnd).getText();
							}
							String ampm = driver.findElement(PostTaskObjects.timePickerAMPMEnd).getText();
							driver.findElement(PostTaskObjects.timePickerAMPMEnd).click();//test click - because if time in datapool is already the same, it will not be set
							while(!ampm.equalsIgnoreCase(endAMPM)){
								driver.findElement(PostTaskObjects.timePickerAMPMEnd).click();
								ampm = driver.findElement(PostTaskObjects.timePickerAMPMEnd).getText();
							}
							driver.findElement(PostTaskObjects.jobTitleField).click();
							gm.setLogMsg("Pass", "end time is set!!");
							
						}else{
							gm.logScreenshot("Fail", "time picker is not displayed!!!", driver);
						}
					}
					
					if(mode.equals("invalid")){
						driver.findElement(PostTaskObjects.workEndHourField).click();
						driver.findElement(PostTaskObjects.workEndHourField).sendKeys(endHours + ":" + endMinutes + " " +  endAMPM);
						driver.findElement(PostTaskObjects.jobTitleField).click();
						gm.checkErrorMessages(driver, PostTaskObjects.endHoursErrorMessage, "endHoursErrorMessage", PostTaskObjects.workEndHourField);
						
					}
					
				} else {
					driver.findElement(PostTaskObjects.workEndHourField).click();
					driver.findElement(PostTaskObjects.workEndHour).click();
					gm.checkErrorMessages(driver, PostTaskObjects.endHoursErrorMessage, "endHoursErrorMessage - because null - ", PostTaskObjects.workEndHourField);
				}
				
		} else {
			gm.logScreenshot("Fail", "cannot set start time!!!", driver);
		}
		
	}
	
//=========================================================================================================================================
	/**User Story 9529:Able to provide the Working Hours Required in the Task Detail section of Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 12/20/2016
	 * 
	*/
	public void setTimeZone(WebDriver driver, String timeZone) throws InterruptedException{
		if(gm.elementExist(driver, PostTaskObjects.timeZone)&&gm.elementExist(driver, PostTaskObjects.timeZoneField)){
			System.out.println("==============================yehey==============================timeZone field");
			
				if(timeZone.equals("null")){
					//check the placeholders if correct
					gm.checkThings(driver, PostTaskObjects.timeZoneplaceholder1, "timezone Placeholder 1");
					driver.findElement(PostTaskObjects.timeZoneField).click();
					Thread.sleep(1000);
					
					//END check the placeholders if correct
					
					driver.findElement(PostTaskObjects.workEndHour).click();
					gm.checkErrorMessagesAreHidden(driver, PostTaskObjects.endHoursErrorMessage, "no error message for timezone - Expected RED border", PostTaskObjects.timeZoneField);
											
						}else{
							//check the placeholders if correct
							gm.checkThings(driver, PostTaskObjects.timeZoneplaceholder1, "timezone Placeholder 1");
							driver.findElement(PostTaskObjects.timeZoneField).click();
							Thread.sleep(3000);
							
							//END check the placeholders if correct
							driver.findElement(PostTaskObjects.timeZoneFieldOption(timeZone)).click();
							gm.logScreenshot("Pass", "time zone is set!!",driver);	
						}
				
		} else {
			gm.logScreenshot("Fail", "cannot set time zone!!!", driver);
		}
		
	}	

	
//=========================================================================================================================================
	/**User Story 9525:Able to provide Estimated Effort in the Task Detail section of Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 12/21/2016
	 * 
	*/
	public void effort(WebDriver driver, String effort, String unit) throws InterruptedException{
		if(gm.elementExist(driver, PostTaskObjects.effort) && gm.elementExist(driver, PostTaskObjects.effortField) && gm.elementExist(driver, PostTaskObjects.effortUnitSpan)){
			System.out.println("==============================yehey==============================effort field");
			
			gm.checkErrorMessages(driver, PostTaskObjects.unitOfEffortErrorMessage, "unitOfEffortErrorMessage", PostTaskObjects.effortField);
			gm.checkThings(driver, PostTaskObjects.effortPlaceHolder, "effortPlaceHolder");
			
				if(unit.equals("Longterm")){
					driver.findElement(PostTaskObjects.effortDropDown(unit)).click();
					gm.checkThings(driver, PostTaskObjects.effortFieldDisabled, "effortFieldDisabled");	
				} else {
					
					if(unit.equals("null")){
						driver.findElement(PostTaskObjects.effortField).click();
						
						Thread.sleep(3000);
						gm.checkErrorMessages(driver, PostTaskObjects.unitOfEffortErrorMessage, "unitOfEffortErrorMessage - because NULL", PostTaskObjects.effortField);
					} else{//if other values
						driver.findElement(PostTaskObjects.effortUnitSpan).click();
						driver.findElement(PostTaskObjects.effortDropDown(unit)).click();
						driver.findElement(PostTaskObjects.effortField).click();
						driver.findElement(PostTaskObjects.effortField).sendKeys(effort);
						driver.findElement(PostTaskObjects.effort).click();
						
						gm.checkErrorMessagesAreHidden(driver, PostTaskObjects.unitOfEffortErrorMessage, "unitOfEffortErrorMessage", PostTaskObjects.effortField);
						
						
					}
					
				}
				
		} else {
			gm.logScreenshot("Fail", "cannot set effort field!!!"+gm.elementExist(driver, PostTaskObjects.effort) + gm.elementExist(driver, PostTaskObjects.effortField) + gm.elementExist(driver, PostTaskObjects.effortUnitSpan), driver);
		}
		
	}
	
	
//=========================================================================================================================================
	/**User Story 9521:Able to provide Start Date in the Task Detail section of Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 12/21/2016
	 * 
	*/
	public void startDate(WebDriver driver, String startHours, String startMinutes, String startAMPM, String mode, String endHours, String endMinutes, String endAMPM, String startDateDay, String startDateMonth, String startDateYear, String endDateDay, String endDateMonth, String endDateYear, String expireDateHour, String expireDateMinute, String expireDateAMPM, String expireDateDay, String expireDateMonth, String expireDateYear) throws InterruptedException{
		if(gm.elementExist(driver, PostTaskObjects.workStartDay)){
			
						
			
			System.out.println("==============================yehey==============================works start day field");
			gm.checkThings(driver, PostTaskObjects.startDatePlaceHolder, "startDatePlaceHolder");
			
				if(!startDateDay.equals("null")){
					if(mode.equals("manual")){
						
						gm.checkErrorMessages(driver, PostTaskObjects.startDateErrorMessage, "startDateErrorMessage", PostTaskObjects.startDateField);
						
						driver.findElement(PostTaskObjects.startDateField).click();
						driver.findElement(PostTaskObjects.startDateField).sendKeys(converter(startDateMonth) + "/" + converter(startDateDay) + "/" + startDateYear + " " +  converter(startHours) + ":" + converter(startMinutes) + " " + startAMPM);
						System.out.println(converter(startDateMonth) + "/" + converter(startDateDay) + "/" + startDateYear + " " +  converter(startHours) + ":" + converter(startMinutes) + " " + startAMPM);
						gm.setLogMsg("Pass", "start date is set!!");
						
						gm.checkErrorMessagesAreHidden(driver, PostTaskObjects.startDateErrorMessage, "startDateErrorMessage - means date ok", PostTaskObjects.startDateField);
					}
					
					if(mode.equals("picker")){
						driver.findElement(PostTaskObjects.startDateField).click();
						
						if(gm.elementExist(driver, PostTaskObjects.calendarOpened)){
							driver.findElement(PostTaskObjects.timeButton).click();
							
							if(gm.elementExist(driver, PostTaskObjects.timePickerOpened)){/**set date first*/
								
								String minute = driver.findElement(PostTaskObjects.timePickerMinuteStart).getText();
								
								while(!minute.equals(startMinutes)){
									driver.findElement(PostTaskObjects.timePickerMinuteSelectUp).click();
									minute = driver.findElement(PostTaskObjects.timePickerMinuteStart).getText();
								}
								
								String hour = driver.findElement(PostTaskObjects.timePickerHourStart).getText();
								
								while(!hour.equals(startHours)){
									//System.out.println(hour + "-" + startHours);
									driver.findElement(PostTaskObjects.timePickerHourSelectUp).click();
									hour = driver.findElement(PostTaskObjects.timePickerHourStart).getText();
								}
								
								String ampm = driver.findElement(PostTaskObjects.timePickerAMPMStart).getText();
								
								while(!ampm.equalsIgnoreCase(startAMPM)){
									driver.findElement(PostTaskObjects.timePickerAMPMSelectDown).click();
									ampm = driver.findElement(PostTaskObjects.timePickerAMPMStart).getText();
								}
								
							}else{
								gm.logScreenshot("Fail", "Time Picker is not displayed!!!", driver);
							}
							
							
							gm.setLogMsg("Pass", "start time of start date is set!!");
							Thread.sleep(3000);
							driver.findElement(PostTaskObjects.calendarPickerButton).click();
							
							if(gm.elementExist(driver, PostTaskObjects.calendarOpened)){/**then go back to calendar*/
								driver.findElement(PostTaskObjects.monthPickerButton).click();
								
								if(gm.elementExist(driver, PostTaskObjects.monthPicker)){/**then click month button*/
									driver.findElement(PostTaskObjects.yearPickerButton).click();
									
									if(gm.elementExist(driver, PostTaskObjects.yearPicker)){/**then click year button and set year*/
										driver.findElement(PostTaskObjects.yearPickerOption(startDateYear)).click();
										gm.setLogMsg("Pass", "start year of start date is set!!");
										
										if(gm.elementExist(driver, PostTaskObjects.monthPicker)){/**go back to set month*/
											startDateMonth = startDateMonth.substring(0, Math.min(startDateMonth.length(), 3));/**cut the name of month to 3 letters*/
											System.out.println(startDateMonth);
											driver.findElement(PostTaskObjects.monthPickerOption(startDateMonth)).click();
											gm.setLogMsg("Pass", "start month of start date is set!!");
											
											if(gm.elementExist(driver, PostTaskObjects.calendarOpened)){/**go back to set day*/
												driver.findElement(PostTaskObjects.dayPickerOption(startDateDay)).click();/**no need to convert*/
												driver.findElement(PostTaskObjects.workStartLabel).click();
												gm.setLogMsg("Pass", "Start date is set!!");
												
											}else {
												gm.logScreenshot("Fail", "did not go back to start day picker!!!", driver);
											}
																						
										} else {
											gm.logScreenshot("Fail", "did not go back to start month picker!!!", driver);
										}
																			
									} else {
										gm.logScreenshot("Fail", "cannot pick start year!!!", driver);
									}
									
								} else {
									gm.logScreenshot("Fail", "Month picker not displayed!!!", driver);
								}
								
							} else {
								gm.logScreenshot("Fail", "did not go back to calendar picker!!!", driver);
							}
							
							//endWorkHours(driver,endHours, endMinutes, endAMPM, mode);
							
							gm.checkErrorMessagesAreHidden(driver, PostTaskObjects.startDateErrorMessage, "startDateErrorMessage - means date ok", PostTaskObjects.startDateField);
							Thread.sleep(3000);
							
						}else{
							gm.logScreenshot("Fail", "Calendar is not displayed!!!", driver);
						}
					}
					
					if(mode.equals("invalid")){
						driver.findElement(PostTaskObjects.startDateField).click();
						driver.findElement(PostTaskObjects.startDateField).sendKeys(converter(startDateMonth) + "/" + converter(startDateDay) + "/" + startDateYear + " " +  converter(startHours) + ":" + converter(startMinutes) + " " + startAMPM);
						

						gm.checkErrorMessagesAreHidden(driver, PostTaskObjects.startDateErrorMessage, "startDateErrorMessage - invalid testing", PostTaskObjects.startDateField);
						//endWorkHours(driver,endHours, endMinutes, endAMPM, mode);
					}
					
				} else {
					driver.findElement(PostTaskObjects.startDateField).click();
					driver.findElement(PostTaskObjects.jobTitleFieldLabel).click(); //here!
					gm.checkErrorMessages(driver, PostTaskObjects.startDateErrorMessage, "startDateErrorMessage - because NULL", PostTaskObjects.startDateField);
				}
				
				
				endWorkdays(driver,endHours, endMinutes, endAMPM, mode, endDateDay, endDateMonth, endDateYear);
				expireDate(driver, expireDateHour,  expireDateMinute,  expireDateAMPM,  expireDateDay,  expireDateMonth,  expireDateYear, mode);
				
		} else {
			gm.logScreenshot("Fail", "cannot set start date and time!!!", driver);
		}
		
	}
	
//=========================================================================================================================================	
	public String converter(String item){
		
		String result = item;
		String[] months1 = {"Filler", "January", "February", "March", "April", "May", "June", "July", "August", "September"};
		String[] months2 = {"Filler", "Filler", "Filler", "Filler", "Filler", "Filler", "Filler", "Filler", "Filler", "Filler", "October", "November", "December"};
		String[] days = {"Filler", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
		
		try{
			for(int i = 0;i<=months1.length;i++){
				
				if(months1[i].equalsIgnoreCase(item)){
					result = "0"+i;
					
					break;
				} else {
					
				}
			}
		} catch (ArrayIndexOutOfBoundsException e){
			
		}
		
		
		try{
			for(int i = 0;i<=months2.length;i++){
				
				if(months2[i].equalsIgnoreCase(item)){
					result = ""+i;
					
					break;
				}
			}
		}catch (ArrayIndexOutOfBoundsException e){
			
		}
		
		
		try{
			for(int i = 0;i<=days.length;i++){
				
				if(days[i].equalsIgnoreCase(item)){
					result = "0"+i;
					
					break;
				}
			}
		}catch (ArrayIndexOutOfBoundsException e){
			
		}
		

		if(item.equals("0")){
			result = "00";
			
		}
		
		
		
		return result;
	}
	
	
//=========================================================================================================================================
	/**User Story 9522:Able to provide End Date in the Task Detail section of Post a task screen.
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 12/22/2016
	 * 
	*/
	public void endWorkdays(WebDriver driver,String endHours, String endMinutes, String endAMPM,  String mode, String endDateDay, String endDateMonth, String endDateYear) throws InterruptedException{
		if(gm.elementExist(driver, PostTaskObjects.workStartDay)){
			
						
			
			System.out.println("==============================yehey==============================works start day field");
			gm.checkThings(driver, PostTaskObjects.endDatePlaceHolder, "endDatePlaceHolder");
			
				if(!endDateDay.equals("null")){
					if(mode.equals("manual")){
						
						gm.checkErrorMessages(driver, PostTaskObjects.endDateErrorMessage, "endDateErrorMessage", PostTaskObjects.endDateField);
						driver.findElement(PostTaskObjects.endDateField).click();
						driver.findElement(PostTaskObjects.endDateField).sendKeys(converter(endDateMonth) + "/" + converter(endDateDay) + "/" + endDateYear + " " +  converter(endHours) + ":" + converter(endMinutes) + " " + endAMPM);
						System.out.println(converter(endDateMonth) + "/" + converter(endDateDay) + "/" + endDateYear + " " +  converter(endHours) + ":" + converter(endMinutes) + " " + endAMPM);
						gm.setLogMsg("Pass", "start date is set!!");
						gm.checkErrorMessagesAreHidden(driver, PostTaskObjects.endDateErrorMessage, "endDateErrorMessage - means date ok", PostTaskObjects.endDateField);

					}
					
					if(mode.equals("picker")){
						driver.findElement(PostTaskObjects.endDateField).click();
						
						if(gm.elementExist(driver, PostTaskObjects.calendarOpenedEnd)){
							driver.findElement(PostTaskObjects.timeButtonEnd).click();
							
							if(gm.elementExist(driver, PostTaskObjects.timePickerOpenedEnd)){/**set date first*/
								
								String minute = driver.findElement(PostTaskObjects.timePickerMinuteEndDate).getText();

								while(!minute.equals(endMinutes)){
									driver.findElement(PostTaskObjects.timePickerMinuteSelectUpEnd).click();
									minute = driver.findElement(PostTaskObjects.timePickerMinuteEndDate).getText();
								}
								
								String hour = driver.findElement(PostTaskObjects.timePickerHourEndDate).getText();
								
								while(!hour.equals(endHours)){
									//System.out.println(hour + "-" + startHours);
									driver.findElement(PostTaskObjects.timePickerHourSelectUpEnd).click();
									hour = driver.findElement(PostTaskObjects.timePickerHourEndDate).getText();
								}
								
								String ampm = driver.findElement(PostTaskObjects.timePickerAMPMEndDate).getText();
								
								while(!ampm.equalsIgnoreCase(endAMPM)){
									driver.findElement(PostTaskObjects.timePickerAMPMSelectDownEnd).click();
									ampm = driver.findElement(PostTaskObjects.timePickerAMPMEndDate).getText();
								}
								
							}else{
								gm.logScreenshot("Fail", "Time Picker is not displayed!!!", driver);
							}
							
							
							gm.setLogMsg("Pass", "start time of end date is set!!");
							Thread.sleep(3000);
							driver.findElement(PostTaskObjects.calendarPickerButtonEnd).click();
							
							if(gm.elementExist(driver, PostTaskObjects.calendarOpenedEnd)){/**then go back to calendar*/
								driver.findElement(PostTaskObjects.monthPickerButtonEnd).click();
								
								if(gm.elementExist(driver, PostTaskObjects.monthPickerEnd)){/**then click month button*/
									driver.findElement(PostTaskObjects.yearPickerButtonEnd).click();
									
									if(gm.elementExist(driver, PostTaskObjects.yearPickerEnd)){/**then click year button and set year*/
										driver.findElement(PostTaskObjects.yearPickerOptionEnd(endDateYear)).click();
										gm.setLogMsg("Pass", "end year of end date is set!!");
										
										if(gm.elementExist(driver, PostTaskObjects.monthPickerEnd)){/**go back to set month*/
											endDateMonth = endDateMonth.substring(0, Math.min(endDateMonth.length(), 3));/**cut the name of month to 3 letters*/
											System.out.println(endDateMonth);
											driver.findElement(PostTaskObjects.monthPickerOptionEnd(endDateMonth)).click();
											gm.setLogMsg("Pass", "end month of end date is set!!");
											
											if(gm.elementExist(driver, PostTaskObjects.calendarOpenedEnd)){/**go back to set day*/
												driver.findElement(PostTaskObjects.dayPickerOptionEnd(endDateDay)).click();/**no need to convert*/
												driver.findElement(PostTaskObjects.workEndLabel).click();
												gm.setLogMsg("Pass", "end date is set!!");
												
											}else {
												gm.logScreenshot("Fail", "did not go back to end day picker!!!", driver);
											}
																						
										} else {
											gm.logScreenshot("Fail", "did not go back to end month picker!!!", driver);
										}
																			
									} else {
										gm.logScreenshot("Fail", "cannot pick end year!!!", driver);
									}
									
								} else {
									gm.logScreenshot("Fail", "Month picker not displayed!!!", driver);
								}
								
							} else {
								gm.logScreenshot("Fail", "did not go back to calendar picker!!!", driver);
							}
							
							
							
							gm.checkErrorMessagesAreHidden(driver, PostTaskObjects.endDateErrorMessage, "endDateErrorMessage - means date ok", PostTaskObjects.endDateField);
							Thread.sleep(3000);
							
						}else{
							gm.logScreenshot("Fail", "Calendar is not displayed!!!", driver);
						}
					}
					
					if(mode.equals("invalid")){
						driver.findElement(PostTaskObjects.endDateField).click();
						driver.findElement(PostTaskObjects.endDateField).sendKeys(converter(endDateMonth) + "/" + converter(endDateDay) + "/" + endDateYear + " " +  converter(endHours) + ":" + converter(endMinutes) + " " + endAMPM);
						

						gm.checkErrorMessagesAreHidden(driver, PostTaskObjects.endDateErrorMessage, "endDateErrorMessage - invalid testing", PostTaskObjects.startDateField);
						
					}
					
				} else {
					driver.findElement(PostTaskObjects.endDateField).click();
					driver.findElement(PostTaskObjects.jobTitleFieldLabel).click(); //here!
					gm.checkErrorMessages(driver, PostTaskObjects.endDateErrorMessage, "endDateErrorMessage - because NULL", PostTaskObjects.startDateField);
				}
				
		} else {
			gm.logScreenshot("Fail", "cannot set end date and time!!!", driver);
		}
		
	}
	
	
//=========================================================================================================================================
	/**User Story *** expire date
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 12/22/2016
	 * 
	*/
	public void expireDate(WebDriver driver,String expireDateHour, String expireDateMinute, String expireDateAMPM, String expireDateDay, String expireDateMonth, String expireDateYear, String mode) throws InterruptedException{
		if(gm.elementExist(driver, PostTaskObjects.workStartDay)){
			
			
			
			System.out.println("==============================yehey==============================works expire day field");
			gm.checkThings(driver, PostTaskObjects.expireDatePlaceHolder, "expireDatePlaceHolder");
			
				if(!expireDateDay.equals("null")){
					if(mode.equals("manual")){
						gm.checkErrorMessages(driver, PostTaskObjects.expirationDateErrorMessage, "expirationDateErrorMessage", PostTaskObjects.endDateField);
						driver.findElement(PostTaskObjects.expireDateField).click();
						driver.findElement(PostTaskObjects.expireDateField).sendKeys(converter(expireDateMonth) + "/" + converter(expireDateDay) + "/" + expireDateYear + " " +  converter(expireDateHour) + ":" + converter(expireDateMinute) + " " + expireDateAMPM);
						System.out.println(converter(expireDateMonth) + "/" + converter(expireDateDay) + "/" + expireDateYear + " " +  converter(expireDateHour) + ":" + converter(expireDateMinute) + " " + expireDateAMPM);
						gm.setLogMsg("Pass", "start date is set!!");
						driver.findElement(PostTaskObjects.jobTitleFieldLabel).click();
						gm.checkErrorMessagesAreHidden(driver, PostTaskObjects.expirationDateErrorMessage, "expirationDateErrorMessage - means date ok", PostTaskObjects.endDateField);

					}
					
					if(mode.equals("picker")){
						driver.findElement(PostTaskObjects.expireDateField).click();
						
						if(gm.elementExist(driver, PostTaskObjects.calendarOpenedExpire)){
							driver.findElement(PostTaskObjects.timeButtonExpire).click();
							
							if(gm.elementExist(driver, PostTaskObjects.timePickerOpenedExpire)){/**set date first*/
								
								String minute = driver.findElement(PostTaskObjects.timePickerMinuteExpire).getText();
								
								while(!minute.equals(expireDateMinute)){
									driver.findElement(PostTaskObjects.timePickerMinuteSelectUpExpire).click();
									minute = driver.findElement(PostTaskObjects.timePickerMinuteExpire).getText();
								}
								
								String hour = driver.findElement(PostTaskObjects.timePickerHourExpire).getText();
								
								while(!hour.equals(expireDateHour)){
									//System.out.println(hour + "-" + startHours);
									driver.findElement(PostTaskObjects.timePickerHourSelectUpExpire).click();
									hour = driver.findElement(PostTaskObjects.timePickerHourExpire).getText();
								}
								
								String ampm = driver.findElement(PostTaskObjects.timePickerAMPMExpire).getText();
								
								while(!ampm.equalsIgnoreCase(expireDateAMPM)){
									driver.findElement(PostTaskObjects.timePickerAMPMSelectDownExpire).click();
									ampm = driver.findElement(PostTaskObjects.timePickerAMPMExpire).getText();
								}
								
							}else{
								gm.logScreenshot("Fail", "Time Picker is not displayed!!!", driver);
							}
							
							
							gm.setLogMsg("Pass", "time of expire date is set!!");
							Thread.sleep(3000);
							driver.findElement(PostTaskObjects.calendarPickerButtonExpire).click();
							
							if(gm.elementExist(driver, PostTaskObjects.calendarOpenedExpire)){/**then go back to calendar*/
								driver.findElement(PostTaskObjects.monthPickerButtonExpire).click();
								
								if(gm.elementExist(driver, PostTaskObjects.monthPickerExpire)){/**then click month button*/
									driver.findElement(PostTaskObjects.yearPickerButtonExpire).click();
									
									if(gm.elementExist(driver, PostTaskObjects.yearPickerExpire)){/**then click year button and set year*/
										driver.findElement(PostTaskObjects.yearPickerOptionExpire(expireDateYear)).click();
										gm.setLogMsg("Pass", "Expire year of Expire date is set!!");
										
										if(gm.elementExist(driver, PostTaskObjects.monthPickerExpire)){/**go back to set month*/
											expireDateMonth = expireDateMonth.substring(0, Math.min(expireDateMonth.length(), 3));/**cut the name of month to 3 letters*/
											System.out.println(expireDateMonth);
											driver.findElement(PostTaskObjects.monthPickerOptionExpire(expireDateMonth)).click();
											gm.setLogMsg("Pass", "Expire month of Expire date is set!!");
											
											if(gm.elementExist(driver, PostTaskObjects.calendarOpenedExpire)){/**go back to set day*/
												driver.findElement(PostTaskObjects.dayPickerOptionExpire(expireDateDay)).click();/**no need to convert*/
												driver.findElement(PostTaskObjects.workExpireLabel).click();
												gm.setLogMsg("Pass", "Expire date is set!!");
												
											}else {
												gm.logScreenshot("Fail", "did not go back to expire day picker!!!", driver);
											}
																						
										} else {
											gm.logScreenshot("Fail", "did not go back to expire month picker!!!", driver);
										}
																			
									} else {
										gm.logScreenshot("Fail", "cannot pick expire year!!!", driver);
									}
									
								} else {
									gm.logScreenshot("Fail", "Month picker not displayed!!!", driver);
								}
								
							} else {
								gm.logScreenshot("Fail", "did not go back to calendar picker!!!", driver);
							}
							
							
							
							gm.checkErrorMessagesAreHidden(driver, PostTaskObjects.expirationDateErrorMessage, "expirationDateErrorMessage - means date ok", PostTaskObjects.endDateField);
							Thread.sleep(3000);
							
						}else{
							gm.logScreenshot("Fail", "Calendar is not displayed!!!", driver);
						}
					}
					
					if(mode.equals("invalid")){
						driver.findElement(PostTaskObjects.expireDateField).click();
						driver.findElement(PostTaskObjects.expireDateField).sendKeys(converter(expireDateMonth) + "/" + converter(expireDateDay) + "/" + expireDateYear + " " +  converter(expireDateHour) + ":" + converter(expireDateMinute) + " " + expireDateAMPM);
						driver.findElement(PostTaskObjects.jobTitleFieldLabel).click();

						gm.checkErrorMessagesAreHidden(driver, PostTaskObjects.expirationDateErrorMessage, "expirationDateErrorMessage - invalid testing", PostTaskObjects.expireDateField);
						
					}
					
				} else {
					driver.findElement(PostTaskObjects.expireDateField).click();
					driver.findElement(PostTaskObjects.jobTitleFieldLabel).click(); //here!
					gm.checkErrorMessages(driver, PostTaskObjects.expirationDateErrorMessage, "expirationDateErrorMessage - because NULL", PostTaskObjects.expireDateField);
				}
				
		} else {
			gm.logScreenshot("Fail", "cannot set expire date and time!!!", driver);
		}
		
	}
	
	
//=========================================================================================================================================
	/**User Story *** expire date
	 * @author jan.carlo.l.sabanal
	 * @throws InterruptedException 
	 * @since 1/3/2017
	 * 
	*/
	public void checkInfoTexts(WebDriver driver) throws InterruptedException
	{
		Actions action = new Actions(driver);  //set actions
		WebElement moveToInfoIcon = driver.findElement(PostTaskObjects.attachInfo);
		System.out.println("=============1===============");
		action.moveToElement(moveToInfoIcon);  //hover over i icon. 
		System.out.println("=============2===============");
		action.perform();
		System.out.println("=============3===============");
		
		if(gm.elementExist(driver, PostTaskObjects.attachInfoText)){
			System.out.println("Pop-up appeared with correct text!!!");
			gm.logScreenshot("Pass", "Pop-up appeared with correct text!!!", driver);
		} else {
			System.out.println("Either no pop-up or incorrect text!!!");
			gm.logScreenshot("Fail", "Either no pop-up or incorrect text!!!", driver);
		}
			
		
	}
	
	
//=========================================================================================================================================
	/**User Story 11591:Task Poster is able to see and access the Target Reach page from the Post a task screen
	 * @author jan.carlo.l.sabanal
	 * @since 2017/01/13
	 * 
	 * */
	public void applyFilters(WebDriver driver, String requiredFilters, String language, String languageAny, String resourceLoc, String careerLevel, String talentSegment, String orgUnit, String specialty, String skills, String marketAttributes) throws InterruptedException
	{
		FilterMethods fm = new FilterMethods();
		if(!requiredFilters.equals("null")){// if null, do not click filter button

			Thread.sleep(1000);
			gm.checkThings(driver, FilterObjects.filterButtonErrorMessage, "filter Button Error Message");
			String marketplaceMembers = driver.findElement(FilterObjects.filterButtonCrowdCount).getText();
			driver.findElement(FilterObjects.filterButtonEnabled).click();
			Thread.sleep(2000);
			
			String checkFilterHeader = driver.findElement(FilterObjects.header).getText();
			if(checkFilterHeader.equals("Target Reach: This post will reach "+marketplaceMembers+" people in the liquid workforce.")){
				gm.setLogMsg("Pass", "Filter Header is with correct initial crowd count!");
			} else {
				gm.logScreenshot("Fail", "Filter Header is incorrect!" + checkFilterHeader + " - " + marketplaceMembers, driver);
			}
			
			gm.checkThings(driver, FilterObjects.filterPanelHeader, "filter Panel Header");
			gm.checkThings(driver, FilterObjects.filterPanelStar, "filter Panel Star");
			gm.checkThings(driver, FilterObjects.filterPanelRequired, "filter Panel Required Text");
			gm.checkThings(driver, FilterObjects.languageHeader, "Language Filter Default Display");
			gm.checkThings(driver, FilterObjects.filterLanguageSelected, "Language Filter Selected By Default");
			boolean f1=true,f2=true,f3=true,f4=true,f5=true,f6=true,f7=true,f8=true;
			
			String[] filter = requiredFilters.split(",");
			for (int i = 0; i < filter.length; i++) {
				
				switch(filter[i]){/**here is the list of methods for each filters:*/
					case "Language":{
							/**User Story 11741: As a Task Poster, I am able to provide Language(s) to filter my targeted crowd in Post a task screen.
							 * @author jan.carlo.l.sabanal
							 * @since 2017/01/13
							 * 
							 * */
							String index = "0";//div id index
								gm.checkThings(driver, FilterObjects.languageIcon, "Language Icon");
								gm.checkThings(driver, FilterObjects.languageStar, "Language Star Required Indicator");
								gm.checkHiddenThings(driver, FilterObjects.languageCheck, "check mark not visible until language filter is set");
								gm.checkThings(driver, FilterObjects.filterLanguageSelected, "Header Present");
								gm.checkThings(driver, FilterObjects.languagePageText, "Page Text present");
							
							if(!language.equals("null")){
								fm.applyLanguageFilter(driver, language, languageAny, index);
							} else {
								 f1 = false;
								 driver.findElement(FilterObjects.applyAndNext(index)).click();
								 gm.checkHiddenThings(driver, FilterObjects.languageFilterErrorMessage, "languageFilterErrorMessage careerLevelFilterErrorMessage - hidden due to xpath @style");
							}
						}
					break;
					
					case "Resource Location": {/**User Story 11057:Task Poster is able to provide my required Resource Location to filter my targeted crowd in Post a task screen*/
						
						String index = "1";
							
							if(gm.elementExist(driver, FilterObjects.filterResourceLocUnselected))
								driver.findElement(FilterObjects.filterResourceLocUnselected).click();
								Thread.sleep(2000);
								gm.checkThings(driver, FilterObjects.filterResourceLocSelected, "Navigated to resorce location page");
								gm.checkThings(driver, FilterObjects.resourceLocHeader, "resorce location Filter Page Header");
								gm.checkThings(driver, FilterObjects.resourceLocPageText, "resorce location Filter Page info text");
								gm.checkThings(driver, FilterObjects.resourceLocIcon, "resorce location Icon");
								gm.checkThings(driver, FilterObjects.resourceLocStar, "resorce location Star Required Indicator");
								gm.checkHiddenThings(driver, FilterObjects.resourceLocCheck, "resorce location checkmark - should be hidden");
						
								if(resourceLoc.equals("null")){
									System.out.println("resource location data is null");
									f2 = false;
									driver.findElement(FilterObjects.resourceLocAnyCheckBox).click();
									driver.findElement(FilterObjects.applyAndNext(index)).click();
									gm.checkThings(driver, FilterObjects.ResourceLocationvalidationErrorMessage, "ResourceLocationvalidationErrorMessage");
								} else {
									fm.applyResourceLocationFilter(driver, resourceLoc);
								}
								
					driver.findElement(FilterObjects.applyAndNext(index)).click();
				
					}
					break;
					
					case "Career Level":{
							/**User Story 11058:Task Poster is able to provide my required Career Level to filter my targeted crowd in Post a task screen for the new UI.
							 * @author jan.carlo.l.sabanal
							 * @since 2017/01/16
							 * 
							 * */
							String index = "2";
							if(gm.elementExist(driver, FilterObjects.filterCareerLevelUnselected))
								driver.findElement(FilterObjects.filterCareerLevelUnselected).click();
								Thread.sleep(2000);
								gm.checkThings(driver, FilterObjects.filterCareerLevelSelected, "Career Level Page Selected");
								gm.checkThings(driver, FilterObjects.careerLevelHeader, "Career Level Filter Page Header");
								gm.checkThings(driver, FilterObjects.careerLevelPageText, "Career Level Filter Page info text");
								gm.checkThings(driver, FilterObjects.careerLevelIcon, "Career Level Icon");
								gm.checkThings(driver, FilterObjects.careerLevelStar, "Career Level Star Required Indicator");
								gm.checkHiddenThings(driver, FilterObjects.careerLevelCheck, "Career Level checkmark");
									
								
							switch(careerLevel){
							case "null":
								f3 = false;
								driver.findElement(FilterObjects.careerLevelCheckBox("Any")).click();
								driver.findElement(FilterObjects.applyAndNext(index)).click();
								gm.checkHiddenThings(driver, FilterObjects.careerLevelFilterErrorMessage, "careerLevelFilterErrorMessage - hidden due to xpath @style");
								break;
								
							case "any":
								/**1.1.1.1f , 1.1.1.1g check if checkboxes are disabled*/
								gm.checkThings(driver, FilterObjects.careerLevelCheckBoxDisabled("LDR"), "LDR disabled");
								
								for(int j=5;j<10;j++){
									gm.checkThings(driver, FilterObjects.careerLevelCheckBoxDisabled("0" + j + "0"), "checkbox disabled for level " + j);
								}
								
								for(int j=10;j<14;j++){
									gm.checkThings(driver, FilterObjects.careerLevelCheckBoxDisabled(j + "0"), "checkbox disabled for level " + j);
								}
								driver.findElement(FilterObjects.applyAndNext(index)).click();
								break;
							
							default:
								fm.applyCareerLevelFilter(driver, careerLevel,index);
							}
							
						}
					break;
					
					
					case "Talent Segment": {/**User Story 11059:Task Poster is able to provide my required Talent Segment to filter my targeted crowd in Post a task screen.*/
						
						String index = "3";
							
							if(gm.elementExist(driver, FilterObjects.filterTalentSegmentUnselected))
								driver.findElement(FilterObjects.filterTalentSegmentUnselected).click();
								Thread.sleep(2000);
								gm.checkThings(driver, FilterObjects.filterTalentSegmentSelected, "Navigated to talent segment page");
								gm.checkThings(driver, FilterObjects.talentSegmentHeader, "talent segment Filter Page Header");
								gm.checkThings(driver, FilterObjects.talentSegmentPageText, "talent segment Filter Page info text");
								gm.checkThings(driver, FilterObjects.talentSegmentIcon, "talent segment Icon");
								gm.checkThings(driver, FilterObjects.talentSegmentStar, "talent segment Star Required Indicator");
								gm.checkHiddenThings(driver, FilterObjects.talentSegmentCheck, "talent segment checkmark - should be hidden");
						
								if(resourceLoc.equals("null")){
									System.out.println("resource location data is null");
									f4 = false;
									driver.findElement(FilterObjects.talentSegmentAnyCheckBox).click();
									driver.findElement(FilterObjects.applyAndNext(index)).click();
									gm.checkThings(driver, FilterObjects.TalentSegmentvalidationErrorMessage, "TalentSegmentvalidationErrorMessage");
								} else {
									fm.applyTalentSegmentFilter(driver, talentSegment);
								}
								
						driver.findElement(FilterObjects.applyAndNext(index)).click();
					
						}
						break;
					
					case "Organization Unit": {/**User Story 11060:Task Poster is able to provide my required Organization Unit to filter my targeted crowd in Post a task screen*/
						
							String index = "4";
								
								if(gm.elementExist(driver, FilterObjects.filterOrgUnitUnselected))
									driver.findElement(FilterObjects.filterOrgUnitUnselected).click();
									Thread.sleep(2000);
									gm.checkThings(driver, FilterObjects.filterOrgUnitSelected, "Navigated to Org Unit page");
									gm.checkThings(driver, FilterObjects.orgUnitHeader, "Org Unit Filter Page Header");
									gm.checkThings(driver, FilterObjects.orgUnitPageText, "Org Unit Filter Page info text");
									gm.checkThings(driver, FilterObjects.orgUnitIcon, "Org Unit Icon");
									gm.checkThings(driver, FilterObjects.orgUnitStar, "Org Unit Star Required Indicator");
									gm.checkHiddenThings(driver, FilterObjects.orgUnitCheck, "Org Unit checkmark - should be hidden");
							
									if(orgUnit.equals("null")){
										System.out.println("org unit data is null");
										f5 = false;
										driver.findElement(FilterObjects.orgUnitAnyCheckBox).click();
										driver.findElement(FilterObjects.applyAndNext(index)).click();
										gm.checkThings(driver, FilterObjects.orgLevelFilterErrorMessage, "orgLevelFilterErrorMessage");
									} else {
										fm.applyOrgUnitFilter(driver, orgUnit);
									}
									
						driver.findElement(FilterObjects.applyAndNext(index)).click();
					
						}
					break;	
					
					case "Specialty":{
						/**User Story 11594: Task Poster is able to provide my required Specialty to filter my targeted crowd in Post a task screen.*/
						String index = "5";//div id index
						
						if(gm.elementExist(driver, FilterObjects.filterSpecialtyUnselected))
							driver.findElement(FilterObjects.filterSpecialtyUnselected).click();
							Thread.sleep(2000);
							gm.checkThings(driver, FilterObjects.filterSpecialtySelected, "Navigated to Specialty page");
							gm.checkThings(driver, FilterObjects.specialtyHeader, "Specialty Filter Page Header");
							gm.checkThings(driver, FilterObjects.specialtyPageText, "Specialty Filter Page info text");
							gm.checkThings(driver, FilterObjects.specialtyIcon, "Specialty Icon");
							gm.checkThings(driver, FilterObjects.specialtyStar, "Specialty Star Required Indicator");
							gm.checkHiddenThings(driver, FilterObjects.specialtyCheck, "Specialty checkmark - should be hidden");
						
						if(!specialty.equals("null")){
							fm.applySpecialtyFilter(driver, specialty, index);
						} else {
							 f6 = false;
							 driver.findElement(FilterObjects.applyAndNext(index)).click();
							 gm.checkHiddenThings(driver, FilterObjects.languageFilterErrorMessage, "languageFilterErrorMessage careerLevelFilterErrorMessage - Hidden due to xpath @style");
						}
					}
					break;
					
					default:
				}
				
				
		    }
			
			driver.findElement(FilterObjects.filterMarketAttributesUnselected).click();//to go to the last panel and click close index 8
			driver.findElement(FilterObjects.done).click();//click this everytime for now, regardless if marketplace attributes filter is required or not.
			
			driver.findElement(FilterObjects.filterSpecialtyUnselected).click();//temporary only. change this once "Done" button stabilized. Basically after clicking done under marketplace attributes, this will just select the last filter and click ApplyAndClose, just to save the applied filters.
			driver.findElement(FilterObjects.applyAndClose("5")).click();
			
			
			String filterSavedCount = driver.findElement(FilterObjects.headerCount).getText();
			
			//driver.findElement(FilterObjects.applyAndClose("7")).click();
			//Thread.sleep(3000);
			marketplaceMembers = driver.findElement(FilterObjects.filterButtonCrowdCount).getText();
			
			String outline = driver.findElement(FilterObjects.filterButtonEnabled).getCssValue("border-bottom-color");/**US 12563*/
			String background = driver.findElement(FilterObjects.filterButtonEnabled).getCssValue("background-color");/**US 12563*/
			
			/**12563*/
				if(outline.equals("rgba(192, 155, 209, 1)") && background.equals("rgba(192, 155, 209, 1)"))
					gm.setLogMsg("Pass", "Button border and background is purple!");
				else 
					gm.logScreenshot("Fail", "Button is NOT purple!" + outline + background, driver);
			
			
			if(marketplaceMembers.equals(filterSavedCount)){
				gm.setLogMsg("Pass", "Filter crowd count matches with the button info!");
			} else {
				gm.logScreenshot("Fail", "Filter crowd count does not match with the button info!" + filterSavedCount + " - " + marketplaceMembers, driver);
			}
			

			
			if(f1&&f2&&f3&&f4&&f5&&f6&&f7&&f8){//if true, then that means all required filters are populated
				gm.checkHiddenThings(driver, FilterObjects.filterButtonErrorMessage, "filter Button Error Message - Should not appear since required filters are populated " + f1+f2+f3+f4+f5+f6+f7+f8);
			}else{
				driver.findElement(PostTaskObjects.saveButton).click();
				if(gm.elementExist(driver, PostTaskObjects.reviewModal)){
					gm.logScreenshot("Fail", "Poster is able to post even when required filters are not populated!", driver);
					driver.findElement(PostTaskObjects.reviewCloseButton).click();
					gm.checkThings(driver, FilterObjects.filterButtonErrorMessage, "filter Button Error Message" + f1+f2+f3+f4+f5+f6+f7+f8);
				} else {
					gm.checkThings(driver, FilterObjects.filterButtonErrorMessage, "filter Button Error Message" + f1+f2+f3+f4+f5+f6+f7+f8);
				}
				
			}
			
			
		} else {//if null, just check if error appears
			gm.checkThings(driver, FilterObjects.filterButtonErrorMessage, "filter Button Error Message");
		}
		
			
		
	}	
	
	
//=========================================================================================================================================
	
	/**Sprint 5
	 * User Story 10344:Task Poster is able to click the Post a task for a specific Marketpace.
	 * @author jan.carlo.l.sabanal
	 * @since 2016/12/19 modified 2017/01/03
	 * 
	 * Sprint 6
	 * User Story 11636: Task Poster is able to see the correct filter information in the Review modal pop up after I apply and proceed in clicking the Post button from the post a task form
	 * @since 01/17/2017
	 * */
	public void reviewPost( WebDriver driver, String title, String requiredFilters, String language, String languageAny, String resourceLoc, String careerLevel, String talentSegment, String orgUnit, String specialty, String skills, String marketAttributes, String restrict, String targetCrowdBeforeReview) throws InterruptedException
	{
		
			gm.checkThings(driver, PostTaskObjects.reviewReviewTitle, "Review");
			gm.checkThings(driver, PostTaskObjects.reviewReviewTask, "Task summary");
			gm.checkThings(driver, PostTaskObjects.reviewReviewTitle, "Title");
			
			String checkTaskname = driver.findElement(PostTaskObjects.reviewReviewTaskName).getText();
			if(checkTaskname.equals(title)){
				gm.setLogMsg("pass", "Task Title is reviewed and correct!!");
			} else {
				gm.logScreenshot("Fail", "Task Title is incorrect!! " + checkTaskname + " - " + title, driver);
			}
			
			gm.checkThings(driver, PostTaskObjects.reviewPeopleCount, "peopleCount");
			String checkTargetCrowd = driver.findElement(PostTaskObjects.reviewPeopleCount).getText();
			if(checkTargetCrowd.contains(targetCrowdBeforeReview)){
				gm.setLogMsg("pass", "Target crowd number is reviewed and matches!!");
			} else {
				gm.logScreenshot("Fail", "target crowd is not matching!! " + checkTargetCrowd + " - " + targetCrowdBeforeReview, driver);
			}
			
			gm.checkThings(driver, PostTaskObjects.reviewOptimize, "optimize statement");
			
			
			
			/**insert method here that will validate the filters used*/
			
			String[] filter = requiredFilters.split(",");
			String image = null;
			String[] filterCount = null;
			int displayedReviewFilterCount;
			
				for (int i = 0; i < filter.length; i++) {
					
					try{
						switch(filter[i]){
						case "Language":
							image = "icn_review_language";
							filterCount = language.split(",");
						case "Career Level":
							image = "icn_career_level_slate";
							filterCount = careerLevel.split(",");
						case "Organization Unit":/**added to compliment US11060, US 13702*/
							image = "icn_org_unit_slate";
							filterCount = orgUnit.split("-");
							filter[i] = "Organizational Unit";//add "al" to Organization
						case "Resource Location":/**added to compliment US11057, US 13702*/
							image = "icn_review_location";
							filterCount = resourceLoc.split("-");
						case "Specialty":/**added to compliment US11594, US 13702*/
							image = "icn_specialty_slate";
							filterCount = specialty.split(",");
						case "Talent Segment":/**added to compliment US11059, US 13702*/
							image = "icn_talent_segment_slate";
							filterCount = talentSegment.split("-");
						default:
							
						}
							
						gm.checkThings(driver, PostTaskObjects.reviewAppliedFilter(filter[i]), "Check if filter is present: " + filter[i]);
						gm.checkThings(driver, PostTaskObjects.reviewAppliedFilterIcon(filter[i],image), "Check if filter icon for " + filter[i] + " is present.");
										
						displayedReviewFilterCount	= Integer.parseInt(driver.findElement(PostTaskObjects.reviewAppliedFilterCount(filter[i])).getText());
						if(displayedReviewFilterCount==filterCount.length){
							gm.setLogMsg("Pass", displayedReviewFilterCount + " = " + filterCount.length);
						} else {
							gm.logScreenshot("Fail", displayedReviewFilterCount + " != " + filterCount.length, driver);
							System.out.println("*****" + displayedReviewFilterCount + "!=" + filterCount.length + "*****");
						}
					} catch (org.openqa.selenium.NoSuchElementException e){
						gm.logScreenshot("info", "Filter details not found for " + filter[i], driver);
					}
				}
			
			
			
			gm.checkThings(driver, PostTaskObjects.modifyPost, "modify post button");
			/**insert method here that will check if modify post works*/
			gm.checkThings(driver, PostTaskObjects.startOver, "start over button");
			
				/**
				 * Sprint 6
				 * User Story 12564: Task Poster is able see and configure restrict application options to the members who matched the defined filter of the task at any given point in time from the Review screen.
				 * @since 01/18/2017
				 * @author jan.carlo.l.sabanal
				 */
				gm.checkThings(driver, PostTaskObjects.restrictMessage, "restrict applicants Message");
				gm.checkThings(driver, PostTaskObjects.restrictCheckBox, "restrictCheckBox");
				if(restrict.equalsIgnoreCase("yes"))
					driver.findElement(PostTaskObjects.restrictCheckBox).click();
			
			driver.findElement(PostTaskObjects.confirmPost).click();
			Thread.sleep(1000);
			gm.checkThings(driver, PostTaskObjects.loading, "loading image");
			gm.waitForObject(driver, PostTaskObjects.thumbsUp);
			
			
			successfullPostModal(driver);
			
			
		
		
	
	}
	
//=========================================================================================================================================
	
	public void successfullPostModal(WebDriver driver) throws InterruptedException
	{
		if(gm.elementExist(driver, PostTaskObjects.thumbsUp)){
			gm.logScreenshot("Pass", "successfull posting!!!", driver);
			
			gm.checkThings(driver, PostTaskObjects.awesome, "Awesome!");
			gm.checkThings(driver, PostTaskObjects.liveNow, "live Now!");
			gm.checkThings(driver, PostTaskObjects.whatNow, "what now??");
			gm.checkThings(driver, PostTaskObjects.viewMyPost, "view your post!");
			gm.checkThings(driver, PostTaskObjects.postNewtask, "Post Again!");
			gm.checkThings(driver, PostTaskObjects.signOut, "sign Out!");
			gm.checkThings(driver, PostTaskObjects.home2, "home!");
			
			
			/**insert here methods that will check view post, post new task(similar to start over), and sign out
			 * */
			
			//driver.navigate().to(EnvironmentVariables.URL);
		}else{
			gm.logScreenshot("Fail", "unsuccessfull posting!!!", driver);
			driver.navigate().to(EnvironmentVariables.URL);
		}
	}
}
